{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}

module Annotate where

import Control.Monad (forM)
import Control.Monad.State (MonadState, evalState, get, put)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set

import Parse
import Interpret

annotate :: Map String (LocalSpec, [Code]) -> Map String (Function, [(Function, Code)])
annotate = fmap annotate1

annotate1 :: (LocalSpec, [Code]) -> (Function, [(Function, Code)])
annotate1 (spec, code) = flip evalState 0 $ do
  f <- specFunction spec
  pure (f, map (\c -> (codeFunction c, c)) code)

data Constraint = CConstraint String Type
  deriving (Show, Eq, Ord)

data Function = FFunction (Set Int) (Set Constraint) [Type] [Type] [Type] [Type]
  deriving (Show, Eq, Ord)

delta :: Function -> Int
delta (FFunction _ _ pre post _ _) = length post - length pre

vdelta :: Function -> Int
vdelta (FFunction _ _ _ _ vpre vpost) = length vpost - length vpre

data Type = TVariable Int | TAtomic Atom | TFunction Function
  deriving (Show, Eq, Ord)

isVariable :: Type -> Bool
isVariable (TVariable _) = True
isVariable _ = False

fresh :: MonadState Int m => m Int
fresh = do
  n <- get
  put (n + 1)
  return n

specFunction :: MonadState Int m => LocalSpec -> m Function
specFunction (LocalSpec ins vars outs) = do
  is <- forM ins $ const fresh
  os <- forM outs $ const fresh
  pure $ FFunction (Set.fromList (is ++ os)) Set.empty (map TVariable is) (map TVariable os) [] []

codeFunction (LocalInit) = func [0] [] [] [0]
codeFunction (LocalNoInit) = func [] [] [] [0]
codeFunction (LocalDrop n) = func [] [] [0..n-1] []
codeFunction (LocalFetch n) = func [] [n] [0..n] [0..n]
codeFunction (LocalStore n) = func [n] [] ([0..(n-1)] ++ [n+1]) [0..n]
codeFunction (PushInt _)  = func [] [0] [] []
codeFunction (PushFloat _)  = func [] [0] [] []
codeFunction (Unary _)  = func [0] [0] [] []
codeFunction (Binary _)  = func [0, 0] [0] [] []
codeFunction (Cast t)  = cast [0] [TAtomic t]

func pre post vpre vpost = FFunction (Set.fromList (pre ++ post ++ vpre ++ vpost)) Set.empty (map TVariable pre) (map TVariable post) (map TVariable vpre) (map TVariable vpost)

cast pre post = FFunction (Set.fromList pre) Set.empty (map TVariable pre) post [] []
