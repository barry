{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Main (main) where

import Control.Monad (forM_)
import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import Text.Parsec (parse)

import Parse (Document(Document), document)
import Interpret (interpret)
import Annotate (annotate)
import Check (check)
import Compile (compile)

err msg = hPutStrLn stderr (show msg) >> exitFailure

main = do
  [filename] <- getArgs
  contents <- readFile filename
  case parse document filename contents of
    Right doc@(Document defs cabis) -> case interpret doc of
      Right code -> case annotate code of
        anno -> forM_ cabis $ \cabi -> case check anno cabi of
          Right mono -> case compile cabi mono of
            Right str -> putStr str
            Left compileError -> err compileError
          Left typeError -> err typeError
      Left interpretError -> err interpretError
    Left parseError -> err parseError
