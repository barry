{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Interpret where

import Control.Monad (forM_)
import Control.Monad.Except (runExceptT, throwError)
import Control.Monad.Writer (execWriterT, tell)
import Data.Map (Map)
import qualified Data.Map as Map
import Text.Read (readMaybe)

import Parse

data InterpretError = WordNotFoundNotANumber String | LocalNotFound String
  deriving Show

interpret1 :: Definition -> Map String (LocalSpec, [Code]) -> Either InterpretError (Map String (LocalSpec, [Code]))
interpret1 (Definition name spec@(LocalSpec args vars outs) source) words = do
  let arglocs = (args ++ vars) `zip` [0..]
  r <- runExceptT . execWriterT $ do
    forM_ (reverse arglocs) $ \(arg, i) -> tell [if arg `elem` args then LocalInit else LocalNoInit]
    let arglocsmap = Map.fromList arglocs
        go ("->":op:rest) = case Map.lookup op arglocsmap of
          Just i -> tell [LocalStore i] >> go rest
          _ -> throwError (LocalNotFound op)
        go (op:rest) = case Map.lookup op arglocsmap of
          Just i -> tell [LocalFetch i] >> go rest
          _ -> case Map.lookup op words of
            Just code -> tell (snd code) >> go rest
            _ -> case readMaybe op of
              Just i -> tell [PushInt i] >> go rest
              _ -> case readMaybe op of
                Just i -> tell [PushFloat i] >> go rest
                _ -> throwError (WordNotFoundNotANumber op)
        go [] = tell [LocalDrop (Map.size arglocsmap)]
    go source
  case r of
    Right code -> Right $ Map.insert name (spec, code) words
    Left err -> Left err

interprets :: [Definition] -> Map String (LocalSpec, [Code]) -> Either InterpretError (Map String (LocalSpec, [Code]))
interprets [] words = Right words
interprets (def:defs) words = case interpret1 def words of
  Right words -> interprets defs words
  Left err -> Left err

interpret :: Document -> Either InterpretError (Map String (LocalSpec, [Code]))
interpret (Document defs _) = interprets defs builtins

data Code
  = LocalInit
  | LocalNoInit
  | LocalDrop Int
  | LocalFetch Int
  | LocalStore Int
  | PushInt Int
  | PushFloat Double
  | Unary UnOp
  | Binary BinOp
  | Cast Atom
  deriving Show

data UnOp = Not | Invert
  deriving Show

data BinOp = Add | Sub | Mul | Div | Shl | Shr | And | Or | Xor | Eq | Gt
  deriving Show

data Atom = U8 | U16 | U32 | U64 | I8 | I16 | I32 | I64 | F32 | F64
  deriving (Read, Show, Eq, Ord)

b op = (LocalSpec ["a", "b"] [] ["z"], [Binary op])
u op = (LocalSpec ["a"] [] ["z"], [Unary op])
c op = (LocalSpec ["a"] [] ["z"], [Cast op])

builtins = Map.fromList
  [ ("+", b Add)
  , ("-", b Sub)
  , ("*", b Mul)
  , ("/", b Div)
  , ("<<", b Shl)
  , (">>", b Shr)
  , ("&", b And)
  , ("|", b Or)
  , ("^", b Xor)
  , ("==", b Eq)
  , (">", b Gt)
  , ("!", u Not)
  , ("~", u Invert)
  , ("u8", c U8)
  , ("u16", c U16)
  , ("u32", c U32)
  , ("u64", c U64)
  , ("i8", c I8)
  , ("i16", c I16)
  , ("i32", c I32)
  , ("i64", c I64)
  , ("f32", c F32)
  , ("f64", c F64)
  ]
