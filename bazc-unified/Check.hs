{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

module Check where

import Control.Monad (forM)
import Control.Monad (foldM, replicateM, unless)
import Control.Monad.Except (MonadError(throwError), ExceptT, runExceptT)
import Control.Monad.Identity (Identity, runIdentity)
import Control.Monad.State (MonadState(get, put), StateT, evalStateT)
import Control.Monad.Writer (MonadWriter(tell), WriterT, runWriterT)
import Data.Char (toUpper)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set

import Parse
import Interpret
import Annotate hiding (fresh)
import Compile hiding (fresh)

data TypeError
  = Underflow
  | Overflow
  | Conflict Equation
  | Infinite Equation
  | Ambiguous [Function]
  deriving (Show, Eq, Ord)


data Equation = Equation (Type) (Type)
  deriving (Show, Eq, Ord)

lhs, rhs :: Equation -> Type
lhs (Equation l _) = l
rhs (Equation _ r) = r

data Substitution = Int :-> Type
  deriving (Show, Eq, Ord)

class Substitute t where
  substitute :: Substitution -> t -> t
  substitutes :: Map Int Type -> t -> t

instance Substitute Constraint where
   substitute s (CConstraint name t) = (CConstraint name $ substitute s t)
   substitutes s (CConstraint name t) = (CConstraint name $ substitutes s t)

instance Substitute Function where
  substitute s@(v :-> _) f@(FFunction vars constraints pre post vpre vpost)
    | v `Set.member` vars = f
    | otherwise = FFunction vars constraints
        (substitute s `map` pre)
        (substitute s `map` post)
        (substitute s `map` vpre)
        (substitute s `map` vpost)
  substitutes s f@(FFunction vars constraints pre post vpre vpost)
    = FFunction vars (substitutes s' `Set.map` constraints)
        (substitutes s' `map` pre)
        (substitutes s' `map` post)
        (substitutes s' `map` vpre)
        (substitutes s' `map` vpost)
    where
      s' = s `Map.withoutKeys` vars

instance Substitute Type where
  substitute (v :-> t) s@(TVariable u)
    | u == v = t
    | otherwise = s
  substitute _ a@(TAtomic _) = a
  substitute s (TFunction f) = TFunction (substitute s f)
  substitutes s t@(TVariable u) = case Map.lookup u s of
    Just v -> v
    Nothing -> t
  substitutes _ a@(TAtomic _) = a
  substitutes s (TFunction f) = TFunction (substitutes s f)

instance Substitute Equation where
   substitute s (Equation l r) = Equation (substitute s l) (substitute s r)
   substitutes s (Equation l r) = Equation (substitutes s l) (substitutes s r)

class Variables t where
  variables :: t -> Set Int

instance Variables Type where
  variables (TVariable v) = Set.singleton v
  variables (TAtomic _) = Set.empty
  variables (TFunction f) = variables f

instance Variables Function where
  variables (FFunction vars constraints pre post vpre vpost) =
    Set.unions (map variables (pre ++ post ++ vpre ++ vpost)) Set.\\ vars

instance Variables Equation where
  variables (Equation l r) = variables l `Set.union` variables r


data Step
  = SDelete (Equation)
  | SSwap (Equation)
  | SDecompose (Equation)
  | SEliminate (Equation)
  deriving (Show, Eq, Ord)

class (Monad m) => MonadUnify m where
  fresh :: m Int
  step :: Step -> Set (Equation) -> m (Set (Equation))
  conflict :: Equation -> m a
  infinite :: Equation -> m a
  underflow :: m a
  overflow :: m a

delete :: MonadUnify m => Equation -> m (Set (Equation))
delete e@(Equation l r)
  | l == r = step (SDelete e) Set.empty
  | otherwise = pure (Set.singleton e)

deletes :: MonadUnify m => Set (Equation) -> m (Set (Equation))
deletes = fmap Set.unions . mapM delete . Set.toList

decompose :: MonadUnify m => Equation -> m (Set (Equation))
{-
l :: { a b c -- d e }
r :: {   u v --   x }
add free variable padding
r :: { p u v -- p x }
l ~ r => a ~ p , b ~ u, c ~ v, d ~ p, e ~ x
-}
decompose e@(Equation (TVariable _) _) = pure (Set.singleton e)
decompose e@(Equation _ (TVariable _)) = pure (Set.singleton e)
decompose e@(Equation a@(TAtomic _) b@(TAtomic _))
  | a == b = pure (Set.singleton e)
decompose e@(Equation (TFunction f@(FFunction _ _ lpre lpost lvpre lvpost))
                      (TFunction g@(FFunction _ _ rpre rpost rvpre rvpost)))
  | delta f == delta g && vdelta f == vdelta g = do
      let ldiff = max 0 $ length rpre - length lpre
          rdiff = max 0 $ length lpre - length rpre
          lvdiff = max 0 $ length rvpre - length lvpre
          rvdiff = max 0 $ length lvpre - length rvpre
      lpad <- map TVariable <$> replicateM ldiff fresh
      rpad <- map TVariable <$> replicateM rdiff fresh
      lvpad <- map TVariable <$> replicateM lvdiff fresh
      rvpad <- map TVariable <$> replicateM rvdiff fresh
      let e' = Set.fromList $
                  zipWith Equation (lpre ++ lpad) (rpre ++ rpad) ++
                  zipWith Equation (lpost ++ lpad) (rpost ++ rpad) ++
                  zipWith Equation (lvpre ++ lvpad) (rvpre ++ rvpad) ++
                  zipWith Equation (lvpost ++ lvpad) (rvpost ++ rvpad)
      step (SDecompose e) e'
decompose e = conflict e

decomposes :: MonadUnify m => Set (Equation) -> m (Set (Equation))
decomposes = fmap Set.unions . mapM decompose . Set.toList

swap :: MonadUnify m => Equation -> m (Set (Equation))
swap e@(Equation (TVariable _) _) = pure (Set.singleton e)
swap e@(Equation l r@(TVariable _)) = do
  let e' = Set.singleton (Equation r l)
  step (SSwap e) e'
swap e = pure (Set.singleton e)

swaps :: MonadUnify m => Set (Equation) -> m (Set (Equation))
swaps = fmap Set.unions . mapM swap . Set.toList

eliminates :: MonadUnify m => Set (Equation) -> m (Set (Equation))
eliminates eqs = head $
  [ step (SEliminate e) e'
  | e@(Equation (TVariable l) r) <- Set.toList eqs
  , l `Set.notMember` variables r
  , let otherEqs = Set.delete e eqs
  , l `Set.member` (Set.unions . map variables . Set.toList) otherEqs
  , let s = l :-> r
  , let e' = Set.insert e . Set.map (substitute s) $ otherEqs
  ] ++ [pure eqs]

occursCheck :: MonadUnify m => Equation -> m (Set (Equation))
occursCheck e@(Equation (TVariable _) (TVariable _)) = pure (Set.singleton e)
occursCheck e@(Equation (TVariable l) r)
  | l `Set.member` variables r = infinite e
occursCheck e = pure (Set.singleton e)

occursChecks :: MonadUnify m => Set (Equation) -> m (Set (Equation))
occursChecks = fmap Set.unions . mapM occursCheck . Set.toList

substitution :: MonadUnify m => Set (Equation) -> m (Map Int (Type))
substitution eqs =
  if any (not . isVariable . lhs) (Set.toList eqs)
  then error "not a substitution"
  else pure $ Map.fromList
         [ (k, v) | Equation (TVariable k) v <- Set.toList eqs ]

unifyM :: MonadUnify m => Set (Equation) -> m (Map Int (Type))
unifyM eqs0 = do
  eqs <- deletes eqs0
  eqs <- decomposes eqs
  eqs <- swaps eqs
  eqs <- eliminates eqs
  eqs <- occursChecks eqs
  if eqs == eqs0
    then substitution eqs
    else unifyM eqs

newtype UnifyT m a
  = Unify (ExceptT (TypeError) (StateT Int (WriterT [Step] m)) a)
  deriving (Functor, Applicative, Monad)
deriving instance Monad m => MonadError (TypeError) (UnifyT m)
deriving instance Monad m => MonadState Int (UnifyT m)
deriving instance Monad m => MonadWriter [Step] (UnifyT m)

instance (Monad m) => MonadUnify (UnifyT m) where
  fresh = do
    n <- get
    put (n + 1)
    pure n
  step tag e = do
    tell [tag]
    pure e
  conflict e = throwError (Conflict e)
  infinite e = throwError (Infinite e)
  underflow = throwError Underflow
  overflow = throwError Overflow

runUnifyT
  :: (Monad m)
  => UnifyT m a -> m (Either (TypeError) a, [Step])
runUnifyT (Unify u) = runWriterT . flip evalStateT 0 . runExceptT $ u

unify :: Set (Equation) -> (Either (TypeError) (Map Int (Type)), [Step])
unify = runIdentity . runUnifyT . unifyM

rename :: MonadUnify m => Function -> m (Function)
rename (FFunction vars constraints pre post vpre vpost) = do
  let var v = (,) v `fmap` fresh
  t <- fmap Map.fromList . mapM var . Set.toList $ vars
  let s = fmap TVariable t
  pure (FFunction Set.empty (substitutes s `Set.map` constraints)
          (substitutes s `map` pre) (substitutes s `map` post)
          (substitutes s `map` vpre) (substitutes s `map` vpost))

typeCheck
  :: (MonadUnify m)
  => Function -> [Function] -> m (Function, [Function])
typeCheck f fs = do
  f@(FFunction vars constraints pre post vpre vpost) <- rename f
  fs <- mapM rename fs
  (constraints, result, vresult, eqs) <- foldM equating (constraints, pre, vpre, Set.empty) fs
  eq1 <- equateExact vresult vpost
  eq <- equateExact result post
  let e = eq1 `Set.union` eq `Set.union` eqs
  s <- unifyM e
  pure (substitutes s f, substitutes s `map` fs)

equateExact result post
  | length result > length post = overflow
  | length result < length post = underflow
  | otherwise = pure . Set.fromList $ zipWith Equation result post

equating (constraints, stack, locals, eqs) (FFunction _ constraints2 pre post vpre vpost)
  | length stack < length pre = underflow
  | length locals < length vpre = underflow
  | otherwise = pure
      ( Set.union constraints constraints2
      , post ++ drop (length pre) stack
      , vpost ++ drop (length vpre) locals
      , Set.fromList (zipWith Equation stack pre ++ zipWith Equation locals vpre) `Set.union` eqs
      )

toplevelConstraints
  :: (Function, [Function]) -> (Function, [Function])
toplevelConstraints (f, fs) =
  let constraintsOf (FFunction _ c _ _ _ _) = c
      (FFunction vars _ pre post vpre vpost) `withConstraints` c
        = FFunction vars c pre post vpre vpost
      cs = Set.unions (constraintsOf f : map constraintsOf fs)
      for = flip map
  in  (f `withConstraints` cs, fs `for` (`withConstraints` Set.empty))

monomorphic :: Variables t => t -> Bool
monomorphic = Set.null . variables

check :: Map String (Function, [(Function, Code)]) -> C_ABI -> Either TypeError (Int, Int, [Primitive])
check words (C_ABI _ spec word) = case (typeFunction spec, Map.lookup word words) of
  (func, Just (_, funcCode)) -> case runIdentity $ runUnifyT (typeCheck func (map fst funcCode)) of
    (Left typeError, _) -> Left typeError
    (Right (func, funcs), _) -> case toplevelConstraints (func, funcs) of
      (f, fs) -> case filter (not . monomorphic) (f : fs) of
        [] -> assemble (zip fs (map snd funcCode))
        es -> Left (Ambiguous es)

typeFunction :: TypeSpec -> Function
typeFunction (TypeSpec ins outs) = FFunction Set.empty Set.empty (map (TAtomic . read . map toUpper) ins) (map (TAtomic . read . map toUpper) outs) [] []

assemble :: [(Function, Code)] -> Either TypeError (Int, Int, [Primitive])
assemble = asm 0 0 0 0 []
  where
    asm stackSize localSize _ _ acc [] = Right (stackSize, localSize, reverse acc)
    asm stackSize localSize stackDepth localDepth acc ((func, code) : fcs)
      = asm (max stackSize newStackDepth)
            (max localSize newLocalDepth)
            newStackDepth newLocalDepth (reverse (prim func code) ++ acc) fcs
      where
        newStackDepth = stackDepth + delta func
        newLocalDepth = case code of
          LocalInit -> localDepth + 1
          LocalNoInit -> localDepth + 1
          LocalDrop n -> localDepth - n
          _ -> localDepth

prim (FFunction _ _ [TAtomic t] [] _ _) (LocalInit) = [OpLocalInit (show t)]
prim (FFunction _ _ [] [] _ _) (LocalNoInit) = [OpLocalNoInit]
prim (FFunction _ _ [] [] _ _) (LocalDrop n) = [OpLocalDrop n]
prim (FFunction _ _ [] [TAtomic t] _ _) (LocalFetch n) = [OpLocalFetch n (show t) (show t)]
prim (FFunction _ _ [TAtomic t] [] _ _) (LocalStore n) = [OpLocalStore (show t) n (show t)]
prim (FFunction _ _ [] [TAtomic t] _ _) (PushInt n) = [OpInt n (show t)]
prim (FFunction _ _ [] [TAtomic t] _ _) (PushFloat n) = [OpFloat n (show t)]
prim (FFunction _ _ [TAtomic s] [TAtomic t] _ _) (Unary op) = [OpUnary op (show s) (show t)]
prim (FFunction _ _ [TAtomic s1, TAtomic s2] [TAtomic t] _ _) (Binary op) = [OpBinary op (show s1) (show s2) (show t)]
prim (FFunction _ _ [TAtomic s] [TAtomic t] _ _) (Cast op) = [OpCast op (show s) (show t)]
prim f o = error $ show (f, o)
