{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}

module Compile where

import Control.Monad (forM, forM_)
import Control.Monad.Except (MonadError, runExceptT, throwError)
import Control.Monad.State (MonadState, evalStateT, get, put)
import Control.Monad.Writer (MonadWriter, execWriterT, tell)
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map

import Parse
import Interpret

data CompileError = ProgramNotFoundError | PrimitiveNotFoundError
  deriving Show

compile :: C_ABI -> (Int, Int, [Primitive]) -> Either CompileError String
compile cabi code = fmap unlines . flip evalStateT 0 . execWriterT . runExceptT $ tell ["#include \"barry.h\""] >> compiles cabi code

fresh :: MonadState Int m => m String
fresh = do
  n <- get
  put (n + 1)
  return $ "v" ++ show n


compiles :: (MonadState Int m, MonadWriter [String] m, MonadError CompileError m) => C_ABI -> (Int, Int, [Primitive]) -> m ()
compiles (C_ABI name (TypeSpec ins outs) word)  (dataSize, localSize, code) = do
  ins <- forM ins $ \t -> do
    v <- fresh
    return (t, v)
  outs <- forM outs $ \t -> do
    v <- fresh
    return (t, v)
  let indecls = map indecl ins
      outdecls = map outdecl outs
      indecl (t, v) = t ++ " " ++ v
      outdecl (t, v) = t ++ " *" ++ v
  tell ["extern void " ++ name ++ "(" ++ intercalate "," (indecls ++ outdecls) ++ ")"]
  tell ["{"]
  tell ["CELL DATA[" ++ show dataSize ++ "];"]
  tell ["int DP = 0;"]
  tell ["CELL LOCAL[" ++ show localSize ++ "];"]
  tell ["int LP = 0;"]
  forM_ ins $ \(t, v) -> tell ["DATA[DP]." ++ t ++ " = " ++ v ++ "; DP += 1;"]
  forM_ code primitive
  forM_ (reverse outs) $ \(t, v) -> tell ["*" ++ v ++ " = DATA[DP-1]." ++ t ++ "; DP -= 1;"]
  tell ["}"]

primitive (OpLocalInit zt) = tell ["LOCAL[LP]." ++ zt ++ " = DATA[DP-1]." ++ zt ++ "; DP -= 1; LP += 1;"]
primitive (OpLocalNoInit) = tell ["LP += 1;"]
primitive (OpLocalDrop n) = tell ["LP -= " ++ show n ++ ";"]
primitive (OpLocalFetch a at zt) = tell ["DATA[DP]." ++ zt ++ " = LOCAL[LP-1-" ++ show a ++ "]." ++ at ++ "; DP += 1;"]
primitive (OpLocalStore at z zt) = tell ["LOCAL[LP-" ++ show z ++ "]." ++ zt ++ " = DATA[DP-1]." ++ at ++ "; DP -= 1;"]
primitive (OpInt a zt) = tell ["DATA[DP]." ++ zt ++ " = " ++ show a ++ "; DP += 1;"]
primitive (OpFloat a zt) = tell ["DATA[DP]." ++ zt ++ " = " ++ show a ++ "; DP += 1;"]
primitive (OpUnary op at zt) = tell ["DATA[DP-1]." ++ zt ++ " = " ++ show op ++ "(DATA[DP-1]." ++ at ++ ");"]
primitive (OpBinary op at bt zt) = tell ["DATA[DP-2]." ++ zt ++ " = " ++ show op ++ "(DATA[DP-2]." ++ at ++ ", DATA[DP-1]." ++ bt ++ "); DP -= 1;"]
primitive (OpCast op at zt) = tell ["DATA[DP-1]." ++ zt ++ " = (" ++ show op ++ ")(DATA[DP-1]." ++ at ++ ");"]

data Primitive
  = OpLocalInit String
  | OpLocalNoInit
  | OpLocalDrop Int
  | OpLocalFetch Int String String
  | OpLocalStore String Int String
  | OpInt Int String
  | OpFloat Double String
  | OpUnary UnOp String String
  | OpBinary BinOp String String String
  | OpCast Atom String String
