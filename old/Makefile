#!/usr/bin/env -S make -f
# barry -- bytebeat livecoding environment
# Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

C99FLAGS = -std=c99 -Wall -Wextra -pedantic
OFLAGS = -O3
SOURCE = -Wl,barry.c -Wl,Makefile -Wl,README.md -Wl,LICENSE.md -Wl,barry.png -Wl,alphabet.utf8
EMBED = -Wl,--format=binary $(SOURCE) -Wl,--format=default
STB ?= -I../../github.com/nothings/stb
GL ?= -lGL -lGLEW -lglfw
JACK ?= -ljack

barrington: barrington.c
	gcc $(C99FLAGS) $(OFLAGS) -o barrington barrington.c $(JACK) -lm

barry: barry.c Makefile README.md LICENSE.md barry.png alphabet.utf8
	gcc $(C99FLAGS) $(OFLAGS) -o barry barry.c $(EMBED) $(STB) $(GL) $(JACK) -lm

clean:
	-rm barry

.PHONY: clean
