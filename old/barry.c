/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <getopt.h>
#include <glob.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <jack/jack.h>

#define STBI_ONLY_PNG
#define STBI_NO_STDIO
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// ----------------------------- embedded source ----------------------

extern const unsigned char _binary_alphabet_utf8_start[], _binary_alphabet_utf8_end[];
extern const unsigned char _binary_barry_c_start[], _binary_barry_c_end[];
extern const unsigned char _binary_barry_png_start[], _binary_barry_png_end[];
extern const unsigned char _binary_Makefile_start[], _binary_Makefile_end[];
extern const unsigned char _binary_README_md_start[], _binary_README_md_end[];
extern const unsigned char _binary_LICENSE_md_start[], _binary_LICENSE_md_end[];

int write_file(const char *name, const unsigned char *start, const unsigned char *end)
{
  printf("writing '%s'... ", name);
  fflush(stdout);
  FILE *file = fopen(name, "wxb");
  if (! file)
  {
    printf("FAILED\n");
    return 0;
  }
  int ok = (fwrite(start, end - start, 1, file) == 1);
  fclose(file);
  if (ok)
    printf("ok\n");
  else
    printf("FAILED\n");
  return ok;
}

int write_files(void)
{
  int ok = 1;
  ok &= write_file("alphabet.utf8", _binary_alphabet_utf8_start, _binary_alphabet_utf8_end);
  ok &= write_file("barry.c", _binary_barry_c_start, _binary_barry_c_end);
  ok &= write_file("barry.png", _binary_barry_png_start, _binary_barry_png_end);
  ok &= write_file("Makefile", _binary_Makefile_start, _binary_Makefile_end);
  ok &= write_file("README.md", _binary_README_md_start, _binary_README_md_end);
  ok &= write_file("LICENSE.md", _binary_LICENSE_md_start, _binary_LICENSE_md_end);
  return ok;
}

// ----------------------------- constants ----------------------------

const int screen_width = 1280;
const int screen_height = 800;
const int screen_border = 20;

typedef uint32_t word;
// <http://www.burtleburtle.net/bob/hash/integer.html>
word hash(word a)
{
  a = (a+0x7ed55d16) + (a<<12);
  a = (a^0xc761c23c) ^ (a>>19);
  a = (a+0x165667b1) + (a<<5);
  a = (a+0xd3a2646c) ^ (a<<9);
  a = (a+0xfd7046c5) + (a<<3);
  a = (a^0xb55a4f09) ^ (a>>16);
  return a;
}

// ----------------------------- opcodes ------------------------------

#define SHL 0xab
#define SHR 0xbb
typedef word opcode;

#define PROGRAM_LENGTH_MAX 4096
struct program
{
  unsigned char code[PROGRAM_LENGTH_MAX]; // FIXME 8bit encoding
  word length;
  word cursor;
  bool insert;
};

bool insert_code(struct program *p, opcode op)
{
  if (p->length < PROGRAM_LENGTH_MAX)
  {
    if (p->insert)
    {
      memmove(&p->code[p->cursor + 1], &p->code[p->cursor], p->length - p->cursor);
      p->code[p->cursor] = op;
      p->cursor++;
      p->length++;
      return true;
    }
    else
    {
      p->code[p->cursor] = op;
      if (p->cursor == p->length)
      {
        p->cursor++;
        p->length++;
      }
      return true;
    }
  }
  else
  {
    return false;
  }
}

void cursor_toggle_mode(struct program *p)
{
  p->insert = ! p->insert;
}

bool cursor_left(struct program *p)
{
  if (p->cursor > 0)
  {
    p->cursor--;
    return true;
  }
  else
  {
    return false;
  }
}

bool cursor_right(struct program *p)
{
  if (p->cursor < p->length)
  {
    p->cursor++;
    return true;
  }
  else
  {
    return false;
  }
}

bool cursor_up(struct program *p)
{
  if (p->cursor > 0)
  {
    while (true)
    {
      p->cursor--;
      if (p->cursor == 0 || p->code[p->cursor - 1] == '\n')
      {
        break;
      }
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool cursor_down(struct program *p)
{
  if (p->cursor < p->length)
  {
    while (true)
    {
      p->cursor++;
      if (p->cursor == p->length || p->code[p->cursor] == '\n')
      {
        break;
      }
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool cursor_home(struct program *p)
{
  if (p->cursor > 0)
  {
    p->cursor = 0;
    return true;
  }
  else
  {
    return false;
  }
}

bool cursor_end(struct program *p)
{
  if (p->cursor < p->length)
  {
    p->cursor = p->length;
    return true;
  }
  else
  {
    return false;
  }
}

bool delete_code(struct program *p)
{
  if (p->cursor < p->length)
  {
    memmove(&p->code[p->cursor], &p->code[p->cursor + 1], p->length - p->cursor - 1);
    p->length--;
    return true;
  }
  else
  {
    return false;
  }
}

// ----------------------------- stack --------------------------------

#define STACK_DEPTH_MAX 256
struct stack
{
  word stack[STACK_DEPTH_MAX];
  word depth;
};

bool stack_push(struct stack *s, word w)
{
  if (s->depth < STACK_DEPTH_MAX)
  {
    s->stack[s->depth++] = w;
    return true;
  }
  else
  {
    return false;
  }
}

bool stack_pop(struct stack *s, word *w)
{
  if (s->depth > 0)
  {
    *w = s->stack[--s->depth];
    return true;
  }
  else
  {
    *w = 0;
    return false;
  }
}

#define MEMORY_MAX (1 << 8) // must be a power of two, >= 16 * channels
#define MEMORY_MASK (MEMORY_MAX - 1)

struct memory
{
  word mem[MEMORY_MAX];
};

word opcode_nibble(opcode op);

word interpret(struct program *p, struct memory *m, word chan, word time)
{
  struct stack eval;
  memset(&eval, 0, sizeof(eval));
  for (word k = 0; k < p->length; ++k)
  {
      opcode op = p->code[k];
      switch (op)
      {
        case '(':
        {
          int comment_depth = 1;
          for (++k; k < p->length; ++k)
          {
            opcode op2 = p->code[k];
            if (op2 == '(')
            {
              comment_depth++;
            }
            else if (op2 == ')')
            {
              if (--comment_depth == 0)
              {
                break;
              }
            }
          }
          break;
        }

        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9':
        case 'a': case 'b': case 'c':
        case 'd': case 'e': case 'f':
        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F':
        {
          stack_push(&eval, opcode_nibble(op));
          break;
        }

        case 't':
        case 'T':
        case '$':
        {
          stack_push(&eval, time);
          break;
        }

        case '@':
        {
          stack_push(&eval, chan);
          break;
        }

        case '~':
        {
          word a;
          stack_pop(&eval, &a);
          stack_push(&eval, ~a);
          break;
        }

        case '!':
        {
          word a;
          stack_pop(&eval, &a);
          stack_push(&eval, !a);
          break;
        }

        case ']':
        {
          word a;
          stack_pop(&eval, &a);
          stack_push(&eval, m->mem[a & MEMORY_MASK]);
          break;
        }

        case '+':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a + b);
          break;
        }

        case '-':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a - b);
          break;
        }

        case SHL:
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a << b);
          break;
        }

        case SHR:
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a >> b);
          break;
        }

        case '*':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a * b);
          break;
        }

        case '/':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          if (b != 0)
          {
            stack_push(&eval, a / b);
          }
          else
          {
            stack_push(&eval, ~(word)0);
          }
          break;
        }

        case '%':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          if (b != 0)
          {
            stack_push(&eval, a % b);
          }
          else
          {
            stack_push(&eval, 0);
          }
          break;
        }

        case '&':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a & b);
          break;
        }

        case '|':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a | b);
          break;
        }

        case '^':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a ^ b);
          break;
        }

        case '<':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a < b);
          break;
        }

        case '>':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a > b);
          break;
        }

        case '=':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, a == b);
          break;
        }

        case '[':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          m->mem[b & MEMORY_MASK] = a;
          break;
        }

        case ':':
        {
          word a;
          stack_pop(&eval, &a);
          stack_push(&eval, a);
          stack_push(&eval, a);
          break;
        }

        case 'x':
        case 'X':
        {
          word a;
          word b;
          stack_pop(&eval, &b);
          stack_pop(&eval, &a);
          stack_push(&eval, b);
          stack_push(&eval, a);
          break;
        }

        case '_':
        {
          word a;
          stack_pop(&eval, &a);
          break;
        }

        case '?':
        {
          word tst, thn, els;
          stack_pop(&eval, &tst);
          stack_pop(&eval, &thn);
          stack_pop(&eval, &els);
          stack_push(&eval, tst ? thn : els);
          break;
        }

        case '#':
        {
          word n = 0;
          stack_pop(&eval, &n);
          n &= 0xF; // avoid infinite loop when n = UINT_MAX
          word acc = 0;
          for (word i = 0; i < n; ++i)
          {
            word w = 0;
            stack_pop(&eval, &w);
            acc <<= 4;
            acc ^= w;
          }
          stack_push(&eval, acc);
          break;
        }
      }
  }
  word result;
  stack_pop(&eval, &result);
  return result;
}

// ----------------------------- state --------------------------------

enum focus { focus_default = 0, focus_filelist };

struct
{
  struct program program;
  struct memory memory;
  // audio
#define OUTCHANNELS 2
  jack_client_t *client;
  jack_port_t *out[OUTCHANNELS];
#define BUFFER_FRAMES 16
// must be a power of two, and a multiple of this value
#define RING_BUFFER_SIZE (BUFFER_FRAMES * 16 * 16 * OUTCHANNELS)
#define RING_BUFFER_MASK (RING_BUFFER_SIZE - 1)
  word t;
  unsigned char ringbuffer[RING_BUFFER_SIZE][OUTCHANNELS];
  bool usign;
  double sractual;
  double srbackend;
  double srtarget;
  int srdivider;
  // video
  struct stack visual, literal;
  unsigned char background[PROGRAM_LENGTH_MAX * STACK_DEPTH_MAX][4]; // 4MiB
#define TEX_CODE_GLYPHS   0
#define TEX_KEYBOARD_CODE 1
#define TEX_PROGRAM_CODE  2
#define TEX_STACK_VISUAL  3
#define TEX_VIDEO         4
#define TEX_MEMORY        5
#define TEX_FILES         6
  GLuint vbo;
  GLuint vao;
  GLuint shader;
  GLint cursor;
  GLint insert;
  GLint file;
  GLuint textures[7];
  enum focus input_focus;
  int selected_file;
  glob_t glob;
} interpreter;

bool load_file(const char *filename_to_load);

// ----------------------------- audio --------------------------------

static int processcb(jack_nframes_t nframes, void *arg)
{
  (void) arg;
  // get jack buffers
  jack_default_audio_sample_t *out[OUTCHANNELS];
  for (int c = 0; c < OUTCHANNELS; ++c)
  {
    out[c] = (jack_default_audio_sample_t *)
      jack_port_get_buffer(interpreter.out[c], nframes);
  }
  // clear out to prevent possibly loud xruns
  for (int c = 0; c < OUTCHANNELS; ++c)
  {
    for (jack_nframes_t i = 0; i < nframes; ++i)
    {
      out[c][i] = 0;
    }
  }
  // loop over samples
  for (jack_nframes_t i = 0; i < nframes; ++i)
  {
    word t = interpreter.t++ / interpreter.srdivider;
    for (int c = 0; c < OUTCHANNELS; ++c)
    {
      uint8_t w = interpreter.ringbuffer[t & RING_BUFFER_MASK][c]
        = interpret(&interpreter.program, &interpreter.memory, c, t);
      float o = 0.0f;
      if (interpreter.usign)
      {
        // unsigned
        o = (((float) w) - (float) 0x80) / (float) 0x80;
      }
      else
      {
        // signed, enabled by default to avoid extreme DC offset on load
        int8_t ws = w;
        o = (float) ws / (float) 0x80;
      }
      out[c][i] = o;
    }
  }
  return 0;
}

// ----------------------------- video --------------------------------

unsigned char keyboard[] =
  "cdef +-\xab\xbb $@"
  "89ab */%! []"
  "4567 &|^~ :x"
  "0123 <>=? _#"
  ;

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

#define GLSL(s) "#version 330 core\n" #s

const char *vertex = GLSL(

  layout(location = 0) in vec2 vertex;

  out vec2 coord;

  void main(void)
  {
    gl_Position = vec4(2.0 * vertex - vec2(1.0), 0.0, 1.0);
    coord = vertex;
  }

);

const char *fragment = GLSL(

  uniform sampler2DArray glyphs;
  uniform usampler2D keyboard;
  uniform usampler1D code;
  uniform sampler2D stack;

  uniform int cursor;
  uniform bool insert;

  uniform sampler2D video;
  uniform sampler2D memory;

  uniform usampler2D files;
  uniform int file;

  in vec2 coord;

  layout(location = 0) out vec4 colour;

  void main(void)
  {
    vec4 c = vec4(vec3(0.0), 1.0);
    if (gl_FragCoord.x < 256.0 && gl_FragCoord.y < 256.0)
    {
      c = vec4(texture(video, gl_FragCoord.xy / 256.0).rgg, 1.0);
    }
    else if (gl_FragCoord.x < 512.0 && gl_FragCoord.y < 256.0)
    {
      c = vec4(texture(memory, gl_FragCoord.xy / 256.0 - vec2(1.0, 0.0)).rgb, 1.0);
    }
    else
    {
      vec2 x = coord;
      x.x = atanh(2.0 * x.x - 1.0);
      x.x *= 16.0;
      x.x += float(cursor) - (insert ? 0.5 : 0.0);
      vec2 px = dFdx(x);
      x.y -= 0.5;
      x.y *= dFdx(x.x) / dFdx(coord.x);
      x.y += 0.5;
      vec2 py = dFdy(x);
      float l = floor(x.x);
      float y = 1.0 - x.y;
      if (-1.0 <= l && l < textureSize(code, 0) + 1.0)
      {
        uint layer = 0u;
        if (0.0 <= l && l < textureSize(code, 0) - 0.5)
          layer = texture(code, l / textureSize(code, 0)).x;
        float glyph = 0.0;
        if (0.0 <= y && y <= 1.0)
        {
          glyph = textureGrad(glyphs, vec3(fract(x.x), y, layer), px, py).x;
          if (int(floor(x.x + (insert ? 0.5 : 0.0))) == cursor)
          {
            glyph = 1.0 - glyph;
          }
        }
        x.y -= 1.0;
        x.y /= textureSize(code, 0);
        x.y *= float(textureSize(stack, 0).y) / float(textureSize(stack, 0).x);
        x.x /= textureSize(code, 0);
        if (0 <= x.y && x.y < 1.0 && 0.0 <= l && l < textureSize(code, 0))
        {
          vec4 background = texture(stack, x.yx);
          c *= 1.0 - background.a;
          c.rgb += background.rgb;
        }
        c = mix(c, vec4(1.0), glyph);
      }
    }
    if (file >= 0)
    {
      vec2 x = coord;
      uint layer = texture(files, x).x;
      x.y = 1.0 - x.y;
      x *= textureSize(files, 0);
      float glyph = textureGrad(glyphs, vec3(fract(x), layer), dFdx(x), dFdy(x)).x;
      if (int(floor(x.y)) == textureSize(files, 0).y - 1 - file)
      {
        glyph = 1.0 - glyph;
      }
      c = mix(c, vec4(vec3(0.0), 1.0), 0.75);
      c = mix(c, vec4(1.0), glyph);
    }
    colour = c;
  }

);

void video_initialize(void)
{
  // compile shader
  interpreter.shader = vertex_fragment_shader(vertex, fragment);
  glUseProgram(interpreter.shader);
  glUniform1i(glGetUniformLocation(interpreter.shader, "glyphs"), TEX_CODE_GLYPHS);
  glUniform1i(glGetUniformLocation(interpreter.shader, "keyboard"), TEX_KEYBOARD_CODE);
  glUniform1i(glGetUniformLocation(interpreter.shader, "code"), TEX_PROGRAM_CODE);
  glUniform1i(glGetUniformLocation(interpreter.shader, "stack"), TEX_STACK_VISUAL);
  glUniform1i(glGetUniformLocation(interpreter.shader, "video"), TEX_VIDEO);
  glUniform1i(glGetUniformLocation(interpreter.shader, "memory"), TEX_MEMORY);
  glUniform1i(glGetUniformLocation(interpreter.shader, "files"), TEX_FILES);
  interpreter.cursor = glGetUniformLocation(interpreter.shader, "cursor");
  interpreter.insert = glGetUniformLocation(interpreter.shader, "insert");
  interpreter.file = glGetUniformLocation(interpreter.shader, "file");
  // vertex specification
  glGenVertexArrays(1, &interpreter.vao);
  glBindVertexArray(interpreter.vao);
  glGenBuffers(1, &interpreter.vbo);
  glBindBuffer(GL_ARRAY_BUFFER, interpreter.vbo);
  const unsigned char vdata[8] = { 0, 0, 0, 1, 1, 0, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vdata), &vdata[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // generate textures
  glGenTextures(7, &interpreter.textures[0]);
  // initialize glyphs
  glActiveTexture(GL_TEXTURE0 + TEX_CODE_GLYPHS);
  glBindTexture(GL_TEXTURE_2D_ARRAY, interpreter.textures[TEX_CODE_GLYPHS]);
  int x, y, n;
  unsigned char *data = stbi_load_from_memory(_binary_barry_png_start, _binary_barry_png_end - _binary_barry_png_start, &x, &y, &n, 1);
  if (! data)
  {
    fprintf(stderr, "ERROR: could not load font image\n");
    exit(1);
  }
  glPixelStorei(GL_UNPACK_ROW_LENGTH, x);
  glTexImage3D
    ( GL_TEXTURE_2D_ARRAY, 0, GL_RED
    , x / 16, y / 16, 256, 0
    , GL_RED, GL_UNSIGNED_BYTE, 0
    );
  int k = 0;
  for (int j = 0; j < 16; ++j)
  {
    for (int i = 0; i < 16; ++i)
    {
      glTexSubImage3D
        ( GL_TEXTURE_2D_ARRAY, 0, 0, 0, k++
        , x / 16, y / 16, 1
        , GL_RED, GL_UNSIGNED_BYTE
        , &data[j * y / 16 * x + i * x / 16]
        );
    }
  }
  stbi_image_free(data);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
  glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // initialize keyboard
  glActiveTexture(GL_TEXTURE0 + TEX_KEYBOARD_CODE);
  glBindTexture(GL_TEXTURE_2D, interpreter.textures[TEX_KEYBOARD_CODE]);
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_R8UI
    , 12, 4, 0
    , GL_RED_INTEGER, GL_UNSIGNED_BYTE, &keyboard[0]
    );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // initialize code texture to 1 invalid
  glActiveTexture(GL_TEXTURE0 + TEX_PROGRAM_CODE);
  glBindTexture(GL_TEXTURE_1D, interpreter.textures[TEX_PROGRAM_CODE]);
  word empty = -1;
  glTexImage1D
    ( GL_TEXTURE_1D, 0, GL_R8UI
    , 1, 0
    , GL_RED_INTEGER, GL_UNSIGNED_BYTE, &empty
    );
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // initialize stack visual to 1x1 transparent
  glActiveTexture(GL_TEXTURE0 + TEX_STACK_VISUAL);
  glBindTexture(GL_TEXTURE_2D, interpreter.textures[TEX_STACK_VISUAL]);
  empty = 0;
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_RGBA
    , 1, 1, 0
    , GL_RGBA, GL_UNSIGNED_BYTE, &empty
    );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  // initialize video to 16x16, no data
  glActiveTexture(GL_TEXTURE0 + TEX_VIDEO);
  glBindTexture(GL_TEXTURE_2D, interpreter.textures[TEX_VIDEO]);
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_RG
    , 16, 16, 0
    , GL_RG, GL_UNSIGNED_BYTE, 0
    );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  // initialize memory to 16x16, no data
  glActiveTexture(GL_TEXTURE0 + TEX_MEMORY);
  glBindTexture(GL_TEXTURE_2D, interpreter.textures[TEX_MEMORY]);
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_RGBA
    , 16, 16, 0
    , GL_RGBA, GL_UNSIGNED_BYTE, 0
    );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  // initialize memory to 1x1, no data
  glActiveTexture(GL_TEXTURE0 + TEX_FILES);
  glBindTexture(GL_TEXTURE_2D, interpreter.textures[TEX_FILES]);
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_R8UI
    , 19, 1, 0
    , GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0
    );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

bool opcode_is_nibble(opcode op)
{
  switch (op)
  {
    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7':
    case '8': case '9': case 'a': case 'b':
    case 'c': case 'd': case 'e': case 'f':
    case 'A': case 'B': case 'C': case 'D':
    case 'E': case 'F':
      return true;
    default:
      return false;
  }
}

word opcode_nibble(opcode op)
{
  switch (op)
  {
    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7':
    case '8': case '9':
      return op - '0';
    case 'a': case 'b': case 'c': case 'd':
    case 'e': case 'f':
      return 10 + op - 'a';
    case 'A': case 'B': case 'C': case 'D':
    case 'E': case 'F':
      return 10 + op - 'A';
    default:
      return -1;
  }
}

void video_update_code(void)
{
  memset(&interpreter.background, 0, sizeof(interpreter.background));
  word max_stack_depth = 0;
  for (int pass = 0; pass < 2; ++pass)
  {
    memset(&interpreter.visual, 0, sizeof(interpreter.visual));
    memset(&interpreter.literal, 0, sizeof(interpreter.literal));
    for (word k = 0; k < interpreter.program.length; ++k)
    {
      opcode op = interpreter.program.code[k];
      switch (op)
      {
        case '(':
        {
          int comment_depth = 1;
          for (++k; k < interpreter.program.length; ++k)
          {
            opcode op2 = interpreter.program.code[k];
            if (op2 == '(')
            {
              comment_depth++;
            }
            else if (op2 == ')')
            {
              if (--comment_depth == 0)
              {
                break;
              }
            }
          }
          break;
        }

        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9':
        case 'a': case 'b': case 'c':
        case 'd': case 'e': case 'f':
        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F':
        case 't': case 'T': case '$':
        case '@':
        {
          stack_push(&interpreter.visual, hash(op));
          stack_push(&interpreter.literal, op);
          break;
        }

        case '~': case '!': case ']':
        {
          word a;
          stack_pop(&interpreter.visual, &a);
          stack_push(&interpreter.visual, hash(op ^ hash(a)));
          stack_pop(&interpreter.literal, &a);
          stack_push(&interpreter.literal, op);
          break;
        }

        case '+': case '-': case SHL: case SHR:
        case '*': case '/': case '%':
        case '&': case '|': case '^':
        case '<': case '>': case '=':
        {
          word a, b;
          stack_pop(&interpreter.visual, &b);
          stack_pop(&interpreter.visual, &a);
          stack_push(&interpreter.visual, hash(op ^ hash(a ^ hash(b))));
          stack_pop(&interpreter.literal, &b);
          stack_pop(&interpreter.literal, &a);
          stack_push(&interpreter.literal, op);
          break;
        }

        case '[':
        {
          word a, b;
          stack_pop(&interpreter.visual, &b);
          stack_pop(&interpreter.visual, &a);
          stack_pop(&interpreter.literal, &b);
          stack_pop(&interpreter.literal, &a);
          break;
        }

        case ':':
        {
          word a;
          stack_pop(&interpreter.visual, &a);
          stack_push(&interpreter.visual, a);
          stack_push(&interpreter.visual, a);
          stack_pop(&interpreter.literal, &a);
          stack_push(&interpreter.literal, op);
          stack_push(&interpreter.literal, op);
          break;
        }

        case 'x':
        case 'X':
        {
          word a, b;
          stack_pop(&interpreter.visual, &b);
          stack_pop(&interpreter.visual, &a);
          stack_push(&interpreter.visual, b);
          stack_push(&interpreter.visual, a);
          stack_pop(&interpreter.literal, &b);
          stack_pop(&interpreter.literal, &a);
          stack_push(&interpreter.literal, op);
          stack_push(&interpreter.literal, op);
          break;
        }

        case '_':
        {
          word a;
          stack_pop(&interpreter.visual, &a);
          stack_pop(&interpreter.literal, &a);
          break;
        }

        case '?':
        {
          word tst, thn, els;
          stack_pop(&interpreter.visual, &tst);
          stack_pop(&interpreter.visual, &thn);
          stack_pop(&interpreter.visual, &els);
          stack_push(&interpreter.visual, hash(op ^ hash(els ^ hash(thn ^ hash(tst)))));
          stack_pop(&interpreter.literal, &tst);
          stack_pop(&interpreter.literal, &thn);
          stack_pop(&interpreter.literal, &els);
          stack_push(&interpreter.literal, op);
          break;
        }

        case '#':
        {
          word n = 0;
          stack_pop(&interpreter.visual, &n);
          stack_pop(&interpreter.literal, &n);
          if (opcode_is_nibble(n))
          {
            n = opcode_nibble(n);
            word h = hash(op ^ hash(n));
            for (word i = 0; i < n; ++i)
            {
              word w = 0;
              stack_pop(&interpreter.literal, &w);
              stack_pop(&interpreter.visual, &w);
              h = hash(h ^ hash(w));
            }
            stack_push(&interpreter.visual, h);
            stack_push(&interpreter.literal, op);
          }
          else
          {
            // type error: in n# n must be a literal nibble
          }
          break;
        }
      }
      if (pass == 0)
      {
        if (interpreter.visual.depth > max_stack_depth)
        {
          max_stack_depth = interpreter.visual.depth;
        }
      }
      else if (pass == 1)
      {
        for (word j = 0; j < interpreter.visual.depth; ++j)
        {
          word jk = k * max_stack_depth + j;
          word i = interpreter.visual.depth - j - 1;
          word h = interpreter.visual.stack[i];
          interpreter.background[jk][0] = (h >>  0) & 0xFF;
          interpreter.background[jk][1] = (h >>  8) & 0xFF;
          interpreter.background[jk][2] = (h >> 16) & 0xFF;
          interpreter.background[jk][3] =             0xFF;
        }
      }
    }
  }
  glActiveTexture(GL_TEXTURE0 + TEX_STACK_VISUAL);
  glTexImage2D
    ( GL_TEXTURE_2D, 0, GL_RGBA
    , max_stack_depth, interpreter.program.length, 0
    , GL_RGBA, GL_UNSIGNED_BYTE, &interpreter.background[0][0]
    );
  glActiveTexture(GL_TEXTURE0 + TEX_PROGRAM_CODE);
  glTexImage1D
    ( GL_TEXTURE_1D, 0, GL_R8UI
    , interpreter.program.length, 0
    , GL_RED_INTEGER, GL_UNSIGNED_BYTE, &interpreter.program.code[0]
    );
  glUniform1i(interpreter.cursor, interpreter.program.cursor);
  glUniform1i(interpreter.insert, interpreter.program.insert);
  glUniform1i(interpreter.file, interpreter.selected_file);
}

opcode opcode_from_codepoint(unsigned int codepoint)
{
  if (codepoint <= 0xFF)
  {
    // FIXME only 8-bit codepoints supported
    return codepoint;
  }
  else
  {
    return -1;
  }
}

bool serialize(FILE *out);

void charcb(GLFWwindow* window, unsigned int codepoint)
{
  (void) window;
  if (interpreter.input_focus == focus_default)
  {
    int op = opcode_from_codepoint(codepoint);
    if (op >= 0)
    {
      insert_code(&interpreter.program, op);
      video_update_code();
      return;
    }
  }
  fprintf(stderr, "unhandled codepoint: 0x%x\n", codepoint);
}

void do_focus(enum focus f)
{
  switch (f)
  {
    case focus_default:
      interpreter.input_focus = focus_default;
      interpreter.selected_file = -1;
      glUniform1i(interpreter.file, interpreter.selected_file);
      video_update_code();
      break;
    case focus_filelist:
      interpreter.input_focus = focus_filelist;
      interpreter.selected_file = interpreter.glob.gl_pathc - 1;
      if (interpreter.selected_file < 0)
      {
        interpreter.selected_file = 0;
      }
      glUniform1i(interpreter.file, interpreter.selected_file);
      video_update_code();
      break;
  }
}

void keycb(GLFWwindow* window, int key, int code, int action, int modifiers)
{
  (void) code;
  if (action == GLFW_PRESS)
  {
    if (interpreter.input_focus == focus_default)
    {
      switch (key)
      {
        case GLFW_KEY_LEFT:
          if (cursor_left(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_RIGHT:
          if (cursor_right(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_UP:
          if (cursor_up(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_DOWN:
          if (cursor_down(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_HOME:
          if (cursor_home(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_END:
          if (cursor_end(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_BACKSPACE:
          if (cursor_left(&interpreter.program))
          {
            delete_code(&interpreter.program);
            video_update_code();
          }
          return;
        case GLFW_KEY_DELETE:
          if (delete_code(&interpreter.program))
          {
            video_update_code();
          }
          return;
        case GLFW_KEY_INSERT:
          cursor_toggle_mode(&interpreter.program);
          video_update_code();
          return;
        case GLFW_KEY_ENTER:
          insert_code(&interpreter.program, '\n');
          video_update_code();
          return;
        case GLFW_KEY_U:
          if (modifiers & GLFW_MOD_CONTROL)
          {
            interpreter.usign = ! interpreter.usign;
            fprintf(stderr, "NOTICE: switching to %s audio\n", interpreter.usign ? "unsigned" : "signed");
          }
          return;
        case GLFW_KEY_S:
          if (modifiers & GLFW_MOD_CONTROL)
          {
            bool ok = false;
            time_t t = time(0);
            struct tm *tm = localtime(&t);
            if (tm)
            {
              char filename[100];
              if (strftime(filename, 100, "%Y-%m-%dT%H-%M-%S.bbb", tm))
              {
                FILE *out = fopen(filename, "wb");
                if (out)
                {
                  if (serialize(out))
                  {
                    fprintf(stderr, "NOTICE: saved '%s'\n", filename);
                    ok = true;
                  }
                  fclose(out);
                }
              }
            }
            if (! ok)
            {
              fprintf(stderr, "ERROR: failed to save!\n");
            }
          }
          return;
      }
    }
    switch (key)
    {
      case GLFW_KEY_ESCAPE:
        do_focus(focus_default);
        return;
      case GLFW_KEY_Q:
        if (modifiers & GLFW_MOD_CONTROL)
        {
          glfwSetWindowShouldClose(window, GL_TRUE);
        }
        return;
      case GLFW_KEY_DOWN:
        if (interpreter.input_focus == focus_filelist)
        {
          interpreter.selected_file += interpreter.glob.gl_pathc - 1;
          interpreter.selected_file %= interpreter.glob.gl_pathc + (! interpreter.glob.gl_pathc);
          glUniform1i(interpreter.file, interpreter.selected_file);
        }
        return;
      case GLFW_KEY_UP:
        if (interpreter.input_focus == focus_filelist)
        {
          interpreter.selected_file += 1;
          interpreter.selected_file %= interpreter.glob.gl_pathc + (! interpreter.glob.gl_pathc);
          glUniform1i(interpreter.file, interpreter.selected_file);
        }
        return;
      case GLFW_KEY_O:
        if (modifiers & GLFW_MOD_CONTROL)
        {
          glob("*.bbb", 0, 0, &interpreter.glob);
          if (interpreter.glob.gl_pathc > 0)
          {
            size_t maxlen = 0;
            for (size_t i = 0; i < interpreter.glob.gl_pathc; ++i)
            {
              size_t len = strlen(interpreter.glob.gl_pathv[i]);
              if (maxlen < len)
              {
                maxlen = len;
              }
            }
            char *buffer = malloc(maxlen * interpreter.glob.gl_pathc + 1);
            for (size_t i = 0; i < interpreter.glob.gl_pathc; ++i)
            {
              strncpy(buffer + i * maxlen, interpreter.glob.gl_pathv[i], maxlen);
            }
            glActiveTexture(GL_TEXTURE0 + TEX_FILES);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexImage2D
              ( GL_TEXTURE_2D, 0, GL_R8UI
              , maxlen, interpreter.glob.gl_pathc, 0
              , GL_RED_INTEGER, GL_UNSIGNED_BYTE, buffer
              );
            glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
            free(buffer);
            do_focus(focus_filelist);
          }
        }
        return;
      case GLFW_KEY_ENTER:
        if (interpreter.input_focus == focus_filelist)
        {
          if (0 <= interpreter.selected_file && (size_t) interpreter.selected_file < interpreter.glob.gl_pathc)
          {
            const char *f = interpreter.glob.gl_pathv[interpreter.selected_file];
            if (load_file(f))
            {
              fprintf(stderr, "NOTICE: loaded '%s'\n", f);
              do_focus(focus_default);
              video_update_code();
            }
          }
        }
        return;
      default:
        fprintf(stderr, "unhandled key: 0x%x\n", key);
        break;
      }
  }
}

void video_update_video()
{
  // FIXME this will only give smooth video frame rate when audio buffer size is small
  word t = ((interpreter.t / interpreter.srdivider - 0xFF) & ~0xFF) & RING_BUFFER_MASK;
  glActiveTexture(GL_TEXTURE0 + TEX_VIDEO);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 16, 16, GL_RG, GL_UNSIGNED_BYTE, &interpreter.ringbuffer[t][0]);
  // FIXME this is out of sync, latest only
  glActiveTexture(GL_TEXTURE0 + TEX_MEMORY);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 16, 16, GL_RGBA, GL_UNSIGNED_BYTE, &interpreter.memory.mem[0]);
}

void video_draw(void)
{
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


void serialize_string(FILE *out, const char *str)
{
  fprintf(out, "%d:%s", (int) strlen(str), str);
}

void serialize_double(FILE *out, double x)
{
  char buffer[100];
  snprintf(buffer, 100, "%.18e", x);
  serialize_string(out, buffer);
}

bool deserialize_string(FILE *in, char **str)
{
  word len = 0;
  if (1 == fscanf(in, "%u:", &len))
  {
    if ((*str = realloc(*str, len + 1)))
    {
      (*str)[len] = 0;
      if (0 < len)
      {
        if (1 == fread(*str, len, 1, in))
        {
          return true;
        }
      }
      else
      {
        return true;
      }
    }
  }
  return false;
}

bool deserialize_double(FILE *in, double *x)
{
  char *buf = 0;
  if (deserialize_string(in, &buf))
  {
    if (1 == sscanf(buf, "%lf", x))
    {
      free(buf);
      return true;
    }
  }
  if (buf)
  {
    free(buf);
  }
  return false;
}

bool deserialize_integer(FILE *in, word *x)
{
  return 1 == fscanf(in, "i%ue", x);
}

bool deserialize(FILE *in)
{
  bool retval = false;
  bool charsetok = false;
  bool softwareok = false;
  bool versionok = false;
  bool codeok = false;
  bool cursorok = false;
  bool insertok = false;
  bool memoryok = false;
  bool sractualok = false;
  bool srbackendok = false;
  bool srdividerok = false;
  bool srtargetok = false;
  bool timeok = false;
  bool usignok = false;
  word version = 0;
  word cursor = 0;
  word insert = 0;
  double sractual = 0;
  double srbackend = 0;
  word srdivider = 0;
  double srtarget = 0;
  word time = 0;
  word usign = 0;
  struct memory memory;
  memset(&memory, 0, sizeof(memory));
  char *key = 0;
  char *value = 0;
  char *k = 0;
  char *code = 0;
  if ('d' == fgetc(in))
  {
    while (deserialize_string(in, &key))
    {
      if (0 == strcmp(key, "CHARSET"))
      {
        if (deserialize_string(in, &value))
        {
          if (0 == strcmp(value, "ISO-8859-1"))
          {
            charsetok = true;
          }
          else
          {
            fprintf(stderr, "ERROR: unrecognized CHARSET: '%s'\n", value);
            goto fail;
          }
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable CHARSET\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "SOFTWARE"))
      {
        if (deserialize_string(in, &value))
        {
          if (0 == strcmp(value, "barry"))
          {
            softwareok = true;
          }
          else
          {
            fprintf(stderr, "ERROR: unrecognized SOFTWARE: '%s'\n", value);
            goto fail;
          }
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable SOFTWARE\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "VERSION"))
      {
        if (deserialize_integer(in, &version))
        {
          versionok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable VERSION\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "code"))
      {
        if (deserialize_string(in, &code))
        {
          codeok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable code\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "cursor"))
      {
        if (deserialize_integer(in, &cursor))
        {
          cursorok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable cursor\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "insert"))
      {
        if (deserialize_integer(in, &insert))
        {
          insertok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable insert\n");
          goto fail;
        }
      }
      else
      if (0 == strcmp(key, "memory"))
      {
        if ('l' == fgetc(in))
        {
          int c;
          while ('i' == (c = fgetc(in)))
          {
            ungetc(c, in);
            word k = 0;
            if (deserialize_integer(in, &k))
            {
              word v = 0;
              if (deserialize_integer(in, &v))
              {
                if (k < MEMORY_MASK)
                {
                  memory.mem[k] = v;
                }
                else
                {
                  fprintf(stderr, "ERROR: memory index '%u' out of range '%d'\n", k, MEMORY_MAX);
                  goto fail;
                }
              }
              else
              {
                fprintf(stderr, "ERROR: unparsable memory value\n");
                goto fail;
              }
            }
            else
            {
              fprintf(stderr, "ERROR: unparsable memory key\n");
              goto fail;
            }
          }
          ungetc(c, in);
          if ('e' == fgetc(in))
          {
            memoryok = true;
          }
          else
          {
            fprintf(stderr, "ERROR: unparsable memory end\n");
            goto fail;
          }
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable memory begin\n");
          goto fail;
        }
      }
      else if (0 == strcmp(key, "samplerate"))
      {
        if ('d' == fgetc(in))
        {
          while (deserialize_string(in, &k))
          {
            if (0 == strcmp(k, "actual"))
            {
              if (deserialize_double(in, &sractual))
              {
                sractualok = true;
              }
              else
              {
                fprintf(stderr, "ERROR: unparsable samplerate.actual\n");
                goto fail;
              }
            }
            else if (0 == strcmp(k, "backend"))
            {
              if (deserialize_double(in, &srbackend))
              {
                srbackendok = true;
              }
              else
              {
                fprintf(stderr, "ERROR: unparsable samplerate.backend\n");
                goto fail;
              }
            }
            else if (0 == strcmp(k, "divider"))
            {
              if (deserialize_integer(in, &srdivider))
              {
                srdividerok = true;
              }
              else
              {
                fprintf(stderr, "ERROR: unparsable samplerate.divider\n");
                goto fail;
              }
            }
            else if (0 == strcmp(k, "target"))
            {
              if (deserialize_double(in, &srtarget))
              {
                srtargetok = true;
              }
              else
              {
                fprintf(stderr, "ERROR: unparsable samplerate.target\n");
                goto fail;
              }
            }
            else
            {
              fprintf(stderr, "ERROR: unrecognized samplerate key '%s'\n", k);
              goto fail;
            }
          }
          if ('e' != fgetc(in))
          {
            fprintf(stderr, "ERROR: unparsable samplerate end\n");
            goto fail;
          }
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable samplerate begin\n");
          goto fail;
        }
      }
      else if (0 == strcmp(key, "time"))
      {
        if (deserialize_integer(in, &time))
        {
          timeok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable time\n");
          goto fail;
        }
      }
      else if (0 == strcmp(key, "unsigned"))
      {
        if (deserialize_integer(in, &usign))
        {
          usignok = true;
        }
        else
        {
          fprintf(stderr, "ERROR: unparsable unsigned\n");
          goto fail;
        }
      }
      else
      {
        fprintf(stderr, "ERROR: unrecognized key '%s'\n", key);
        goto fail;
      }
    }
    if ('e' != fgetc(in))
    {
      fprintf(stderr, "ERROR: unparsable end\n");
      goto fail;
    }
  }
  else
  {
    fprintf(stderr, "ERROR: unparsable begin\n");
    goto fail;
  }
  if (EOF != fgetc(in))
  {
    fprintf(stderr, "WARNING: junk at end of file\n");
  }
  // check ok
  bool ok =
    charsetok && softwareok && versionok &&
    codeok && cursorok && insertok && memoryok &&
    sractualok && srbackendok && srdividerok && srtargetok &&
    timeok && usignok;
  if (ok && version == 1 && srdivider > 0)
  {
    retval = true;
  }
fail:
  if (retval)
  {
    // all ok, update state
    // FIXME make state updates more atomic, double buffering?
    interpreter.program.cursor = 0;
    interpreter.program.length = 0;
    word length = strlen(code);
    for (word i = 0; i < length; ++i)
    {
      interpreter.program.code[i] = opcode_from_codepoint((unsigned char) code[i]);
    }
    if (interpreter.srbackend != srbackend)
    {
      fprintf(stderr, "WARNING: backend samplerate mismatch: now %f, file %f\n", interpreter.srbackend, srbackend);
    }
    word srdivider2 = fmax(1, round(interpreter.srbackend / srtarget));
    if (srdivider2 != srdivider)
    {
      fprintf(stderr, "WARNING: samplerate divider changed: now %d, file %d\n", srdivider2, srdivider);
    }
    double sractual2 = interpreter.srbackend / srdivider2;
    if (sractual2 != sractual)
    {
      fprintf(stderr, "WARNING: actual samplerate changed: now %f, file %f\n", sractual2, sractual);
    }
    if (sractual2 != srtarget)
    {
      fprintf(stderr, "WARNING: samplerate differs from target: have %f, want %f\n", sractual2, srtarget);
    }
    interpreter.srtarget = srtarget;
    interpreter.srdivider = srdivider2;
    interpreter.sractual = sractual2;
    interpreter.memory = memory;
    interpreter.usign = usign;
    interpreter.program.insert = insert;
    interpreter.program.cursor = cursor;
    interpreter.program.length = length;
    interpreter.t = time;
  }
  // cleanup
  if (key) free(key);
  if (value) free(value);
  if (k) free(k);
  if (code) free(code);
  return retval;
}

bool load_file(const char *filename_to_load)
{
  FILE *in = fopen(filename_to_load, "rb");
  if (in)
  {
    if (! deserialize(in))
    {
      fprintf(stderr, "ERROR: could not deserialize '%s'\n", filename_to_load);
      return false;
    }
    fclose(in);
  }
  else
  {
    fprintf(stderr, "ERROR: could not open '%s' for reading\n", filename_to_load);
    return false;
  }
  return true;
}

bool serialize(FILE *out)
{
  fputc('d', out);
  serialize_string(out, "CHARSET");
  serialize_string(out, "ISO-8859-1");
  serialize_string(out, "SOFTWARE");
  serialize_string(out, "barry");
  serialize_string(out, "VERSION");
  fputs("i1e", out);
  serialize_string(out, "code");
  fprintf(out, "%u:", interpreter.program.length);
  for (word i = 0; i < interpreter.program.length; ++i)
  {
    fputc(interpreter.program.code[i], out);
  }
  serialize_string(out, "cursor");
  fprintf(out, "i%ue", interpreter.program.cursor);
  serialize_string(out, "insert");
  fprintf(out, "i%ue", interpreter.program.insert);

  // only store non-zero memory values to save space
  serialize_string(out, "memory");
  fputc('l', out);
  for (word i = 0; i < MEMORY_MAX; ++i)
  {
    word m = interpreter.memory.mem[i];
    if (m != 0)
    {
      fprintf(out, "i%uei%ue", i, m);
    }
  }
  fputc('e', out);

  serialize_string(out, "samplerate");
  fputc('d', out);
  serialize_string(out, "actual");
  serialize_double(out, interpreter.sractual);
  serialize_string(out, "backend");
  serialize_double(out, interpreter.srbackend);
  serialize_string(out, "divider");
  fprintf(out, "i%de", interpreter.srdivider);
  serialize_string(out, "target");
  serialize_double(out, interpreter.srtarget);
  fputc('e', out);

  serialize_string(out, "time");
  fprintf(out, "i%ue", interpreter.t / interpreter.srdivider);
  serialize_string(out, "unsigned");
  fprintf(out, "i%ue", interpreter.usign);

  fputc('e', out);
  return true;
}


int main(int argc, char **argv)
{
  while (1)
  {
    int option_index = 0;
    static struct option long_options[] =
      { { "help",    no_argument,       0, 'h' }
      , { "version", no_argument,       0, 'v' }
      , { "source",  no_argument,       0, 'S' }
      , { 0,         0,                 0,  0  }
      };
    int opt = getopt_long(argc, argv, "hH?vVS", long_options, &option_index);
    if (opt == -1) break;
    switch (opt)
    {
      case 'h':
      case 'H':
      case '?':
        printf(
          "barry -- bytebeat livecoding environment\n"
          "Copyright (C) 2020  Claude Heiland-Allen\n"
          "License: GNU AGPLv3+\n"
          "\n"
          "Usage:\n"
          "    %s\n"
          "        start new session\n"
          "    %s FILE.bbb\n"
          "        open previous session\n"
          "    %s -?,-h,-H,--help\n"
          "        output this message\n"
          "    %s -v,-V,--version\n"
          "        output version string\n"
          "    %s -S,--source\n"
          "        output %s's source code to the current working directory\n"
          "        files written:\n"
          "            barry.c\n"
          "            barry.png\n"
          "            alphabet.utf8\n"
          "            Makefile\n"
          "            README.md\n"
          "            LICENSE.md\n"
          "\n"
          "Keys:\n"
          "    Ctrl-Q  quit\n"
          "    Ctrl-S  save session to timestamped file\n"
          "    Ctrl-O  open session from current working directory\n"
          "    Ctrl-U  toggles signed vs unsigned audio\n"
          , argv[0], argv[0], argv[0], argv[0], argv[0], argv[0]
          );
        return 0;
      case 'v':
      case 'V':
        printf("%d\n", 1);
        return 0;
      case 'S':
        return ! write_files();
    }
  }
  const char *filename_to_load = 0;
  if (optind + 1 < argc)
  {
    fprintf(stderr, "%s: error: too many arguments\n", argv[0]);
    return 1;
  }
  else if (optind + 1 == argc)
  {
    filename_to_load = argv[optind];
  }

  memset(&interpreter, 0, sizeof(interpreter));
  memset(&interpreter.program.code[0], -1, sizeof(interpreter.program.code));

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#if 1
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_TRUE);
  glfwWindowHint(GLFW_FLOATING, GL_TRUE);
  GLFWwindow *window = glfwCreateWindow(screen_width, screen_height, "barry", 0, 0);
  assert(window);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glfwSetKeyCallback(window, keycb);
  glfwSetCharCallback(window, charcb);
//  glfwSetCursorPosCallback(window, motioncb);
//  glfwSetMouseButtonCallback(window, buttoncb);

  if (!(interpreter.client = jack_client_open("barry", JackNoStartServer, 0)))
  {
    fprintf(stderr, "jack server not running?\n");
    return 1;
  }
  jack_set_process_callback(interpreter.client, processcb, 0);

  // compute sample rate divider
  interpreter.srtarget = 8000;
  interpreter.srbackend = jack_get_sample_rate(interpreter.client);
  interpreter.srdivider = round(interpreter.srbackend / interpreter.srtarget);
  if (interpreter.srdivider < 1) interpreter.srdivider = 1;
  interpreter.sractual = interpreter.srbackend / interpreter.srdivider;
  fprintf(stderr, "JACK sample rate: %f\n", interpreter.srbackend);
  fprintf(stderr, "want sample rate: %f\n", interpreter.srtarget);
  fprintf(stderr, "have sample rate: %f\n", interpreter.sractual);
  fprintf(stderr, "sample rate divider: %d\n", interpreter.srdivider);

  if (filename_to_load)
  {
    if (! load_file(filename_to_load))
    {
      return 1;
    }
  }

  // multi-channel processing
  for (int c = 0; c < OUTCHANNELS; ++c)
  {
    char portname[100];
    snprintf(portname, 100, "output_%d", c + 1);
    interpreter.out[c] = jack_port_register(interpreter.client, portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  }
  if (jack_activate(interpreter.client))
  {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  // stereo output to system
  jack_connect(interpreter.client, "barry:output_1", "system:playback_1");
  jack_connect(interpreter.client, "barry:output_2", "system:playback_2");

  video_initialize();
  video_update_code();
  do_focus(focus_default);

  int frame = 0;
  while (1)
  {
    glfwPollEvents();
    if (glfwWindowShouldClose(window)) { break; }

    video_update_video();
    video_draw();
    glfwSwapBuffers(window);

    GLint e = glGetError();
    if (e)
      fprintf(stderr, "OpenGL ERROR %d\n", e);

    frame++;
  }

  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
