{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Parse where

import Data.Char (isSpace)
import qualified Data.Set as Set
import Text.Parsec hiding (token)

import TopSort (TopSort(..))

token = try (spaces >> many (satisfy (not . isSpace)))
literal s = try (spaces >> string s)

data Definition = Definition String LocalSpec [String]
  deriving Show

instance TopSort String Definition where
  key (Definition name _ _) = name
  depends (Definition _ (LocalSpec is vs _) words) =
    Set.fromList words `Set.difference` Set.fromList (is ++ vs)

definition = do
  literal ":"
  name <- token
  spec <- localSpecification
  code <- token `manyTill` literal ";"
  pure $ Definition name spec code

data C_ABI = C_ABI String TypeSpec String
  deriving Show

c_abi = do
  literal "C-ABI"
  name <- token
  spec <- typeSpecification
  word <- token
  literal ";"
  pure $ C_ABI name spec word

data LocalSpec = LocalSpec [String] [String] [String]
  deriving Show

localSpecification = do
  literal "{"
  argsVars <- token `manyTill` literal "--"
  let (args, vars1) = span (/= "|") argsVars
      vars = drop 1 vars1
  outs <- token `manyTill` literal "}"
  pure $ LocalSpec args vars outs

data TypeSpec = TypeSpec [String] [String]
  deriving Show

typeSpecification = do
  literal "{"
  args <- token `manyTill` literal "--"
  outs <- token `manyTill` literal "}"
  pure $ TypeSpec args outs

data Document = Document [Definition] [C_ABI]
  deriving Show

document = do
  ins <- many definition
  outs <- many c_abi
  spaces
  eof
  pure $ Document ins outs

class Unparse a where
  unparse :: a -> String

instance Unparse Document where
  unparse (Document defs cabis) =
    unlines (map unparse defs ++ map unparse cabis)

instance Unparse TypeSpec where
  unparse (TypeSpec ins outs) =
    "{ " ++ unwords ins ++ " -- " ++ unwords outs ++ " }"

instance Unparse C_ABI where
  unparse (C_ABI name typeSpec word) =
    "C-ABI " ++ name ++ " " ++ unparse typeSpec ++ " " ++ word ++ " ;"

instance Unparse LocalSpec where
  unparse (LocalSpec ins vars outs) =
    "{ " ++ unwords ins ++
      (if null vars then "" else " | " ++ unwords vars) ++
        " -- " ++ unwords outs ++ " }"

instance Unparse Definition where
  unparse (Definition name localSpec words) =
    ": " ++ name ++ " " ++
      unparse localSpec ++ " " ++
        unwords words ++ " ;"
