{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module TopSort
  ( TopSort(key, depends)
  , TopSorted()
  , toList
  , null
  , elem
  , empty
  , singleton
  , insert
  ) where

import qualified Prelude
import Prelude (Ord, Bool, all, map, zip, (++), (==), ($), (.))

import Data.List (inits, tails)
import Data.Maybe
import qualified Data.Set as Set
import Data.Set (Set)

class Ord k => TopSort k v | v -> k where
  key :: v -> k
  depends :: v -> Set k

newtype TopSorted v = TopSorted [v]

null :: TopSorted v -> Bool
-- asymptotics: O(1)
null (TopSorted vs) = Prelude.null vs

empty :: TopSorted v
-- asymptotics: O(1)
empty = TopSorted []

singleton :: v -> TopSorted v
-- asymptotics: O(1)
singleton v = TopSorted [v]

toList :: TopSorted v -> [v]
-- asymptotics: O(1)
toList (TopSorted vs) = vs

elem :: TopSort k v => v -> TopSorted v -> Bool
-- asymptotics: O(n)
elem v (TopSorted vs) = key v `Prelude.elem` map key vs

insert :: TopSort k v => v -> TopSorted v -> TopSorted v
-- asymptotics: O(n^3)
insert v (TopSorted vs) = case splitLookup v vs of
  Nothing -> TopSorted (v `bubbleIn` vs)
  Just (pre, _, post) -> TopSorted (bubble (pre ++ [v] ++ post))

bubble :: TopSort k v => [v] -> [v]
-- asymptotics: O(n^3)
-- postcondition: topsorted
bubble [] = []
bubble (x:xs) = x `bubbleIn` bubble xs

bubbleIn :: TopSort k v => v -> [v] -> [v]
-- asymptotics: O(n^2)
-- precondition: topsorted
-- postcondition: topsorted
bubbleIn x xs | isTopSorted1 ([], x, xs) = x : xs
bubbleIn x (y : ys) = y : bubbleIn x ys

splitLookup :: TopSort k v => v -> [v] -> Maybe ([v], v, [v])
-- asymptotics: O(n^2)
splitLookup x xs = listToMaybe
    [ (pre, x', post) | (pre, x', post) <- splits xs, key x == key x' ]

splits :: [a] -> [([a], a, [a])]
-- asymptotics: O(n^2)
splits xs =
    [ (pre, x, post) | (pre, x:post) <- inits xs `zip` tails xs ]

isTopSorted1 :: TopSort k v => ([v], v, [v]) -> Bool
-- asymptotics: O(n)
isTopSorted1 (_, x, post) = Set.null $
    depends x `Set.intersection` Set.fromList (map key post)

isTopSorted :: TopSort k v => [v] -> Bool
-- asymptotics: O(n^2)
isTopSorted = all isTopSorted1 . splits
