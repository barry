{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Main (main) where

import Data.Char (isSpace)
import qualified Data.Map as Map
import qualified Data.Set as Set
import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import Text.Parsec (parse)

import Parse
import qualified TopSort

block_start = ":{"
block_end = ":}"

main = do
  args <- getArgs
  case args of
    [outputfile] -> outsideBlock outputfile document empty
    _ -> do
      hPutStrLn stderr "ERROR: missing output file argument"
      exitFailure

outsideBlock outputfile parser dict = do
  l <- getLine
  if l == block_start
    then insideBlock outputfile parser dict []
    else do
      putStrLn l
      outsideBlock outputfile parser dict

insideBlock outputfile parser dict acc = do
  l <- getLine
  if l == block_end
    then case parse parser "-" (unlines $ reverse acc) of
      Right kvs -> do
        let dict' = merge kvs dict
        writeFile outputfile . unparse $ dict'
        outsideBlock outputfile parser dict'
      Left err -> do
        hPutStrLn stderr $ "ERROR: " ++ show err
        outsideBlock outputfile parser dict
    else insideBlock outputfile parser dict (l : acc)

data TopDoc = TopDoc (TopSort.TopSorted Definition) [C_ABI]

instance Unparse TopDoc where
  unparse (TopDoc defs cabis) =
    unparse (Document (TopSort.toList defs) cabis)

empty = TopDoc TopSort.empty []

merge (Document newColon newCABI) (TopDoc oldColon oldCABI) =
    ( TopDoc (foldr TopSort.insert oldColon newColon)
      (newCABI ++ (oldCABI `deleteCABI` newCABI))
    )

olds `deleteCABI` news =
    [ old | old@(C_ABI name _ _) <- olds, name `Set.notMember` newNames ]
  where newNames = Set.fromList [ name | C_ABI name _ _ <- news ]
