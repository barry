{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

import Prelude hiding (lookup, Word)

import Control.Monad (foldM, replicateM, unless)
import Control.Monad.Except (MonadError(throwError), ExceptT, runExceptT)
import Control.Monad.Identity (Identity, runIdentity)
import Control.Monad.State (MonadState(get, put), StateT, evalStateT)
import Control.Monad.Writer (MonadWriter(tell), WriterT, runWriterT)
import Data.Char (isSpace)
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import System.IO (hPutStrLn, stderr)
import Text.Read (readMaybe)

data Atom = AInt | AFloat | ASymbol
  deriving (Show, Eq, Ord)

data Constraint v = CConstraint String (Type v)
  deriving (Show, Eq, Ord)

data Function v = FFunction (Set v) (Set (Constraint v)) [Type v] [Type v]
  deriving (Show, Eq, Ord)

delta :: Function v -> Int
delta (FFunction _ _ pre post) = length post - length pre

data Type v = TVariable v | TAtomic Atom | TFunction (Function v)
  deriving (Show, Eq, Ord)

isVariable :: Type v -> Bool
isVariable (TVariable _) = True
isVariable _ = False

data Equation v = Equation (Type v) (Type v)
  deriving (Show, Eq, Ord)

lhs, rhs :: Equation v -> Type v
lhs (Equation l _) = l
rhs (Equation _ r) = r

data Substitution v = v :-> Type v
  deriving (Show, Eq, Ord)

class Substitute t where
  substitute :: Ord v => Substitution v -> t v -> t v
  substitutes :: Ord v => Map v (Type v) -> t v -> t v

instance Substitute Constraint where
   substitute s (CConstraint name t) = (CConstraint name $ substitute s t)
   substitutes s (CConstraint name t) = (CConstraint name $ substitutes s t)

instance Substitute Function where
  substitute s@(v :-> _) f@(FFunction vars constraints pre post)
    | v `Set.member` vars = f
    | otherwise = FFunction vars constraints
        (substitute s `map` pre)
        (substitute s `map` post)
  substitutes s f@(FFunction vars constraints pre post)
    = FFunction vars (substitutes s' `Set.map` constraints)
        (substitutes s' `map` pre)
        (substitutes s' `map` post)
    where
      s' = s `Map.withoutKeys` vars

instance Substitute Type where
  substitute (v :-> t) s@(TVariable u)
    | u == v = t
    | otherwise = s
  substitute _ a@(TAtomic _) = a
  substitute s (TFunction f) = TFunction (substitute s f)
  substitutes s t@(TVariable u) = case Map.lookup u s of
    Just v -> v
    Nothing -> t
  substitutes _ a@(TAtomic _) = a
  substitutes s (TFunction f) = TFunction (substitutes s f)

instance Substitute Equation where
   substitute s (Equation l r) = Equation (substitute s l) (substitute s r)
   substitutes s (Equation l r) = Equation (substitutes s l) (substitutes s r)

class Variables t where
  variables :: Ord v => t v -> Set v

instance Variables Type where
  variables (TVariable v) = Set.singleton v
  variables (TAtomic _) = Set.empty
  variables (TFunction f) = variables f

instance Variables Function where
  variables (FFunction vars constraints pre post) =
    Set.unions (map variables (pre ++ post)) Set.\\ vars

instance Variables Equation where
  variables (Equation l r) = variables l `Set.union` variables r

data Step v
  = SDelete (Equation v)
  | SSwap (Equation v)
  | SDecompose (Equation v)
  | SEliminate (Equation v)
  deriving (Show, Eq, Ord)

class (Monad m, Ord v) => MonadUnify v m | m -> v where
  fresh :: m v
  step :: Step v -> Set (Equation v) -> m (Set (Equation v))
  conflict :: Equation v -> m a
  infinite :: Equation v -> m a
  underflow :: m a
  overflow :: m a

delete :: MonadUnify v m => Equation v -> m (Set (Equation v))
delete e@(Equation l r)
  | l == r = step (SDelete e) Set.empty
  | otherwise = pure (Set.singleton e)

deletes :: MonadUnify v m => Set (Equation v) -> m (Set (Equation v))
deletes = fmap Set.unions . mapM delete . Set.toList

decompose :: MonadUnify v m => Equation v -> m (Set (Equation v))
{-
l :: { a b c -- d e }
r :: {   u v --   x }
add free variable padding
r :: { p u v -- p x }
l ~ r => a ~ p , b ~ u, c ~ v, d ~ p, e ~ x
-}
decompose e@(Equation (TVariable _) _) = pure (Set.singleton e)
decompose e@(Equation _ (TVariable _)) = pure (Set.singleton e)
decompose e@(Equation a@(TAtomic _) b@(TAtomic _))
  | a == b = pure (Set.singleton e)
decompose e@(Equation (TFunction f@(FFunction _ _ lpre lpost))
                      (TFunction g@(FFunction _ _ rpre rpost)))
  | delta f == delta g = do
      let ldiff = max 0 $ length rpre - length lpre
          rdiff = max 0 $ length lpre - length rpre
      lpad <- map TVariable <$> replicateM ldiff fresh
      rpad <- map TVariable <$> replicateM rdiff fresh
      let e' = Set.fromList $
                  zipWith Equation (lpre ++ lpad) (rpre ++ rpad) ++
                  zipWith Equation (lpost ++ lpad) (rpost ++ rpad)
      step (SDecompose e) e'
decompose e = conflict e

decomposes :: MonadUnify v m => Set (Equation v) -> m (Set (Equation v))
decomposes = fmap Set.unions . mapM decompose . Set.toList

swap :: MonadUnify v m => Equation v -> m (Set (Equation v))
swap e@(Equation (TVariable _) _) = pure (Set.singleton e)
swap e@(Equation l r@(TVariable _)) = do
  let e' = Set.singleton (Equation r l)
  step (SSwap e) e'
swap e = pure (Set.singleton e)

swaps :: MonadUnify v m => Set (Equation v) -> m (Set (Equation v))
swaps = fmap Set.unions . mapM swap . Set.toList

eliminates :: MonadUnify v m => Set (Equation v) -> m (Set (Equation v))
eliminates eqs = head $
  [ step (SEliminate e) e'
  | e@(Equation (TVariable l) r) <- Set.toList eqs
  , l `Set.notMember` variables r
  , let otherEqs = Set.delete e eqs
  , l `Set.member` (Set.unions . map variables . Set.toList) otherEqs
  , let s = l :-> r
  , let e' = Set.insert e . Set.map (substitute s) $ otherEqs
  ] ++ [pure eqs]

occursCheck :: MonadUnify v m => Equation v -> m (Set (Equation v))
occursCheck e@(Equation (TVariable _) (TVariable _)) = pure (Set.singleton e)
occursCheck e@(Equation (TVariable l) r)
  | l `Set.member` variables r = infinite e
occursCheck e = pure (Set.singleton e)

occursChecks :: MonadUnify v m => Set (Equation v) -> m (Set (Equation v))
occursChecks = fmap Set.unions . mapM occursCheck . Set.toList

substitution :: MonadUnify v m => Set (Equation v) -> m (Map v (Type v))
substitution eqs =
  if any (not . isVariable . lhs) (Set.toList eqs)
  then error "not a substitution"
  else pure $ Map.fromList
         [ (k, v) | Equation (TVariable k) v <- Set.toList eqs ]

unifyM :: MonadUnify v m => Set (Equation v) -> m (Map v (Type v))
unifyM eqs0 = do
  eqs <- deletes eqs0
  eqs <- decomposes eqs
  eqs <- swaps eqs
  eqs <- eliminates eqs
  eqs <- occursChecks eqs
  if eqs == eqs0
    then substitution eqs
    else unifyM eqs

data Error v
  = Underflow
  | Overflow
  | Conflict (Equation v)
  | Infinite (Equation v)
  deriving (Show, Eq, Ord)

newtype UnifyT v m a
  = Unify (ExceptT (Error v) (StateT v (WriterT [Step v] m)) a)
  deriving (Functor, Applicative, Monad)
deriving instance Monad m => MonadError (Error v) (UnifyT v m)
deriving instance Monad m => MonadState v (UnifyT v m)
deriving instance Monad m => MonadWriter [Step v] (UnifyT v m)

instance (Ord v, Num v, Monad m) => MonadUnify v (UnifyT v m) where
  fresh = do
    n <- get
    put (n + 1)
    pure n
  step tag e = do
    tell [tag]
    pure e
  conflict e = throwError (Conflict e)
  infinite e = throwError (Infinite e)
  underflow = throwError Underflow
  overflow = throwError Overflow

runUnifyT
  :: (Ord v, Num v, Monad m)
  => UnifyT v m a -> m (Either (Error v) a, [Step v])
runUnifyT (Unify u) = runWriterT . flip evalStateT 0 . runExceptT $ u

unify
  :: (Ord v, Num v)
  => Set (Equation v) -> (Either (Error v) (Map v (Type v)), [Step v])
unify = runIdentity . runUnifyT . unifyM

rename :: MonadUnify v m => Function v -> m (Function v)
rename (FFunction vars constraints pre post) = do
  let var v = (,) v `fmap` fresh
  t <- fmap Map.fromList . mapM var . Set.toList $ vars
  let s = fmap TVariable t
  pure (FFunction Set.empty (substitutes s `Set.map` constraints)
          (substitutes s `map` pre) (substitutes s `map` post))

typeCheck
  :: (Show v, MonadUnify v m)
  => Function v -> [Function v] -> m (Function v, [Function v])
typeCheck f fs = do
  f@(FFunction vars constraints pre post) <- rename f
  fs <- mapM rename fs
  (constraints, result, eqs) <- foldM equating (constraints, pre, Set.empty) fs
  eq <- equateExact result post
  let e = eq `Set.union` eqs
  s <- unifyM e
  pure (substitutes s f, substitutes s `map` fs)

equateExact result post
  | length result > length post = overflow
  | length result < length post = underflow
  | otherwise = pure . Set.fromList $ zipWith Equation result post

equating (constraints, stack, eqs) (FFunction _ constraints2 pre post)
  | length stack < length pre = underflow
  | otherwise = pure
      ( Set.union constraints constraints2
      , post ++ drop (length pre) stack
      , Set.fromList (zipWith Equation stack pre) `Set.union` eqs
      )

toplevelConstraints
  :: Ord v => (Function v, [Function v]) -> (Function v, [Function v])
toplevelConstraints (f, fs) =
  let constraintsOf (FFunction _ c _ _) = c
      (FFunction vars _ pre post) `withConstraints` c
        = FFunction vars c pre post
      cs = Set.unions (constraintsOf f : map constraintsOf fs)
      for = flip map
  in  (f `withConstraints` cs, fs `for` (`withConstraints` Set.empty))

monomorphic :: (Ord v, Variables t) => t v -> Bool
monomorphic = Set.null . variables

{-
-- test

class Pretty t where pretty :: t -> String

instance Show v => Pretty (Constraint v) where
  pretty (CConstraint s t) = s ++ " (" ++ pretty t ++ ")"

instance Show v => Pretty (Function v) where
  pretty (FFunction vars constraints pre post) =
    (if Set.null vars then "" else
      "forall " ++ (intercalate " " . map show . Set.toList) vars ++ " . ") ++
    (if Set.null constraints then "" else
      (intercalate " " . map pretty . Set.toList) constraints ++ " => ") ++
    "( " ++ p pre ++ " -- " ++ p post ++ " )"
    where
      p = intercalate " " . map pretty . reverse

instance Pretty Atom where
  pretty AInt = "Int"
  pretty AFloat = "Float"
  pretty ASymbol = "Symbol"

instance Show v => Pretty (Type v) where
  pretty (TVariable v) = show v
  pretty (TAtomic a) = pretty a
  pretty (TFunction f) = pretty f

v = TVariable
i = TAtomic AInt
f = TAtomic AFloat
s = TAtomic ASymbol
c name = Set.singleton (CConstraint name (v 0))

t_id, t_dup, t_swap, t_add, t_sub, t_mul, t_div, t_mod,
 t_liti, t_litf, t_casti, t_castf, t_print :: Function Int
t_id    = FFunction (Set.empty) (Set.empty) [] []
t_dup   = FFunction (Set.singleton 0) (Set.empty) [v 0] [v 0, v 0]
t_swap  = FFunction (Set.fromList [0, 1]) (Set.empty) [v 0, v 1] [v 1, v 0]
t_add   = FFunction (Set.singleton 0) (c "Add") [v 0, v 0] [v 0]
t_sub   = FFunction (Set.singleton 0) (c "Sub") [v 0, v 0] [v 0]
t_mul   = FFunction (Set.singleton 0) (c "Mul") [v 0, v 0] [v 0]
t_div   = FFunction (Set.singleton 0) (c "Div") [v 0, v 0] [v 0]
t_mod   = FFunction (Set.singleton 0) (c "Mod") [v 0, v 0] [v 0]
t_liti  = FFunction (Set.empty) (Set.empty) [] [i]
t_litf  = FFunction (Set.empty) (Set.empty) [] [f]
t_casti = FFunction (Set.singleton 0) (c "Int") [v 0] [i]
t_castf = FFunction (Set.singleton 0) (c "Float") [v 0] [f]
t_print = FFunction (Set.singleton 0) (c "Show") [v 0] []

{-
(testf, testfs) = (t_swap, [ t_swap, t_dup, t_swap, t_dup, t_add, t_castf, t_swap, t_castf, t_mul, t_print, t_print, t_liti ])
(tests, testss) = ("test",  [ "swap", "dup", "swap", "dup", "add", "castf", "swap", "castf", "mul", "print", "print", "liti" ])
-}
(testf, testfs) = (t_swap, [ t_dup, t_add, t_swap, t_dup, t_mul, t_swap, t_swap, t_div, t_dup ])
(tests, testss) = ("test", [ "dup", "add", "swap", "dup", "mul", "swap", "swap", "div", "dup" ])
test = runIdentity . runUnifyT $ typeCheck testf testfs

main = do
  pp tests testf >> sequence_ (zipWith pp testss testfs)
  case test of
    (Left e, _) -> print e
    (Right (f, fs), _) -> do
      (f, fs) <- pure $ toplevelConstraints (f, fs)
      pp tests f >> sequence_ (zipWith pp testss fs)
  where pp name typ = putStrLn (name ++ " :: " ++ pretty typ)

-}

data Cell
  = I Int
  | D Double
  | ColonDef String [Code]
  | LocalDef [String] [String] [String]
  | ExecutionToken (Forth -> IO Forth)

data Mode = Interpret | Compile

appendCode f c = do
  (item, f) <- pop f
  case item of
    Just (ColonDef name code) -> push f (ColonDef name (code ++ c))
    _ -> err f "no colon def token"

data Word = Word
  { compilationSemantics    :: Forth -> IO Forth
  , executionSemantics      :: Forth -> IO Forth
  , interpretationSemantics :: Forth -> IO Forth
  }

data Code
  = PushInt Int
  | PushDouble Double
  | PushExecutionToken (Forth -> IO Forth)
  | Execute
  | Add
  | Sub
  | Mul
  | Div
  | Shl
  | Shr
  | And
  | Or
  | Xor
  | Inv
  | Eq
  | Gt
  | U8
  | PushLocalValue
  | PushLocalNoValue
  | LocalFetch Int
  | LocalStore Int

int num = codeSemantics [PushInt num]
double num = codeSemantics [PushDouble num]
execute = codeSemantics [Execute]

codeSemantics code = Word
  { compilationSemantics = \f -> appendCode f code
  , executionSemantics = \f -> interpretCode f code
  , interpretationSemantics = \f -> interpretCode f code
  }

enter :: [Code]
enter = []

exit :: [Code]
exit = []

colon = Word
  { compilationSemantics = \f -> err f "can't compile colon"
  , executionSemantics = execColon
  , interpretationSemantics = execColon
  }

execColon f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  f <- setMode f Compile
  push f (ColonDef name enter)

colonNoName = Word
  { compilationSemantics = \f -> err f "can't compile colon noname"
  , executionSemantics = execColonNoName
  , interpretationSemantics = execColonNoName
  }

execColonNoName f = do
  f <- setMode f Compile
  push f (ColonDef "" enter)

alignDataSpacePointer f = pure f

semicolon = Word
  { compilationSemantics = compileSemicolon
  , executionSemantics = \f -> err f "can't execute semicolon"
  , interpretationSemantics = \f -> err f "can't interpret semicolon"
  }

compileSemicolon f = do
  f <- clearLocals f
  (item, f) <- pop f
  f <- case item of
    Just (ColonDef "" code) -> push f (ExecutionToken (executionSemantics (codeSemantics code)))
    Just (ColonDef name code) -> insert f name (codeSemantics code)
  f <- setMode f Interpret
  alignDataSpacePointer f

tick = Word
  { compilationSemantics = \f -> err f "can't compile tick"
  , executionSemantics = execTick
  , interpretationSemantics = execTick
  }

execTick f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case lookup f name of
    Just word -> push f (ExecutionToken (executionSemantics word))
    _ -> err f "can't tick that"

boxTick = Word
  { compilationSemantics = compileBoxTick
  , executionSemantics = \f -> err f "can't execute box tick"
  , interpretationSemantics = \f -> err f "can't interpret box tick"
  }

compileBoxTick f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case lookup f name of
    Just word -> appendCode f [PushExecutionToken (executionSemantics word)]
    _ -> err f "can't box tick that"

openBrace = Word
  { compilationSemantics = compileOpenBrace
  , executionSemantics = \f -> err f "can't execute open brace"
  , interpretationSemantics = \f -> err f "can't interpret open brace"
  }

compileOpenBrace f = do
  f <- push f (LocalDef [] [] [])
  compileLocalArguments f

compileLocalArguments f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case name of
    "" -> err f "braced eof"
    "}" -> err f "braces without spec"
    "|" -> compileLocalVariables f
    "--" -> compileOutputSpecifications f
    name -> do
      f <- compileLocalArgument f name
      compileLocalArguments f

compileLocalVariables f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case name of
    "" -> err f "braced eof"
    "}" -> err f "braces without spec"
    "|" -> err f "braced double bar"
    "--" -> compileOutputSpecifications f
    name -> do
      f <- compileLocalVariable f name
      compileLocalVariables f

compileOutputSpecifications f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case name of
    "" -> err f "braced eof"
    "}" -> do
      (item, f) <- pop f
      case item of
        Just (LocalDef args vars outs) -> do
          f <- foldM addLocalArg f $ zip [0..] args
          f <- foldM addLocalVar f $ zip [length args..] vars
          pure f
        _ -> err f "no local def"
    "|" -> err f "braced double bar"
    name -> do
      f <- compileOutputSpecification f name
      compileOutputSpecifications f

addLocalArg f (i, name) = do
  f <- insertLocal f name i
  appendCode f [PushLocalValue]

addLocalVar f (i, name) = do
  f <- insertLocal f name i
  appendCode f [PushLocalNoValue]

compileLocalArgument f name = do
  (item, f) <- pop f
  case item of
    Just (LocalDef args vars outs) -> do
      push f (LocalDef (name:args) vars outs)
    _ -> err f "no local def"

compileLocalVariable f name = do
  (item, f) <- pop f
  case item of
    Just (LocalDef args vars outs) -> do
      push f (LocalDef args (name:vars) outs)
    _ -> err f "no local def"

compileOutputSpecification f name = do
  (item, f) <- pop f
  case item of
    Just (LocalDef args vars outs) -> do
      push f (LocalDef args vars (name:outs))
    _ -> err f "no local def"

builtins = Map.fromList
  [ (":", colon)
  , (":NONAME", colonNoName)
  , (";", semicolon)
  , ("'", tick)
  , ("[']", boxTick)
  , ("{", openBrace)
  , ("->", arrow)
  , ("EXECUTE", codeSemantics [Execute])
  , ("&",  codeSemantics [And])
  , ("|",  codeSemantics [Or])
  , ("^",  codeSemantics [Xor])
  , ("+",  codeSemantics [Add])
  , ("-",  codeSemantics [Sub])
  , ("*",  codeSemantics [Mul])
  , ("/",  codeSemantics [Div])
  , ("~",  codeSemantics [Inv])
  , ("<<", codeSemantics [Shl])
  , (">>", codeSemantics [Shr])
  , ("==", codeSemantics [Eq])
  , (">",  codeSemantics [Gt])
  , ("u8", codeSemantics [U8])
  ]

data Forth
  = Forth
    { input :: String
    , mode :: Mode
    , dict :: Map String Word
    , local :: Map String Int
    , stack :: [Cell]
    }

clearLocals f = pure $ f { local = Map.empty }

insertLocal f name i = pure $ f { local = Map.insert name i (local f) }

forth str = Forth
  { input = str
  , mode = Interpret
  , dict = builtins
  , local = Map.empty
  , stack = []
  }

setMode f m = pure $ f { mode = m }

data Number = Int Int | Double Double

dropSpace f = case span isSpace (input f) of
  (spaces, rest) -> do
    putStr spaces
    pure $ f{ input = rest }

breakSpace f = case break isSpace (input f) of
  (name, rest) -> do
    putStr name
    pure (name, f{ input = rest })

arrow = Word
  { compilationSemantics = compileArrow
  , executionSemantics = \f -> err f "can't execute arrow"
  , interpretationSemantics = \f -> err f "can't interpret arrow"
  }


compileArrow f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  case Map.lookup name (local f) of
    Just i -> compilationSemantics (codeSemantics [LocalStore i]) f
    _ -> err f $ "local " ++ name ++ " not found"

lookup f name = case Map.lookup name (local f) of
  Just i -> Just $ codeSemantics [LocalFetch i]
  _ -> Map.lookup name (dict f)

insert f name word = pure $ f { dict = Map.insert name word (dict f) }

compileOrInterpret f = case mode f of
  Interpret -> interpret f
  Compile -> compile f

compile f word = compilationSemantics word f

interpret f word = interpretationSemantics word f

interpretCode f = foldM interpretCode1 f

interpretCode1 f (PushInt i) = push f (I i)
interpretCode1 f (PushDouble d) = push f (D d)

interpretCode1 f Execute = do
  (item, f) <- pop f
  case item of
    Just (ExecutionToken xt) -> xt f
    _ -> err f "no execution token"

push f item = pure $ f { stack = item : stack f }

pop f = case stack f of
  (item : rest) -> pure (Just item, f { stack = rest })
  _ -> pure (Nothing, f)

convertDouble = readMaybe

convertInt = readMaybe

repl f = do
  f <- dropSpace f
  (name, f) <- breakSpace f
  unless (null name) $ do
    f <- case lookup f name of
      Just word -> compileOrInterpret f word
      Nothing -> case convertDouble name of
        Just d -> compileOrInterpret f (double d)
        Nothing -> case convertInt name of
          Just i -> compileOrInterpret f (int i)
          Nothing -> err f $ "word " ++ name ++ " not found, not a number"
    repl f

err f msg = hPutStrLn stderr (" ERROR: " ++ msg) >> pure f

main :: IO ()
main = repl . forth =<< getContents
