/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

struct local
{
  struct local *next;
  struct symbol *name;
  bool initialized_from_stack;
};

struct local *local_new(struct symbol *name, bool initialized_from_stack)
{
  struct local *l = malloc(sizeof(*l));
  if (! l)
  {
    return 0;
  }
  l->next = 0;
  l->name = name;
  l->initialized_from_stack = initialized_from_stack;
  return l;
}

void local_delete(struct local *l)
{
  free(l);
}

bool local_initialized_from_stack(const struct local *l)
{
  return l->initialized_from_stack;
}

struct word
{
  struct word *next;
  struct word *prev;
  struct symbol *name;
  struct program *code;
  struct local *locals;
  ptrdiff_t locals_count;
};

struct word *word_new(struct symbol *name)
{
  struct word *w = calloc(1, sizeof(struct word));
  if (! w)
  {
    return 0;
  }
  w->code = program_new();
  if (! w->code)
  {
    free(w);
    return 0;
  }
  w->name = name;
  return w;
}

void word_add_local(struct word *word, struct symbol *name, bool initialized_from_stack)
{
  struct local *local = local_new(name, initialized_from_stack);
  local->next = word->locals;
  word->locals = local;
  word->locals_count++;
}

bool word_get_local(const struct word *word, const struct symbol *name, ptrdiff_t *index)
{
  struct local *l = word->locals;
  for (ptrdiff_t i = 0; l; i++, l = l->next)
  {
    if (l->name == name)
    {
      *index = word->locals_count - 1 - i;
      return true;
    }
  }
  *index = -1;
  return false;
}

const struct local *word_get_locals(const struct word *word)
{
  return word->locals;
}

const struct local *local_next(const struct local *l)
{
  return l->next;
}

struct program *word_get_program(struct word *word)
{
  return word->code;
}

void word_activate(struct word *word)
{
  symbol_set_word(word->name, word);
}

void word_deactivate(struct word *word)
{
  symbol_set_word(word->name, 0);
}

bool word_is_active(const struct word *word)
{
  return symbol_get_word(word->name) == word;
}

void word_delete(struct word *word)
{
  if (word_is_active(word))
  {
    word_deactivate(word);
  }
  if (word->next)
    word->next->prev = word->prev;
  if (word->prev)
    word->prev->next = word->next;
  program_delete(word->code);
  struct local *l = word->locals;
  while (l)
  {
    struct local *next = l->next;
    free(l);
    l = next;
  }
  free(word);
}

void word_dump(struct word *w)
{
  fprintf(stderr, "%p %p %p %s\n", (void *) w->prev, (void *) w, (void *) w->next, w->name ? symbol_get_name(w->name) : "(null)");
}

enum error word_execute(struct word *w, struct stack *left, struct stack *right)
{
  return program_execute(w->code, left, right);
}

enum error compile_word(struct word *w, struct chain *chain, struct stack *left, struct stack *right)
{
  return compile_program(w, w->code, chain, left, right);
}

struct dictionary
{
  struct word head;
  struct word tail;
  void *arg;
  can_gc can_gc;
};

struct dictionary *dictionary_new(void)
{
  struct dictionary *dict = calloc(1, sizeof(*dict));
  if (! dict)
  {
    return 0;
  }
  dict->head.next = &dict->tail;
  dict->tail.prev = &dict->head;
  return dict;
}

void dictionary_delete(struct dictionary *dict)
{
  struct word *next;
  for (struct word *w = dict->head.next; w->next; w = next)
  {
    next = w->next;
    word_delete(w);
  }
  free(dict);
}

struct word *dictionary_lookup(struct dictionary *dict, const struct symbol *name)
{
  for (struct word *w = dict->head.next; w->next; w = w->next)
  {
    if (w->name == name)
    {
      return w;
    }
  }
  return 0;
}

void dictionary_insert(struct dictionary *dict, struct word *word)
{
  word->next = dict->head.next;
  word->prev = &dict->head;
  dict->head.next->prev = word;
  dict->head.next = word;
  word_activate(word);
}

void dictionary_set_can_gc(struct dictionary *dict, void *arg, can_gc can_gc_cb)
{
  dict->arg = arg;
  dict->can_gc = can_gc_cb;
}

void dictionary_gc(struct dictionary *dict)
{
  struct word *next = dict->head.next->next;
  for (struct word *w = dict->head.next; next; w = next)
  {
    next = w->next;
    if (! word_is_active(w))
    {
      if ((! dict->can_gc) || dict->can_gc(dict->arg, w))
      {
        word_delete(w);
      }
    }
  }
}

void dictionary_dump(struct dictionary *dict)
{
  for (struct word *w = &dict->head; w; w = w->next)
  {
    word_dump(w);
  }
}
