/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

void atom_set_error(struct atom *a, struct position pos, const char *x)
{
  a->type = a_error;
  a->value.error = x;
  a->pos = pos;
}

void atom_set_symbol(struct atom *a, struct position pos, struct symbol *x)
{
  a->type = a_symbol;
  a->value.symbol = x;
  a->pos = pos;
}

void atom_set_compile(struct atom *a, struct position pos, compile x)
{
  a->type = a_compile;
  a->value.compile = x;
  a->pos = pos;
}

void atom_set_i64(struct atom *a, struct position pos, i64 x)
{
  a->type = a_i64;
  a->value.i64 = x;
  a->pos = pos;
}

void atom_set_grid(struct atom *a, struct position pos, struct grid *x)
{
  a->type = a_grid;
  a->value.grid = x;
  a->pos = pos;
}

void atom_ref(struct atom *a)
{
  if (a->type == a_grid)
  {
    grid_ref(a->value.grid);
  }
}

void atom_unref(struct atom *a)
{
  if (a->type == a_grid)
  {
    grid_unref(a->value.grid);
  }
}

void atom_lock(struct atom *a)
{
  if (a->type == a_grid)
  {
    grid_lock(a->value.grid);
  }
}

void atom_unlock(struct atom *a)
{
  if (a->type == a_grid)
  {
    grid_unlock(a->value.grid);
  }
}

struct stack
{
  struct atom *data;
  ptrdiff_t alloc;
  ptrdiff_t top;
  ptrdiff_t limit;
};

struct stack *stack_new(void)
{
  struct stack *s = malloc(sizeof(*s));
  if (! s)
  {
    return 0;
  }
  s->alloc = 256;
  s->top = 0;
  s->limit = 256;
  s->data = malloc(sizeof(*s->data) * s->alloc);
  if (! s->data)
  {
    free(s);
    return 0;
  }
  return s;
}

void stack_delete(struct stack *s)
{
  stack_reset(s);
  free(s->data);
  free(s);
}

void stack_reset(struct stack *s)
{
  // FIXME leaks grids?
  s->top = 0;
}

enum error stack_push(struct stack *s, struct atom *a)
{
  if (s->top >= s->alloc)
  {
    if (s->alloc >= s->limit)
    {
      return e_stack_overflow;
    }
    else
    {
      s->alloc <<= 1;
      s->data = realloc(s->data, sizeof(*s->data) * s->alloc);
      if (! s->data)
      {
        fprintf(stderr, "0:0: ERROR: out of memory\n");
        abort();
      }
    }
  }
  s->data[s->top++] = *a;
  return e_no_error;
}

enum error stack_push_grid(struct stack *s, struct grid *g)
{
  struct position p = { -1, -1 };
  struct atom a;
  atom_set_grid(&a, p, g);
  return stack_push(s, &a);
}

enum error stack_push_i64(struct stack *s, i64 g)
{
  struct position p = { -1, -1 };
  struct atom a;
  atom_set_i64(&a, p, g);
  return stack_push(s, &a);
}

enum error stack_pop(struct stack *s, struct atom *a)
{
  if (s->top > 0)
  {
    *a = s->data[--s->top];
    return e_no_error;
  }
  else
  {
    struct position p = { -1, -1 };
    atom_set_error(a, p, "stack underflow");
    return e_stack_underflow;
  }
}

enum error stack_get(struct stack *s, ptrdiff_t ix, struct atom *a)
{
  ptrdiff_t off = s->top - 1 - ix;
  if (0 <= off && off < s->top)
  {
    *a = s->data[off];
    return e_no_error;
  }
  else
  {
    struct position p = { -1, -1 };
    atom_set_error(a, p, "index out of range");
    return e_index_out_of_range;
  }
}

enum error stack_set(struct stack *s, ptrdiff_t ix, const struct atom *a)
{
  ptrdiff_t off = s->top - 1 - ix;
  if (0 <= off && off < s->top)
  {
    s->data[off] = *a;
    return e_no_error;
  }
  else
  {
    return e_index_out_of_range;
  }
}

enum error stack_drop(struct stack *s, ptrdiff_t count)
{
  ptrdiff_t new_top = s->top - count;
  if (0 <= new_top && new_top <= s->top)
  {
    s->top = new_top;
    return e_no_error;
  }
  else
  {
    if (new_top < 0)
      return e_stack_underflow;
    else
      return e_negative_drop;
  }
}

enum error atom_execute(struct atom *atom, struct stack *left, struct stack *right)
{
  switch (atom->type)
  {
    default:
    {
      return stack_push(left, atom);
    }
    case a_symbol:
    {
      return symbol_execute(atom->value.symbol, left, right);
    }
  }
}

enum error compile_atom(struct atom *atom, struct chain *chain, struct stack *left, struct stack *right)
{
  switch (atom->type)
  {
    case a_i64:
    {
      return stack_push(left, atom);
    }
    case a_grid:
    {
#ifdef DEBUG_GRID_LITERALS
      fprintf(stderr, " [");
      for (int i = 0; i < atom->value.grid->ndims; ++i)
      {
        fprintf(stderr, " %lu", atom->value.grid->dim[i]);
      }
      switch (atom->value.grid->type)
      {
        case g_i8: fprintf(stderr, " i8"); break;
        case g_u8: fprintf(stderr, " u8"); break;
        case g_i16: fprintf(stderr, " i16"); break;
        case g_u16: fprintf(stderr, " u16"); break;
        case g_i32: fprintf(stderr, " i32"); break;
        case g_u32: fprintf(stderr, " u32"); break;
        case g_i64: fprintf(stderr, " i64"); break;
        case g_u64: fprintf(stderr, " u64"); break;
        case g_f32: fprintf(stderr, " f32"); break;
        case g_f64: fprintf(stderr, " f64"); break;
      }
      fprintf(stderr, " #");
      for (u64 i = 0; i < atom->value.grid->count; ++i)
      {
        switch (atom->value.grid->type)
        {
          case g_i8: fprintf(stderr, " %d", atom->value.grid->value.i8[i]); break;
          case g_u8: fprintf(stderr, " %u", atom->value.grid->value.u8[i]); break;
          case g_i16: fprintf(stderr, " %d", atom->value.grid->value.i16[i]); break;
          case g_u16: fprintf(stderr, " %u", atom->value.grid->value.u16[i]); break;
          case g_i32: fprintf(stderr, " %d", atom->value.grid->value.i32[i]); break;
          case g_u32: fprintf(stderr, " %u", atom->value.grid->value.u32[i]); break;
          case g_i64: fprintf(stderr, " %ld", atom->value.grid->value.i64[i]); break;
          case g_u64: fprintf(stderr, " %lu", atom->value.grid->value.u64[i]); break;
          case g_f32: fprintf(stderr, " %g", atom->value.grid->value.f32[i]); break;
          case g_f64: fprintf(stderr, " %g", atom->value.grid->value.f64[i]); break;
        }
      }
      fprintf(stderr, " ]");
#endif
      return stack_push(left, atom);
    }
    case a_symbol:
    {
      return compile_symbol(atom->pos, atom->value.symbol, chain, left, right);
    }
    case a_compile:
    {
      return atom->value.compile(chain, left, right);
    }
  }
  return e_bad_atom_type;
}

const char *error_string(enum error e)
{
  switch (e)
  {
    case e_no_error: return "no error";
    case e_stack_underflow: return "stack underflow";
    case e_stack_overflow: return "stack overflow";
    case e_type_mismatch: return "type mismatch";
    case e_could_not_acquire_grid: return "could not acquire grid";
    case e_could_not_release_grid: return "could not release grid";
    case e_expected_grid: return "expected grid";
    case e_expected_i64: return "expected i64";
    case e_no_primitive_for_type: return "no primitive for type";
    case e_mismatched_argument_types: return "mismatched argument types";
    case e_mismatched_argument_dimensionality: return "mismatched argument dimensionality";
    case e_mismatched_argument_dimension: return "mismatched argument dimensions";
    case e_chain_overflow: return "chain overflow";
    case e_negative_drop: return "negative drop";
    case e_index_out_of_range: return "index out of range";
    case e_word_not_defined: return "word not defined";
    case e_bad_atom_type: return "bad atom type";
  }
  return "unknown error";
}
