/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

u64 dims_count(int ndims, const u64 *dim)
{
  u64 p = 1;
  for (int i = 0; i < ndims; ++i)
  {
    p *= dim[i];
  }
  return p;
}

u64 grid_count(struct grid *grid)
{
  return grid->count;
}

struct grid *grid_acquire(struct chain *chain, enum grid_type type, int ndims, const u64 *dim)
{
  u64 count = dims_count(ndims, dim);
  if (count == 0)
  {
    return 0;
  }
  for (struct grid *g = chain->head.next; g->next; g = g->next)
  {
    if (g->type == type && g->count == count && g->refcount == 0)
    {
      g->ndims = ndims;
      for (int i = 0; i < ndims; ++i)
      {
        g->dim[i] = dim[i];
      }
      g->refcount++;
#ifdef DEBUG_GRID_ALLOC
      fprintf(stderr, "\nREUSING GRID %p [", g);
      for (int i = 0; i < g->ndims; ++i)
      {
        fprintf(stderr, " %lu", g->dim[i]);
      }
      fprintf(stderr, " ]\n");
#endif
      return g;
    }
  }
  return grid_new(chain, type, ndims, dim);
}

struct grid *grid_new(struct chain *owner, enum grid_type type, int ndims, const u64 *dim)
{
  u64 count = dims_count(ndims, dim);
  if (count == 0)
  {
    return 0;
  }
  struct grid *g = memory_alloc(sizeof(*g));
  g->refcount++;
  if (owner)
  {
    g->owner = owner;
    g->next = owner->head.next;
    g->prev = owner->head.next->prev;
    owner->head.next->prev = g;
    owner->head.next = g;
  }
  g->type = type;
  g->ndims = ndims;
  for (int i = 0; i < ndims; ++i)
  {
    g->dim[i] = dim[i];
  }
  g->count = count;
  switch (type)
  {
#define CASE(TYPE) \
    case g_##TYPE: \
      g->value.TYPE = memory_alloc(count * sizeof(*g->value.TYPE)); \
      break;
    CASE(u8)
    CASE(i8)
    CASE(u16)
    CASE(i16)
    CASE(u32)
    CASE(i32)
    CASE(u64)
    CASE(i64)
    CASE(f32)
    CASE(f64)
#undef CASE
  }
#ifdef DEBUG_GRID_ALLOC
  fprintf(stderr, "\nNEW GRID %p [", g);
  for (int i = 0; i < ndims; ++i)
  {
    fprintf(stderr, " %lu", dim[i]);
  }
  fprintf(stderr, " ]\n");
#endif
  return g;
}

enum error grid_release(struct grid *g)
{
  if (g->owner)
  {
#ifdef DEBUG_GRID_ALLOC
    fprintf(stderr, "\nRECYCLE GRID %p [", g);
    for (int i = 0; i < g->ndims; ++i)
    {
      fprintf(stderr, " %lu", g->dim[i]);
    }
    fprintf(stderr, " ]\n");
#endif
    return e_no_error;
  }
  else
  {
#ifdef DEBUG_GRID_ALLOC
    fprintf(stderr, "\nNO OWNER %p [", g);
    for (int i = 0; i < g->ndims; ++i)
    {
      fprintf(stderr, " %lu", g->dim[i]);
    }
    fprintf(stderr, " ]\n");
#endif
    return e_no_error;
  }
}

void grid_delete(struct grid *g)
{
  if (! g) return;
#ifdef DEBUG_GRID_ALLOC
  fprintf(stderr, "\nDELETE GRID %p [", g);
  for (int i = 0; i < g->ndims; ++i)
  {
    fprintf(stderr, " %lu", g->dim[i]);
  }
  fprintf(stderr, " ]\n");
#endif
  switch (g->type)
  {
#define CASE(TYPE) \
    case g_##TYPE: \
      if (g->value.TYPE) \
      { \
        memory_free(g->value.TYPE, g->count * sizeof(*g->value.TYPE)); \
      } \
      break;
    CASE(u8)
    CASE(i8)
    CASE(u16)
    CASE(i16)
    CASE(u32)
    CASE(i32)
    CASE(u64)
    CASE(i64)
    CASE(f32)
    CASE(f64)
#undef CASE
  }
  memory_free(g, sizeof(*g));
}

void grid_ref(struct grid *g)
{
  if (g->lockedcount)
  {
    return;
  }
  g->refcount++;
}

void grid_unref(struct grid *g)
{
  if (g->lockedcount)
  {
    return;
  }
  g->refcount--;
  if (g->refcount == 0)
  {
    grid_release(g);
  }
}

void grid_lock(struct grid *g)
{
  g->lockedcount++;
}

void grid_unlock(struct grid *g)
{
  g->lockedcount--;
}


void chain_delete(struct chain *s)
{
  struct grid *next;
  for (struct grid *g = s->head.next; g->next; g = next)
  {
    next = g->next;
    grid_delete(g);
  }
  memory_free(s->data, s->alloc * sizeof(*s->data));
  memory_free(s, sizeof(*s));
}

enum error chain_append1(struct chain *s, union chain_item w)
{
  if (s->top >= s->alloc)
  {
    if (s->alloc >= s->limit)
    {
      return e_chain_overflow;
    }
    else
    {
      size_t oldsize = sizeof(*s->data) * s->alloc;
      s->alloc <<= 1;
      size_t newsize = sizeof(*s->data) * s->alloc;
      s->data = memory_realloc(s->data, oldsize, newsize);
      if (! s->data)
      {
        fprintf(stderr, "0:0: ERROR: out of memory\n");
        abort();
      }
    }
  }
  s->data[s->top++] = w;
  return e_no_error;
}

enum error chain_append(struct chain *s, union chain_item *w, int count)
{
  for (int i = 0; i < count; ++i)
  {
    enum error e = chain_append1(s, w[i]);
    if (e) return e;
  }
  return e_no_error;
}

struct chain *chain_new(ptrdiff_t limit)
{
  struct chain *s = memory_alloc(sizeof(*s));
  s->head.next = &s->tail;
  s->tail.prev = &s->head;
  s->alloc = 1;
  s->limit = limit;
  s->data = memory_alloc(s->alloc * sizeof(*s->data));
  return s;
}
