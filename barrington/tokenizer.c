/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

struct tokenizer
{
  struct istream *in;
  FILE *out, *err;
  char *buffer;
  ptrdiff_t alloc;
  ptrdiff_t index;
  enum token_type type;
  union
  {
    i64 i64;
    f64 f64;
    const char *string;
  } value;
};

struct tokenizer *tokenizer_new(struct istream *in, FILE *out, FILE *err)
{
  struct tokenizer *token = malloc(sizeof(*token));
  if (! token)
  {
    return 0;
  }
  token->alloc = 16;
  token->buffer = malloc(token->alloc);
  if (! token->buffer)
  {
    free(token);
    return 0;
  }
  token->index = 0;
  token->buffer[token->index] = 0;
  token->in = in;
  token->out = out;
  token->err = err;
  token->type = t_begin;
  return token;
}

void tokenizer_delete(struct tokenizer *token)
{
  if (token)
  {
    if (token->buffer)
    {
      free(token->buffer);
    }
    free(token);
  }
}

bool tokenizer_grow_buffer(struct tokenizer *token)
{
  if (token->index >= token->alloc)
  {
    token->alloc <<= 1;
    token->buffer = realloc(token->buffer, token->alloc);
    if (! token->buffer)
    {
      struct position p = istream_getpos(token->in);
      fprintf(token->err, "%d:%d: ERROR: out of memory\n", p.row, p.col);
      abort();
    }
  }
  token->buffer[token->index] = 0;
  return true;
}

void tokenizer_many_space(struct tokenizer *token)
{
  if (token->type == t_eof)
  {
    return;
  }
  int comment_depth = 0;
  int c;
  while ((c = istream_getc(token->in)))
  {
    switch (c)
    {
      case EOF:
        if (comment_depth > 0)
        {
          struct position p = istream_getpos(token->in);
          fprintf(token->err, "%d:%d: ERROR: too many (\n", p.row, p.col);
        }
        token->type = t_eof;
        return;
      case ' ':
      case '\t':
      case '\n':
      case '\r':
        continue;
      case '(':
        comment_depth++;
        continue;
      case ')':
        comment_depth--;
        if (comment_depth < 0)
        {
          struct position p = istream_getpos(token->in);
          fprintf(token->err, "%d:%d: ERROR: too many )\n", p.row, p.col);
          comment_depth = 0;
        }
        continue;
      case '\\':
      {
        bool incomment = true;
        while (incomment && (c = istream_getc(token->in)))
        {
          switch (c)
          {
            case EOF:
            {
              struct position p = istream_getpos(token->in);
              fprintf(token->err, "%d:%d: ERROR: EOF in comment\n", p.row, p.col);
              token->type = t_eof;
              return;
            }
            case '\n':
              incomment = false;
              break;
            default:
              continue;
          }
        }
        continue;
      }
      default:
        if (comment_depth == 0)
        {
          istream_ungetc(token->in, c);
          return;
        }
        else
        {
          continue;
        }
    }
  }
}

bool tokenizer_some_print(struct tokenizer *token)
{
  token->index = 0;
  token->buffer[token->index] = 0;
  int c;
  while ((c = istream_getc(token->in)))
  {
    switch (c)
    {
      case EOF:
        return token->index > 0;
      case ' ':
      case '\t':
      case '\n':
      case '\r':
      case '\\':
      case '(':
        istream_ungetc(token->in, c);
        return token->index > 0;
      default:
        token->buffer[token->index++] = c;
        tokenizer_grow_buffer(token);
        continue;
    }
  }
  assert(! "reachable");
  return false;
}

bool tokenizer_lex(struct tokenizer *token)
{
  tokenizer_many_space(token);
  if (tokenizer_some_print(token))
  {
    const char *str = token->buffer;
    assert(*str != 0);
    int base = 0;
    char *end = 0;
    i64 n = strtoll(str, &end, base);
    if (*str != 0 && *end == 0)
    {
      token->type = t_i64;
      token->value.i64 = n;
    }
    else
    {
      f64 f = strtod(str, &end);
      if (*str != 0 && *end == 0)
      {
        token->type = t_f64;
        token->value.f64 = f;
      }
      else
      {
        token->type = t_string;
        token->value.string = token->buffer;
      }
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool tokenizer_read(struct tokenizer *token)
{
  return tokenizer_lex(token);
}

struct symbol *tokenizer_symbol(struct symbol_table *tab, const struct tokenizer *token)
{
  if (token->type == t_string)
  {
    return symbol_new(tab, token->value.string);
  }
  return 0;
}

enum token_type tokenizer_type(const struct tokenizer *token)
{
  return token->type;
}

i64 tokenizer_get_i64(const struct tokenizer *token)
{
  if (token->type == t_i64)
  {
    return token->value.i64;
  }
  else
  {
    return 0;
  }
}

f64 tokenizer_get_f64(const struct tokenizer *token)
{
  if (token->type == t_f64)
  {
    return token->value.f64;
  }
  else
  {
    return 0;
  }
}

struct position tokenizer_getpos(struct tokenizer *token)
{
  return istream_getpos(token->in);
}
