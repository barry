/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

struct program
{
  struct atom *code;
  ptrdiff_t alloc;
  ptrdiff_t count;
};

struct program *program_new(void)
{
  struct program *p = malloc(sizeof(*p));
  if (! p)
  {
    return 0;
  }
  p->alloc = 1;
  p->count = 0;
  p->code = malloc(sizeof(*p->code) * p->alloc);
  if (! p->code)
  {
    free(p);
    return 0;
  }
  return p;
}

void program_delete(struct program *p)
{
  free(p->code);
  free(p);
}

void program_append_atom(struct program *p, const struct atom *a)
{
  p->code[p->count++] = *a;
  if (p->count >= p->alloc)
  {
    p->alloc <<= 1;
    p->code = realloc(p->code, sizeof(*p->code) * p->alloc);
    if (! p->code)
    {
      fprintf(stderr, "ERROR: out of memory\n");
      abort();
    }
  }
}

void program_append_i64(struct program *p, struct position pos, i64 x)
{
  struct atom a;
  atom_set_i64(&a, pos, x);
  program_append_atom(p, &a);
}

void program_append_grid(struct program *p, struct position pos, struct grid *x)
{
  struct atom a;
  atom_set_grid(&a, pos, x);
  program_append_atom(p, &a);
}

void program_append_compile(struct program *p, struct position pos, compile x)
{
  struct atom a;
  atom_set_compile(&a, pos, x);
  program_append_atom(p, &a);
}

void program_append_symbol(struct program *p, struct position pos, struct symbol *x)
{
  struct atom a;
  atom_set_symbol(&a, pos, x);
  program_append_atom(p, &a);
}

void program_append_program(struct program *p, const struct program *q)
{
  for (ptrdiff_t index = 0; index < q->count; ++index)
  {
    program_append_atom(p, &q->code[index]);
  }
}

enum error program_execute(struct program *program, struct stack *left, struct stack *right)
{
  for (ptrdiff_t index = 0; index < program->count; ++index)
  {
    enum error e = atom_execute(&program->code[index], left, right);
    if (e) return e;
  }
  return e_no_error;
}

enum error compile_program(struct word *word, struct program *program, struct chain *chain, struct stack *left, struct stack *right)
{
  for (ptrdiff_t index = 0; index < program->count; ++index)
  {
    struct atom *a = &program->code[index];
    ptrdiff_t ix;
    if (a->type == a_symbol && word_get_local(word, a->value.symbol, &ix))
    {
      stack_push_i64(left, ix);
      enum error e = prim_local_get(chain, left, right);
      if (e) return e;
    }
    else
    {
      enum error e = compile_atom(a, chain, left, right);
      if (e) return e;
    }
  }
  return e_no_error;
}
