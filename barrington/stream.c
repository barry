/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

// abstract input stream

#ifdef getc
#undef getc
#endif

typedef void (*istream_delete_t)(struct istream *);
typedef int (*istream_getc_t)(struct istream *);
typedef int (*istream_ungetc_t)(struct istream *, int);

struct istream
{
  istream_delete_t delete_;
  istream_getc_t getc;
  istream_ungetc_t ungetc;
  struct position pos;
};

void istream_delete(struct istream *s)
{
  if (s && s->delete_)
  {
    s->delete_(s);
  }
}

int istream_getc(struct istream *s)
{
  if (s && s->getc)
  {
    int c = s->getc(s);
    if (c == '\n')
    {
      s->pos.row++;
      s->pos.col = 0;
    }
    else
    {
      s->pos.col++;
    }
    return c;
  }
  else
  {
    return EOF;
  }
}

int istream_ungetc(struct istream *s, int c)
{
  if (s && s->ungetc)
  {
    if (c == '\n')
    {
      s->pos.row--;
      s->pos.col = -1; // FIXME
    }
    else
    {
      s->pos.col--;
    }
    return s->ungetc(s, c);
  }
  else
  {
    return EOF;
  }
}

struct position istream_getpos(struct istream *s)
{
  return s->pos;
}

// file input stream

struct istream_file
{
  struct istream s;
  FILE *f;
};

int istream_file_getc(struct istream *is)
{
  struct istream_file *isf = (struct istream_file *) is;
  return fgetc(isf->f);
}

int istream_file_ungetc(struct istream *is, int c)
{
  struct istream_file *isf = (struct istream_file *) is;
  return ungetc(c, isf->f);
}

void istream_file_delete(struct istream *is)
{
  free(is);
}

struct istream *istream_file_new(FILE *in)
{
  struct istream_file *isf = calloc(1, sizeof(*isf));
  isf->s.delete_ = istream_file_delete;
  isf->s.getc = istream_file_getc;
  isf->s.ungetc = istream_file_ungetc;
  struct position pos = { 1, 0 };
  isf->s.pos = pos;
  isf->f = in;
  return &isf->s;
}

// string input stream

struct istream_string
{
  struct istream s;
  const char *f;
  int i;
};

int istream_string_getc(struct istream *is)
{
  struct istream_string *iss = (struct istream_string *) is;
  if (iss->f[iss->i])
  {
    return iss->f[iss->i++];
  }
  else
  {
    return EOF;
  }
}

int istream_string_ungetc(struct istream *is, int c)
{
  struct istream_string *iss = (struct istream_string *) is;
  if (iss->i > 0 && iss->f[iss->i - 1] == c)
  {
    iss->i--;
    return c;
  }
  else
  {
    return EOF;
  }
}

void istream_string_delete(struct istream *is)
{
  free(is);
}

struct istream *istream_string_new(const char *in)
{
  struct istream_string *iss = calloc(1, sizeof(*iss));
  iss->s.delete_ = istream_string_delete;
  iss->s.getc = istream_string_getc;
  iss->s.ungetc = istream_string_ungetc;
  struct position pos = { 1, 0 };
  iss->s.pos = pos;
  iss->f = in;
  iss->i = 0;
  return &iss->s;
}
