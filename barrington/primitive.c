/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

bool is_signed(enum grid_type t)
{
  switch (t)
  {
    case g_u8:
    case g_u16:
    case g_u32:
    case g_u64:
      return false;
    default:
      return true;
  }
}

bool is_float(enum grid_type t)
{
  switch (t)
  {
    case g_f32:
    case g_f64:
      return true;
    default:
      return false;
  }
}

int bit_size(enum grid_type t)
{
  switch (t)
  {
    case g_u8:
    case g_i8:
      return 8;
    case g_u16:
    case g_i16:
      return 16;
    case g_u32:
    case g_i32:
    case g_f32:
      return 32;
    case g_u64:
    case g_i64:
    case g_f64:
      return 64;
    default:
      return 0;
  }
}

int max(int l, int r)
{
  return l > r ? l : r;
}

enum grid_type type_for(bool signed_, bool float_, int bits_)
{
  if (float_)
  {
    if (bits_ == 32)
    {
      return g_f32;
    }
    else
    {
      return g_f64;
    }
  }
  else
  {
    if (signed_)
    {
      switch (bits_)
      {
        case 8: return g_i8;
        case 16: return g_i16;
        case 32: return g_i32;
        default: return g_i64;
      }
    }
    else
    {
      switch (bits_)
      {
        case 8: return g_u8;
        case 16: return g_u16;
        case 32: return g_u32;
        default: return g_u64;
      }
    }
  }
}

enum grid_type promote_x(enum grid_type l, enum grid_type r)
{
  bool signed_ = is_signed(l) || is_signed(r);
  bool float_ = is_float(l) || is_float(r);
  int bits_ = max(bit_size(l), bit_size(r));
  return type_for(signed_, float_, bits_);
}

enum grid_type promote_f(enum grid_type l, enum grid_type r)
{
  switch (promote_x(l, r))
  {
    case g_u8:
    case g_i8:
    case g_u16:
    case g_i16:
    case g_f32:
      return g_f32;
    case g_u32:
    case g_i32:
    case g_u64:
    case g_i64:
    case g_f64:
    default:
      return g_f64;
  }
}

enum grid_type promote_i(enum grid_type l, enum grid_type r)
{
  enum grid_type t = promote_x(l, r);
  switch (t)
  {
    case g_f32:
    case g_f64:
      return g_i64;
    default:
      return t;
  }
}

#define FOR_ITYPE_0(F) \
  F(u8) \
  F(i8) \
  F(u16) \
  F(i16) \
  F(u32) \
  F(i32) \
  F(u64) \
  F(i64) \

#define FOR_ITYPE_1(F,...) \
  F(u8,__VA_ARGS__) \
  F(i8,__VA_ARGS__) \
  F(u16,__VA_ARGS__) \
  F(i16,__VA_ARGS__) \
  F(u32,__VA_ARGS__) \
  F(i32,__VA_ARGS__) \
  F(u64,__VA_ARGS__) \
  F(i64,__VA_ARGS__) \

#define FOR_FTYPE_0(F) \
  F(f32) \
  F(f64) \

#define FOR_FTYPE_1(F,...) \
  F(f32,__VA_ARGS__) \
  F(f64,__VA_ARGS__) \

#define FOR_TYPE_0(F) \
  FOR_ITYPE_0(F) \
  FOR_FTYPE_0(F) \

#define FOR_TYPE_1(F,...) \
  FOR_ITYPE_1(F,__VA_ARGS__) \
  FOR_FTYPE_1(F,__VA_ARGS__) \

#define FOR_IOP1_0(F) \
  F(not) \
  F(invert) \

#define FOR_FOP1_0(F) \
  F(sqrt) \
  F(exp) \
  F(log) \
  F(floor) \
  F(ceil) \
  F(trunc) \
  F(round) \
  F(sin) \
  F(cos) \
  F(tan) \
  F(asin) \
  F(acos) \
  F(atan) \
  F(sinh) \
  F(cosh) \
  F(tanh) \
  F(asinh) \
  F(acosh) \
  F(atanh) \

#define FOR_XOP1_0(F) \
  F(negate) \

#define FOR_IOP1_1(F,...) \
  F(not,__VA_ARGS__) \
  F(invert,__VA_ARGS__) \

#define FOR_FOP1_1(F,...) \
  F(sqrt,__VA_ARGS__) \
  F(exp,__VA_ARGS__) \
  F(log,__VA_ARGS__) \
  F(floor,__VA_ARGS__) \
  F(ceil,__VA_ARGS__) \
  F(trunc,__VA_ARGS__) \
  F(round,__VA_ARGS__) \
  F(sin,__VA_ARGS__) \
  F(cos,__VA_ARGS__) \
  F(tan,__VA_ARGS__) \
  F(asin,__VA_ARGS__) \
  F(acos,__VA_ARGS__) \
  F(atan,__VA_ARGS__) \
  F(sinh,__VA_ARGS__) \
  F(cosh,__VA_ARGS__) \
  F(tanh,__VA_ARGS__) \
  F(asinh,__VA_ARGS__) \
  F(acosh,__VA_ARGS__) \
  F(atanh,__VA_ARGS__) \

#define FOR_XOP1_1(F,...) \
  F(negate,__VA_ARGS__) \

#define FOR_OP1_0(F) \
  FOR_IOP1_0(F) \
  FOR_FOP1_0(F) \
  FOR_XOP1_0(F) \

#define FOR_OP1_1(F,...) \
  FOR_IOP1_1(F,__VA_ARGS__) \
  FOR_FOP1_1(F,__VA_ARGS__) \
  FOR_XOP1_1(F,__VA_ARGS__) \

#define FOR_IOP2_0(F) \
  F(and) \
  F(or) \
  F(xor) \

#define FOR_FOP2_0(F) \
  F(atan2) \
  F(pow) \

#define FOR_XOP2_0(F) \
  F(ignore) \
  F(put) \
  F(add) \
  F(sub) \
  F(mul) \
  F(div) \
  F(mod) \
  F(shl) \
  F(shr) \
  F(gt) \
  F(ge) \
  F(eq) \
  F(ne) \
  F(le) \
  F(lt) \

#define FOR_IOP2_1(F,...) \
  F(and,__VA_ARGS) \
  F(or,__VA_ARGS) \
  F(xor,__VA_ARGS) \

#define FOR_FOP2_1(F,...) \
  F(atan2,__VA_ARGS) \
  F(pow,__VA_ARGS) \

#define FOR_XOP2_1(F,...) \
  F(ignore,__VA_ARGS) \
  F(put,__VA_ARGS) \
  F(add,__VA_ARGS) \
  F(sub,__VA_ARGS) \
  F(mul,__VA_ARGS) \
  F(div,__VA_ARGS) \
  F(mod,__VA_ARGS) \
  F(shl,__VA_ARGS) \
  F(shr,__VA_ARGS) \
  F(gt,__VA_ARGS) \
  F(ge,__VA_ARGS) \
  F(eq,__VA_ARGS) \
  F(ne,__VA_ARGS) \
  F(le,__VA_ARGS) \
  F(lt,__VA_ARGS) \

#define INFIX_TYPE(TYPE,OP,X) \
static inline TYPE prim_ ## OP ## _ ## TYPE (TYPE a, TYPE b) \
{ \
  return a X b; \
}
#define INFIX(OP,X) FOR_TYPE_1(INFIX_TYPE,OP,X)
INFIX(add,+)
INFIX(sub,-)
INFIX(mul,*)
INFIX(lt,<)
INFIX(le,<=)
INFIX(gt,>)
INFIX(ge,>=)
INFIX(eq,==)
INFIX(ne,!=)
#undef INFIX
#undef INFIX_TYPE

#define INFIXI_TYPE(TYPE,OP,X) \
static inline TYPE prim_ ## OP ## _ ## TYPE (TYPE a, TYPE b) \
{ \
  return a X b; \
}
#define INFIXI(OP,X) FOR_ITYPE_1(INFIXI_TYPE,OP,X)
INFIXI(shl,<<)
INFIXI(shr,>>)
INFIXI(and,&)
INFIXI(or,|)
INFIXI(xor,^)
#undef INFIXI
#undef INFIXI_TYPE

#define PREFIX_TYPE(TYPE,OP,X) \
static inline TYPE prim_ ## OP ## _ ## TYPE (TYPE a) \
{ \
  return X a; \
}
#define PREFIX(OP,X) FOR_TYPE_1(PREFIX_TYPE,OP,X)
PREFIX(negate,-)
#undef PREFIX
#undef PREFIX_TYPE

#define PREFIXI_TYPE(TYPE,OP,X) \
static inline TYPE prim_ ## OP ## _ ## TYPE (TYPE a) \
{ \
  return X a; \
}
#define PREFIXI(OP,X) FOR_ITYPE_1(PREFIXI_TYPE,OP,X)
PREFIXI(not,!)
PREFIXI(invert,~)
#undef PREFIXI
#undef PREFIXI_TYPE

#define F32(OP) \
static inline f32 prim_ ## OP ## _ ## f32 (f32 a) \
{ \
  return OP ## f (a); \
}
FOR_FOP1_0(F32)
#undef F32

#define F64(OP) \
static inline f64 prim_ ## OP ## _ ## f64 (f64 a) \
{ \
  return OP (a); \
}
FOR_FOP1_0(F64)
#undef F64

#define F(TYPE) \
static inline TYPE prim_div_##TYPE(TYPE a, TYPE b) \
{ \
  if (b == 0) \
  { \
    return 0; \
  } \
  else if (b == -1) \
  { \
    /* guard against SIGFPE on some CPU */ \
    return -a; \
  } \
  else \
  { \
    return a / b; \
  } \
} \
static inline TYPE prim_mod_##TYPE(TYPE a, TYPE b) \
{ \
  if (b == 0) \
  { \
    return 0; \
  } \
  else if (b == -1) \
  { \
    /* guard against SIGFPE on some CPU */ \
    return 0; \
  } \
  else \
  { \
    /* handle case of negative a, positive b more intuitively */ \
    return ((a % b) + b) % b; \
  } \
}
F(i8)
F(i16)
F(i64)
F(i32)
#undef F

#define F(TYPE) \
static inline TYPE prim_div_##TYPE(TYPE a, TYPE b) \
{ \
  if (b == 0) \
  { \
    return 0; \
  } \
  else \
  { \
    return a / b; \
  } \
} \
static inline TYPE prim_mod_##TYPE(TYPE a, TYPE b) \
{ \
  if (b == 0) \
  { \
    return 0; \
  } \
  else \
  { \
    return a % b; \
  } \
}
F(u8)
F(u16)
F(u32)
F(u64)
#undef F

static inline f32 prim_div_f32(f32 a, f32 b)
{
  return a / b;
}

static inline f64 prim_div_f64(f64 a, f64 b)
{
  return a / b;
}

static inline f32 prim_mod_f32(f32 a, f32 b)
{
  if (b == 0) return 0;
  if (isinf(b)) return a;
  a /= b;
  a -= floorf(a);
  a *= b;
  return a;
}

static inline f64 prim_mod_f64(f64 a, f64 b)
{
  if (b == 0) return 0;
  if (isinf(b)) return a;
  a /= b;
  a -= floor (a);
  a *= b;
  return a;
}

static inline f32 prim_shl_f32(f32 a, f32 b)
{
  return a * exp2f(b);
}

static inline f32 prim_shr_f32(f32 a, f32 b)
{
  return a / exp2f(b);
}

static inline f64 prim_shl_f64(f64 a, f64 b)
{
  return a * exp2 (b);
}

static inline f64 prim_shr_f64(f64 a, f64 b)
{
  return a / exp2 (b);
}

#define F(TYPE) \
static inline TYPE prim_ignore_##TYPE(TYPE a, TYPE b) \
{ \
  (void) b; \
  return a; \
} \
static inline TYPE prim_put_##TYPE(TYPE a, TYPE b) \
{ \
  (void) a; \
  return b; \
}
FOR_TYPE_0(F)
#undef F

static inline f32 prim_atan2_f32(f32 a, f32 b)
{
  return atan2f(a, b);
}

static inline f64 prim_atan2_f64(f64 a, f64 b)
{
  return atan2(a, b);
}

static inline f32 prim_pow_f32(f32 a, f32 b)
{
  return powf(a, b);
}

static inline f64 prim_pow_f64(f64 a, f64 b)
{
  return pow(a, b);
}


#define CAST(TYPE2,TYPE1) \
union chain_item * cast_copying_ ## TYPE1 ## _ ## TYPE2(union chain_item *w) \
{ \
  u64              n = w[1].u64; \
  TYPE1 * restrict z = w[2].TYPE1 ## _ptr; \
  TYPE2 * restrict a = w[3].TYPE2 ## _ptr; \
  for (u64 i = 0; i < n; ++i) \
  { \
      z[i] = a[i]; \
  } \
  return w+4; \
} \
enum error compile_ ## TYPE1 ## _ ## TYPE2(struct chain *chain, struct stack *left, struct stack *right, struct grid *a) \
{ \
  (void) right; \
  if (a->type != g_ ## TYPE2) return e_type_mismatch; \
  union chain_item w[4]; \
  struct grid *z = grid_acquire(chain, g_ ## TYPE1, a->ndims, a->dim); \
  if (! z) return e_could_not_acquire_grid; \
  if (a == z) \
  { \
    /* NOP */ \
  } \
  else \
  { \
    w[0].chainop = cast_copying_ ## TYPE1 ## _ ## TYPE2; \
    w[1].u64 = grid_count(a); \
    w[2].TYPE1 ## _ptr = z->value.TYPE1; \
    w[3].TYPE2 ## _ptr = a->value.TYPE2; \
    chain_append(chain, w, 4); \
  } \
  return stack_push_grid(left, z); \
}
#define CAST_CASE(TYPE2,TYPE1) \
    case g_ ## TYPE2: \
    { \
      return compile_ ## TYPE1 ## _ ## TYPE2(chain, left, right, a.value.grid); \
    }
#define CAST_TYPE(TYPE1) \
FOR_TYPE_1(CAST,TYPE1) \
enum error compile_ ## TYPE1(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a; \
  enum error e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  switch (a.value.grid->type) \
  { \
    FOR_TYPE_1(CAST_CASE,TYPE1) \
    default: return e_no_primitive_for_type; \
  } \
}
FOR_TYPE_0(CAST_TYPE)
#undef CAST_TYPE
#undef CAST_CASE
#undef CAST

#define MAP(TYPE,OP) \
union chain_item * map_inplace_ ## OP ## _ ## TYPE(union chain_item *w) \
{ \
  u64             n = w[1].u64; \
  TYPE * restrict a = w[2].TYPE ## _ptr; \
  for (u64 i = 0; i < n; ++i) \
  { \
      a[i] = prim_ ## OP ## _ ## TYPE (a[i]); \
  } \
  return w+3; \
} \
union chain_item * map_copying_ ## OP ## _ ## TYPE(union chain_item *w) \
{ \
  u64             n = w[1].u64; \
  TYPE * restrict z = w[2].TYPE ## _ptr; \
  TYPE * restrict a = w[3].TYPE ## _ptr; \
  for (u64 i = 0; i < n; ++i) \
  { \
      z[i] = prim_ ## OP ## _ ## TYPE (a[i]); \
  } \
  return w+4; \
} \
enum error compile_ ## OP ## _ ## TYPE(struct chain *chain, struct stack *left, struct stack *right, struct grid *a) \
{ \
  (void) right; \
  if (a->type != g_ ## TYPE) return e_type_mismatch; \
  union chain_item w[4]; \
  struct grid *z = grid_acquire(chain, a->type, a->ndims, a->dim); \
  if (! z) return e_could_not_acquire_grid; \
  if (a == z) \
  { \
    w[0].chainop = map_inplace_ ## OP ## _ ## TYPE; \
    w[1].u64 = grid_count(a); \
    w[2].TYPE ## _ptr = a->value.TYPE; \
    chain_append(chain, w, 3); \
  } \
  else \
  { \
    w[0].chainop = map_copying_ ## OP ## _ ## TYPE; \
    w[1].u64 = grid_count(a); \
    w[2].TYPE ## _ptr = z->value.TYPE; \
    w[3].TYPE ## _ptr = a->value.TYPE; \
    chain_append(chain, w, 4); \
  } \
  return stack_push_grid(left, z); \
}
#define MAP_CASE(TYPE,OP) \
    case g_ ## TYPE: \
    { \
      return compile_ ## OP ## _ ## TYPE(chain, left, right, a.value.grid); \
    }
#define MAP_FOP(OP) \
FOR_FTYPE_1(MAP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a; \
  enum error e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  switch (a.value.grid->type) \
  { \
    FOR_FTYPE_1(MAP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
FOR_FOP1_0(MAP_FOP)
#define MAP_IOP(OP) \
FOR_ITYPE_1(MAP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a; \
  enum error e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  switch (a.value.grid->type) \
  { \
    FOR_ITYPE_1(MAP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
FOR_IOP1_0(MAP_IOP)
#undef MAP_IOP
#define MAP_XOP(OP) \
FOR_TYPE_1(MAP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a; \
  enum error e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  switch (a.value.grid->type) \
  { \
    FOR_TYPE_1(MAP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
FOR_XOP1_0(MAP_XOP)
#undef MAP_XOP
#undef MAP_CASE
#undef MAP

#if 0
bool fold_compatible(const struct grid *a, const struct grid *b)
{
  if (a->type != b->type)
  {
    return false;
  }
  if (! (a->ndims < b->ndims))
  {
    return false;
  }
  for (int ai = a->ndims - 1, bi = b->ndims - 1; ai >= 0 && bi >= 0; --ai, --bi)
  {
    if (a->dim[ai] != b->dim[bi])
    {
      return false;
    }
  }
  return true;
}

struct grid *fold_new(const struct grid *a, const struct grid *b)
{
  if (fold_compatible(a, b))
  {
    int ndims = a->ndims - 1;
    int fold_dim = ndims - b->ndims;
    u64 dim[ndims];
    for (int i = 0; i < ndims; ++i)
    {
      dim[i] = i < fold_dim ? a->dim[i] : b->dim[i - fold_dim];
    }
    return grid_new(a->type, ndims, dim);
  }
}
#endif

enum error zip_compatible(const struct grid *a, const struct grid *b)
{
  if (a->type != b->type)
  {
    return e_mismatched_argument_types;
  }
#ifdef DEBUG_GRID_DIMS
  if (a->ndims < b->ndims)
  {
    fprintf(stderr, "\nERROR: a ~ [");
    for (int i = 0; i < a->ndims; ++i)
    {
      fprintf(stderr, " %lu", a->dim[i]);
    }
    fprintf(stderr, " ]\n");
    fprintf(stderr, "ERROR: b ~ [");
    for (int i = 0; i < b->ndims; ++i)
    {
      fprintf(stderr, " %lu", b->dim[i]);
    }
    fprintf(stderr, " ]\n");
    return e_mismatched_argument_dimensionality;
  }
#endif
  for (int ai = a->ndims - 1, bi = b->ndims - 1; ai >= 0 && bi >= 0; --ai, --bi)
  {
    if (a->dim[ai] != b->dim[bi])
    {
      return e_mismatched_argument_dimension;
    }
  }
  return e_no_error;
}

#define ZIP(TYPE,OP) \
union chain_item * zip_inplace2_ ## OP ## _ ## TYPE(union chain_item *w) \
{ \
  u64             n = w[1].u64; \
  TYPE * restrict a = w[2].TYPE ## _ptr; \
  for (u64 i = 0; i < n; ++i) \
  { \
    a[i] = prim_ ## OP ## _ ## TYPE (a[i], a[i]); \
  } \
  return w+3; \
} \
union chain_item * zip_inplace1_ ## OP ## _ ## TYPE(union chain_item *w) \
{ \
  u64             n = w[1].u64; \
  u64             m = w[2].u64; \
  TYPE * restrict a = w[3].TYPE ## _ptr; \
  TYPE * restrict b = w[4].TYPE ## _ptr; \
  u64 mn = n > m ? n : m; \
  for (u64 i = 0, j = 0, k = 0; i < mn && j < mn && k < mn; ++i, ++j, ++k) \
  { \
    if (i >= n) i = 0; \
    if (j >= m) j = 0; \
    a[k] = prim_ ## OP ## _ ## TYPE (a[i], b[j]); \
  } \
  return w+5; \
} \
union chain_item * zip_copying_ ## OP ## _ ## TYPE(union chain_item *w) \
{ \
  u64             n = w[1].u64; \
  u64             m = w[2].u64; \
  TYPE * restrict z = w[3].TYPE ## _ptr; \
  TYPE * restrict a = w[4].TYPE ## _ptr; \
  TYPE * restrict b = w[5].TYPE ## _ptr; \
  u64 mn = n > m ? n : m; \
  for (u64 i = 0, j = 0, k = 0; i < mn && j < mn && k < mn; ++i, ++j, ++k) \
  { \
    if (i >= n) i = 0; \
    if (j >= m) j = 0; \
    z[k] = prim_ ## OP ## _ ## TYPE (a[i], b[j]); \
  } \
  return w+6; \
} \
enum error compile_ ## OP ## _ ## TYPE(struct chain *chain, struct stack *left, struct stack *right, struct grid *a, struct grid *b) \
{ \
  (void) right; \
  if (a->type != g_ ## TYPE) return e_type_mismatch; \
  if (b->type != g_ ## TYPE) return e_type_mismatch; \
  union chain_item w[6]; \
  struct grid *z = grid_count(a) > grid_count(b) \
                 ? grid_acquire(chain, a->type, a->ndims, a->dim) \
                 : grid_acquire(chain, b->type, b->ndims, b->dim); \
  if (! z) return e_could_not_acquire_grid; \
  if (a == z) \
  { \
    if (a == b) \
    { \
      w[0].chainop = zip_inplace2_ ## OP ## _ ## TYPE; \
      w[1].u64 = grid_count(a); \
      w[2].TYPE ## _ptr = a->value.TYPE; \
      chain_append(chain, w, 3); \
    } \
    else \
    { \
      w[0].chainop = zip_inplace1_ ## OP ## _ ## TYPE; \
      w[1].u64 = grid_count(a); \
      w[2].u64 = grid_count(b); \
      w[3].TYPE ## _ptr = a->value.TYPE; \
      w[4].TYPE ## _ptr = b->value.TYPE; \
      chain_append(chain, w, 5); \
    } \
  } \
  else \
  { \
    w[0].chainop = zip_copying_ ## OP ## _ ## TYPE; \
    w[1].u64 = grid_count(a); \
    w[2].u64 = grid_count(b); \
    w[3].TYPE ## _ptr = z->value.TYPE; \
    w[4].TYPE ## _ptr = a->value.TYPE; \
    w[5].TYPE ## _ptr = b->value.TYPE; \
    chain_append(chain, w, 6); \
  } \
  return stack_push_grid(left, z); \
}
#define ZIP_CASE(TYPE,OP) \
    case g_ ## TYPE: \
    { \
      return compile_ ## OP ## _ ## TYPE(chain, left, right, a.value.grid, b.value.grid); \
    }
#define PROMOTE_CASE(TYPE) \
    case g_ ## TYPE: \
    { \
      e = compile_ ## TYPE(chain, left, right); \
      if (e) return e; \
      break; \
    }
#define ZIP_FOP(OP) \
FOR_FTYPE_1(ZIP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a, b; \
  enum error e = stack_pop(left, &b); \
  if (e) return e; \
  e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  if (b.type != a_grid) return e_expected_grid; \
  enum grid_type t = promote_f(a.value.grid->type, b.value.grid->type); \
  if (a.value.grid->type != t) \
  { \
    stack_push(left, &a); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_0; \
    } \
    e = stack_pop(left, &a); \
    if (e) return e; \
  } \
  if (b.value.grid->type != t) \
  { \
    stack_push(left, &b); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_1; \
    } \
    e = stack_pop(left, &b); \
    if (e) return e; \
  } \
  e = zip_compatible(a.value.grid, b.value.grid); \
  if (e) return e; \
  switch (t) \
  { \
    FOR_FTYPE_1(ZIP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
#define ZIP_IOP(OP) \
FOR_ITYPE_1(ZIP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a, b; \
  enum error e = stack_pop(left, &b); \
  if (e) return e; \
  e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  if (b.type != a_grid) return e_expected_grid; \
  enum grid_type t = promote_i(a.value.grid->type, b.value.grid->type); \
  if (a.value.grid->type != t) \
  { \
    stack_push(left, &a); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_0; \
    } \
    e = stack_pop(left, &a); \
    if (e) return e; \
  } \
  if (b.value.grid->type != t) \
  { \
    stack_push(left, &b); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_1; \
    } \
    e = stack_pop(left, &b); \
    if (e) return e; \
  } \
  e = zip_compatible(a.value.grid, b.value.grid); \
  if (e) return e; \
  switch (t) \
  { \
    FOR_ITYPE_1(ZIP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
#define ZIP_XOP(OP) \
FOR_TYPE_1(ZIP,OP) \
enum error compile_ ## OP(struct chain *chain, struct stack *left, struct stack *right) \
{ \
  struct atom a, b; \
  enum error e = stack_pop(left, &b); \
  if (e) return e; \
  e = stack_pop(left, &a); \
  if (e) return e; \
  if (a.type != a_grid) return e_expected_grid; \
  if (b.type != a_grid) return e_expected_grid; \
  enum grid_type t = promote_x(a.value.grid->type, b.value.grid->type); \
  if (a.value.grid->type != t) \
  { \
    stack_push(left, &a); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_0; \
    } \
    e = stack_pop(left, &a); \
    if (e) return e; \
  } \
  if (b.value.grid->type != t) \
  { \
    stack_push(left, &b); \
    switch (t) \
    { \
      FOR_TYPE_0(PROMOTE_CASE) \
      default: return e_could_not_promote_type_1; \
    } \
    e = stack_pop(left, &b); \
    if (e) return e; \
  } \
  e = zip_compatible(a.value.grid, b.value.grid); \
  if (e) return e; \
  switch (t) \
  { \
    FOR_TYPE_1(ZIP_CASE,OP) \
    default: return e_no_primitive_for_type; \
  } \
}
FOR_FOP2_0(ZIP_FOP)
FOR_IOP2_0(ZIP_IOP)
FOR_XOP2_0(ZIP_XOP)
#undef ZIP_FOP
#undef ZIP_IOP
#undef ZIP_XOP
#undef ZIP_CASE
#undef ZIP

void primitives(struct symbol_table *tab)
{
#define S(OP,SYM) \
  { \
    struct symbol *s = symbol_new(tab, #SYM); \
    symbol_set_compile(s, compile_ ## OP); \
    symbol_set_immutable(s, true); \
  }
S(not,!)
S(invert,~)
S(add,+)
S(sub,-)
S(mul,*)
S(div,/)
S(mod,%)
S(shl,<<)
S(shr,>>)
S(and,&)
S(or,|)
S(xor,^)
S(gt,>)
S(ge,>=)
S(eq,==)
S(ne,!=)
S(le,<=)
S(lt,<)
#undef S
#define S(OP) \
  { \
    struct symbol *s = symbol_new(tab, #OP); \
    symbol_set_compile(s, compile_ ## OP); \
    symbol_set_immutable(s, true); \
  }
S(negate)
S(sqrt)
S(exp)
S(log)
S(floor)
S(ceil)
S(trunc)
S(round)
S(pow)
S(sin)
S(cos)
S(tan)
S(asin)
S(acos)
S(atan)
S(sinh)
S(cosh)
S(tanh)
S(asinh)
S(acosh)
S(atanh)
FOR_TYPE_0(S)
#undef S
}

enum error prim_local_get(struct chain *chain, struct stack *left, struct stack *right)
{
  (void) chain;
  struct atom ix, val;
  enum error e = stack_pop(left, &ix);
  if (e) return e;
  if (ix.type == a_i64)
  {
    e = stack_get(right, ix.value.i64, &val);
    if (e) return e;
    return stack_push(left, &val);
  }
  else
  {
    return e_expected_i64;
  }
}

enum error prim_local_set(struct chain *chain, struct stack *left, struct stack *right)
{
  (void) chain;
  struct atom ix, val;
  enum error e = stack_pop(left, &ix);
  if (e) return e;
  e = stack_pop(left, &val);
  if (e) return e;
  if (ix.type == a_i64)
  {
    return stack_set(right, ix.value.i64, &val);  }
  else
  {
    return e_expected_i64;
  }
}

enum error prim_local_from_stack(struct chain *chain, struct stack *left, struct stack *right)
{
  (void) chain;
  struct atom val;
  enum error e = stack_pop(left, &val);
  if (e) return e;
  return stack_push(right, &val);
}

enum error prim_local_undefined(struct chain *chain, struct stack *left, struct stack *right)
{
  (void) chain;
  (void) left;
  struct position p = { -1, -1 };
  struct atom val;
  atom_set_error(&val, p, "undefined");
  return stack_push(right, &val);
}

enum error prim_local_drop(struct chain *chain, struct stack *left, struct stack *right)
{
  (void) chain;
  struct atom ix;
  enum error e = stack_pop(left, &ix);
  if (e) return e;
  if (ix.type == a_i64)
  {
    return stack_drop(right, ix.value.i64);
  }
  else
  {
    return e_expected_i64;
  }
}
