/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

static size_t allocated_peak = 0;
static size_t allocated_current = 0;

size_t memory_get_allocated_peak(void)
{
  return allocated_peak;
}

size_t memory_get_allocated_current(void)
{
  return allocated_current;
}

void *memory_alloc(size_t bytes)
{
  void *ptr = calloc(1, bytes);
  if (ptr)
  {
    allocated_current += bytes;
    allocated_peak
      = allocated_current > allocated_peak
      ? allocated_current : allocated_peak;
  }
  return ptr;
}

void memory_free(void *ptr, size_t bytes)
{
  if (ptr)
  {
    free(ptr);
    allocated_current -= bytes;
  }
}

void *memory_realloc(void *ptr, size_t oldbytes, size_t newbytes)
{
  ptr = realloc(ptr, newbytes);
  allocated_current -= oldbytes;
  if (ptr)
  {
    allocated_current += newbytes;
    allocated_peak
      = allocated_current > allocated_peak
      ? allocated_current : allocated_peak;
  }
  return ptr;
}
