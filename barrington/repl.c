/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

struct repl
{
  FILE *out;
  FILE *err;
  struct symbol_table *tab;
  struct dictionary *dict;
  struct stack *left;
  struct stack *right;
  struct tokenizer *token;
  struct supply *supply;
  struct symbol
    *s_colon,
    *s_open_brace,
    *s_close_brace,
    *s_bar,
    *s_minus_minus,
    *s_semicolon,
    *s_minus_greater,
    *s_run;
  repl_set_global_t set_global;
  repl_run_t run;
  void *user_data;
};

struct repl *repl_new(FILE *out, FILE *err)
{
  struct repl *r = calloc(1, sizeof(*r));
  r->out = out;
  r->err = err;
  r->tab = symbol_table_new();
  r->s_colon = symbol_new(r->tab, ":");
  r->s_open_brace = symbol_new(r->tab, "{");
  r->s_close_brace = symbol_new(r->tab, "}");
  r->s_bar = symbol_new(r->tab, "|");
  r->s_minus_minus = symbol_new(r->tab, "--");
  r->s_semicolon = symbol_new(r->tab, ";");
  r->s_minus_greater = symbol_new(r->tab, "->");
  r->s_run = symbol_new(r->tab, "RUN");
  r->dict = dictionary_new();
  r->left = stack_new();
  r->right = stack_new();
  r->set_global = repl_set_global_default;
  r->run = repl_run_default;
  primitives(r->tab);
  return r;
}

void repl_delete(struct repl *r)
{
  tokenizer_delete(r->token);
  stack_delete(r->left);
  stack_delete(r->right);
  dictionary_delete(r->dict);
  symbol_table_delete(r->tab);
  free(r);
}

struct position repl_getpos(struct repl *r)
{
  return tokenizer_getpos(r->token);
}

struct dictionary *repl_get_dictionary(struct repl *r)
{
  return r->dict;
}

struct symbol_table *repl_get_symbol_table(struct repl *r)
{
  return r->tab;
}

struct symbol *repl_symbol(struct repl *r)
{
  tokenizer_read(r->token);
  if (tokenizer_type(r->token) == t_string)
  {
    return tokenizer_symbol(r->tab, r->token);
  }
  else
  {
    return 0;
  }
}

bool repl_locals(struct repl *r, struct word *target)
{
  struct symbol *s;
  while ((s = repl_symbol(r)) && s != r->s_bar && s != r->s_minus_minus)
  {
    word_add_local(target, s, true);
  }
  if (s == r->s_bar)
  {
    while ((s = repl_symbol(r)) && s != r->s_minus_minus)
    {
      word_add_local(target, s, false);
    }
  }
  int result_count = 0;
  if (s == r->s_minus_minus)
  {
    while ((s = repl_symbol(r)) && s != r->s_close_brace)
    {
      result_count++;
    }
  }
  if (s == r->s_close_brace)
  {
    struct position p = repl_getpos(r);
    const struct local *l = word_get_locals(target);
    while (l)
    {
      if (local_initialized_from_stack(l))
      {
        program_append_compile(word_get_program(target), p, prim_local_from_stack);
      }
      else
      {
        program_append_compile(word_get_program(target), p, prim_local_undefined);
      }
      l = local_next(l);
    }
    return true;
  }
  else
  {
    struct position p = repl_getpos(r);
    fprintf(r->err, "%d:%d: ERROR: expected }\n", p.row, p.col);
    return false;
  }
}

bool repl_compile(struct repl *r, struct word *target)
{
  switch (tokenizer_type(r->token))
  {
    case t_string:
    {
      struct symbol *s = tokenizer_symbol(r->tab, r->token);
      if (s == r->s_minus_greater)
      {
        tokenizer_read(r->token);
        s = tokenizer_symbol(r->tab, r->token);
        if (s)
        {
          ptrdiff_t index;
          if (word_get_local(target, s, &index))
          {
            struct position p = repl_getpos(r);
            program_append_i64(word_get_program(target), p, index);
            program_append_compile(word_get_program(target), p, prim_local_set);
            return true;
          }
        }
        struct position p = repl_getpos(r);
        fprintf(r->err, "%d:%d: ERROR: expected symbol\n", p.row, p.col);
        return false;
      }
      else
      {
        ptrdiff_t index;
        if (word_get_local(target, s, &index))
        {
          struct position p = repl_getpos(r);
          program_append_i64(word_get_program(target), p, index);
          program_append_compile(word_get_program(target), p, prim_local_get);
        }
        else
        {
          struct position p = repl_getpos(r);
          program_append_symbol(word_get_program(target), p, s);
        }
        return true;
      }
    }
    break;
    case t_i64:
    {
      i64 i = tokenizer_get_i64(r->token);
      struct position p = repl_getpos(r);
      struct grid *g = grid_new(0, g_i64, 0, 0);
      g->value.i64[0] = i;
      program_append_grid(word_get_program(target), p, g);
      return true;
    }
    break;
    case t_f64:
    {
      f64 f = tokenizer_get_f64(r->token);
      struct position p = repl_getpos(r);
      struct grid *g = grid_new(0, g_f64, 0, 0);
      g->value.f64[0] = f;
      program_append_grid(word_get_program(target), p, g);
      return true;
    }
    break;
    case t_eof:
    {
      struct position p = repl_getpos(r);
      fprintf(r->err, "%d:%d: ERROR: unexpected end of input\n", p.row, p.col);
      return false;
    }
    break;
    case t_begin:
    {
      struct position p = repl_getpos(r);
      fprintf(r->err, "%d:%d: ERROR: no token\n", p.row, p.col);
      return false;
    }
  }
  struct position p = repl_getpos(r);
  fprintf(r->err, "%d:%d: ERROR: unexpected token\n", p.row, p.col);
  return false;
}

struct word *repl_define(struct repl *r)
{
  tokenizer_read(r->token);
  if (tokenizer_type(r->token) == t_string)
  {
    struct symbol *s = tokenizer_symbol(r->tab, r->token);
    if (symbol_get_immutable(s))
    {
      struct position p = repl_getpos(r);
      fprintf(r->err, "%d:%d: ERROR: '%s' is immutable\n", p.row, p.col, symbol_get_name(s));
      return 0;
    }
    struct word *target = word_new(s);
    tokenizer_read(r->token);
    bool ok = true;
    s = tokenizer_symbol(r->tab, r->token);
    if (s == r->s_open_brace)
    {
      ok = repl_locals(r, target);
      tokenizer_read(r->token);
      s = tokenizer_symbol(r->tab, r->token);
    }
    if (ok)
    {
      while (s != r->s_semicolon)
      {
        ok = repl_compile(r, target);
        if (! ok)
        {
          break;
        }
        tokenizer_read(r->token);
        s = tokenizer_symbol(r->tab, r->token);
      }
    }
    if (ok)
    {
      return target;
    }
    else
    {
      word_delete(target);
      return 0;
    }
  }
  else
  {
    struct position p = repl_getpos(r);
    fprintf(r->err, "%d:%d: ERROR: expected symbol\n", p.row, p.col);
    return 0;
  }
}

bool repl_immediate(struct repl *r)
{
  switch (tokenizer_type(r->token))
  {
    case t_string:
    {
      symbol_execute(tokenizer_symbol(r->tab, r->token), r->left, r->right);
      return true;
    }
    case t_i64:
    {
      i64 i = tokenizer_get_i64(r->token);
      struct position p = repl_getpos(r);
      struct grid *g = grid_new(0, g_i64, 0, 0);
      g->value.i64[0] = i;
      struct atom a;
      atom_set_grid(&a, p, g);
      atom_execute(&a, r->left, r->right);
      return true;
    }
    case t_f64:
    {
      f64 f = tokenizer_get_f64(r->token);
      struct position p = repl_getpos(r);
      struct grid *g = grid_new(0, g_f64, 0, 0);
      g->value.f64[0] = f;
      struct atom a;
      atom_set_grid(&a, p, g);
      atom_execute(&a, r->left, r->right);
      return true;
    }
    case t_eof:
    {
      return false;
    }
    case t_begin:
    {
      fprintf(r->err, "0:0: ERROR: no token\n");
      return false;
    }
  }
  struct position p = repl_getpos(r);
  fprintf(r->err, "%d:%d: ERROR: unexpected atom type\n", p.row, p.col);
  return false;
}

bool repl_set_global(struct repl *r)
{
  tokenizer_read(r->token);
  struct symbol *s = tokenizer_symbol(r->tab, r->token);
  if (s)
  {
    struct atom a;
    stack_pop(r->left, &a);
    return r->set_global(r, s, &a);
  }
  else
  {
    struct position p = repl_getpos(r);
    fprintf(r->err, "%d:%d: ERROR: expected symbol\n", p.row, p.col);
    return false;
  }
}

bool repl_set_global_default(struct repl *r, struct symbol *s, struct atom *a)
{
  (void) r;
  (void) s;
  (void) a;
  return false;
}

void repl_set_set_global(struct repl *r, repl_set_global_t f)
{
  r->set_global = f;
}

bool repl_run_default(struct repl *r, struct symbol *s)
{
  (void) r;
  (void) s;
  return false;
}

void repl_set_run(struct repl *r, repl_run_t f)
{
  r->run = f;
}

void repl_set_user_data(struct repl *r, void *p)
{
  r->user_data = p;
}

void *repl_get_user_data(struct repl *r)
{
  return r->user_data;
}

bool repl_toplevel(struct repl *r)
{
  struct symbol *s = tokenizer_symbol(r->tab, r->token);
  if (s == r->s_colon)
  {
    struct word *target = repl_define(r);
    if (target)
    {
      const struct local *l = word_get_locals(target);
      if (l)
      {
        ptrdiff_t count = 0;
        while (l)
        {
          l = local_next(l);
          count++;
        }
        struct position p = repl_getpos(r);
        program_append_i64(word_get_program(target), p, count);
        program_append_compile(word_get_program(target), p, prim_local_drop);
      }
      dictionary_insert(r->dict, target);
      return true;
    }
    return false;
  }
  else if (s == r->s_minus_greater)
  {
    return repl_set_global(r);
  }
  else if (s == r->s_run)
  {
    tokenizer_read(r->token);
    if (tokenizer_type(r->token) == t_string)
    {
      struct symbol *s2 = tokenizer_symbol(r->tab, r->token);
      if (r->run)
        return r->run(r, s2);
      else
      {
        struct position p = repl_getpos(r);
        fprintf(r->err, "%d:%d: ERROR: RUN not defined\n", p.row, p.col);
        return false;
      }
    }
    else
    {
      struct position p = repl_getpos(r);
      fprintf(r->err, "%d:%d: ERROR: expected symbol\n", p.row, p.col);
      return false;
    }
  }
  else
  {
    return repl_immediate(r);
  }
}

void repl_run(struct repl *r, struct istream *in)
{
  r->token = tokenizer_new(in, r->out, r->err);
  tokenizer_read(r->token);
  while (tokenizer_type(r->token) != t_eof)
  {
    repl_toplevel(r);
    tokenizer_read(r->token);
  }
  tokenizer_delete(r->token);
  r->token = 0;
}
