/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

struct symbol
{
  struct symbol *next;
  struct symbol *prev;
  char *name;
  struct word *thing;
  compile compile;
  bool immutable;
};

struct symbol_table
{
  struct symbol head;
  struct symbol tail;
};

struct symbol *symbol_table_lookup(struct symbol_table *tab, const char *name)
{
  struct symbol *s;
  for (s = tab->head.next; s->next; s = s->next)
  {
    if (0 == strcmp(s->name, name))
    {
      return s;
    }
  }
  return 0;
}

struct symbol *symbol_new(struct symbol_table *tab, const char *name)
{
  struct symbol *s = symbol_table_lookup(tab, name);
  if (s)
  {
    return s;
  }
  s = calloc(1, sizeof(*s));
  if (! s)
  {
    return 0;
  }
  s->name = strdup(name);
  s->next = tab->head.next;
  s->prev = &tab->head;
  tab->head.next->prev = s;
  tab->head.next = s;
  return s;
}

const char *symbol_get_name(const struct symbol *s)
{
  return s->name;
}


struct word *symbol_get_word(struct symbol *s)
{
  return s->thing;
}

void symbol_set_word(struct symbol *s, struct word *w)
{
  s->thing = w;
}

compile symbol_get_compile(struct symbol *s)
{
  return s->compile;
}

void symbol_set_compile(struct symbol *s, compile c)
{
  s->compile = c;
}

struct symbol_table *symbol_table_new(void)
{
  struct symbol_table *tab = calloc(1, sizeof(*tab));
  if (! tab)
  {
    return 0;
  }
  tab->head.next = &tab->tail;
  tab->tail.prev = &tab->head;
  return tab;
}

void symbol_table_delete(struct symbol_table *tab)
{
  struct symbol *next;
  for (struct symbol *s = tab->head.next; s->next; s = next)
  {
    next = s->next;
    free(s->name);
    if (s->thing)
    {
      word_delete(s->thing);
    }
    free(s);
  }
  free(tab);
}

enum error symbol_execute(struct symbol *s, struct stack *left, struct stack *right)
{
  struct word *w = s->thing;
  if (w)
  {
    return word_execute(w, left, right);
  }
  else
  {
    return e_word_not_defined;
  }
}

enum error compile_symbol(struct position pos, struct symbol *s, struct chain *chain, struct stack *left, struct stack *right)
{
  if (s->compile)
  {
    enum error e = s->compile(chain, left, right);
    if (e)
    {
      fprintf(stderr, "%d:%d: ERROR: %s\n", pos.row, pos.col, error_string(e));
    }
    return e;
  }
  else if (s->thing)
  {
    enum error e = compile_word(s->thing, chain, left, right);
    if (e)
    {
      // already printed
      // fprintf(stderr, "%d:%d: ERROR: %s\n", pos.row, pos.col, error_string(e));
    }
    return e;
  }
  else
  {
    fprintf(stderr, "%d:%d: ERROR: %s not defined\n", pos.row, pos.col, symbol_get_name(s));
    return e_word_not_defined;
  }
}

bool symbol_get_immutable(const struct symbol *s)
{
  return s->immutable;
}

void symbol_set_immutable(struct symbol *s, bool immutable)
{
  s->immutable = immutable;
}
