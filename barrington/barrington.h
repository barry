/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BARRINGTON_H
#define BARRINGTON_H 1

#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <tgmath.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef float f32;
typedef double f64;

enum error
{
  e_no_error = 0,
  e_stack_underflow,
  e_stack_overflow,
  e_type_mismatch,
  e_could_not_acquire_grid,
  e_could_not_release_grid,
  e_expected_grid,
  e_expected_i64,
  e_no_primitive_for_type,
  e_could_not_promote_type_0,
  e_could_not_promote_type_1,
  e_mismatched_argument_types,
  e_mismatched_argument_dimensionality,
  e_mismatched_argument_dimension,
  e_chain_overflow,
  e_negative_drop,
  e_index_out_of_range,
  e_word_not_defined,
  e_bad_atom_type
};
const char *error_string(enum error e);

struct position
{
  int row, col;
};

struct symbol;
struct symbol_table;
struct chain;
struct grid;
struct atom;
struct stack;
struct program;
struct local;
struct word;
struct dictionary;
struct tokenizer;

union chain_item;
typedef union chain_item *(*chainop)(union chain_item *);

typedef enum error (*compile)(struct chain *, struct stack *, struct stack *);

enum grid_type
{
  g_u8,
  g_i8,
  g_u16,
  g_i16,
  g_u32,
  g_i32,
  g_u64,
  g_i64,
  g_f32,
  g_f64
};

// somewhat arbitrary limit
#define GRID_NDIMS_MAX 16

struct grid
{
  struct grid *next;
  struct grid *prev;
  struct chain *owner;
  enum grid_type type;
  // ndims is 0 for scalars
  int ndims;
  u64 dim[GRID_NDIMS_MAX];
  u64 count;
  int lockedcount;
  int refcount;
  union
  {
    u8 *u8;
    i8 *i8;
    u16 *u16;
    i16 *i16;
    u32 *u32;
    i32 *i32;
    u64 *u64;
    i64 *i64;
    f32 *f32;
    f64 *f64;
  } value;
};

struct chain
{
  struct grid head;
  struct grid tail;
  union chain_item *data;
  ptrdiff_t alloc;
  ptrdiff_t top;
  ptrdiff_t limit;
};

u64 grid_count(struct grid *grid);
u64 dims_count(int ndims, const u64 *dim);

struct grid *grid_acquire(struct chain *chain, enum grid_type type, int ndims, const u64 *dim);
enum error grid_release(struct grid *grid);

void grid_ref(struct grid *g);
void grid_unref(struct grid *g);
void grid_lock(struct grid *g);
void grid_unlock(struct grid *g);

void atom_ref(struct atom *g);
void atom_unref(struct atom *g);
void atom_lock(struct atom *g);
void atom_unlock(struct atom *g);

struct chain *chain_new(ptrdiff_t limit);
void chain_delete(struct chain *s);
enum error chain_append1(struct chain *s, union chain_item w);
enum error chain_append(struct chain *chain, union chain_item *w, int count);

struct grid *grid_new(struct chain *owner, enum grid_type type, int ndims, const u64 *dim);
void grid_delete(struct grid *grid);

enum atom_type
{
  a_error,
  a_grid,
  a_i64,
  a_compile,
  a_symbol
};

struct atom
{
  enum atom_type type;
  union
  {
    const char *error;
    compile compile;
    struct symbol *symbol;
    struct grid *grid;
    i64 i64;
  } value;
  struct position pos;
};

union chain_item
{
  chainop chainop;
  u8 u8;
  i8 i8;
  u16 u16;
  i16 i16;
  u32 u32;
  i32 i32;
  u64 u64;
  i64 i64;
  f32 f32;
  f64 f64;
  u8 *u8_ptr;
  i8 *i8_ptr;
  u16 *u16_ptr;
  i16 *i16_ptr;
  u32 *u32_ptr;
  i32 *i32_ptr;
  u64 *u64_ptr;
  i64 *i64_ptr;
  f32 *f32_ptr;
  f64 *f64_ptr;
};

typedef bool (*can_gc)(void *, struct word *);

struct symbol *symbol_new(struct symbol_table *tab, const char *name);
struct symbol_table *symbol_table_new(void);
void symbol_table_delete(struct symbol_table *tab);
const char *symbol_get_name(const struct symbol *s);
compile symbol_get_compile(struct symbol *s);
void symbol_set_compile(struct symbol *s, compile);
struct word *symbol_get_word(struct symbol *s);
void symbol_set_word(struct symbol *s, struct word *w);
void symbol_set_immutable(struct symbol *s, bool immutable);
bool symbol_get_immutable(const struct symbol *s);

void atom_set_error(struct atom *a, struct position pos, const char *x);
void atom_set_compile(struct atom *a, struct position pos, compile x);
void atom_set_symbol(struct atom *a, struct position pos, struct symbol *x);
void atom_set_grid(struct atom *a, struct position pos, struct grid *x);
void atom_set_i64(struct atom *a, struct position pos, i64 x);

struct stack *stack_new(void);
void stack_delete(struct stack *s);
void stack_reset(struct stack *s);
enum error stack_push(struct stack *s, struct atom *a);
enum error stack_push_i64(struct stack *s, i64 g);
enum error stack_push_grid(struct stack *s,struct grid *g);
enum error stack_pop(struct stack *s, struct atom *a);
enum error stack_get(struct stack *s, ptrdiff_t ix, struct atom *a);
enum error stack_set(struct stack *s, ptrdiff_t ix, const struct atom *a);
enum error stack_drop(struct stack *s, ptrdiff_t count);

struct program *program_new(void);
void program_delete(struct program *p);
void program_append_atom(struct program *p, const struct atom *a);
void program_append_i64(struct program *p, struct position pos, i64 x);
void program_append_grid(struct program *p, struct position pos, struct grid *x);
void program_append_compile(struct program *p, struct position pos, compile x);
void program_append_symbol(struct program *p, struct position pos, struct symbol *x);
void program_append_program(struct program *p, const struct program *q);

struct local *local_new(struct symbol *name, bool initialized_from_stack);
void local_delete(struct local *l);
bool local_initialized_from_stack(const struct local *l);
const struct local *local_next(const struct local *l);

struct word *word_new(struct symbol *name);
void word_add_local(struct word *word, struct symbol *name, bool initialized_from_stack);
bool word_get_local(const struct word *word, const struct symbol *name, ptrdiff_t *index);
struct program *word_get_program(struct word *word);
const struct local *word_get_locals(const struct word *word);
void word_activate(struct word *word);
void word_deactivate(struct word *word);
bool word_is_active(const struct word *word);
void word_delete(struct word *word);

struct dictionary *dictionary_new(void);
void dictionary_delete(struct dictionary *dict);
struct word *dictionary_lookup(struct dictionary *dict, const struct symbol *name);
void dictionary_insert(struct dictionary *dict, struct word *word);
void dictionary_set_can_gc(struct dictionary *dict, void *arg, can_gc can_gc_cb);
void dictionary_gc(struct dictionary *dict);

void word_dump(struct word *w);
void dictionary_dump(struct dictionary *dict);

enum error atom_execute(struct atom *atom, struct stack *left, struct stack *right);
enum error symbol_execute(struct symbol *symbol, struct stack *left, struct stack *right);
enum error word_execute(struct word *word, struct stack *left, struct stack *right);
enum error program_execute(struct program *program, struct stack *left, struct stack *right);

enum error compile_atom(struct atom *atom, struct chain *chain, struct stack *left, struct stack *right);
enum error compile_symbol(struct position p, struct symbol *symbol, struct chain *chain, struct stack *left, struct stack *right);
enum error compile_word(struct word *word, struct chain *chain, struct stack *left, struct stack *right);
enum error compile_program(struct word *word, struct program *program, struct chain *chain, struct stack *left, struct stack *right);

struct istream;
struct istream *istream_file_new(FILE *in);
struct istream *istream_string_new(const char *in);
void istream_delete(struct istream *s);
int istream_getc(struct istream *s);
int istream_ungetc(struct istream *s, int c);
struct position istream_getpos(struct istream *s);

enum token_type
{
  t_begin,
  t_eof,
  t_i64,
  t_f64,
  t_string
};

struct tokenizer;
struct tokenizer *tokenizer_new(struct istream *in, FILE *out, FILE *err);
void tokenizer_delete(struct tokenizer *token);
bool tokenizer_read(struct tokenizer *token);
enum token_type tokenizer_type(const struct tokenizer *token);
struct symbol *tokenizer_symbol(struct symbol_table *tab, const struct tokenizer *token);
i64 tokenizer_get_i64(const struct tokenizer *token);
f64 tokenizer_get_f64(const struct tokenizer *token);
struct position tokenizer_getpos(struct tokenizer *token);

struct repl;
struct repl *repl_new(FILE *out, FILE *err);
void repl_delete(struct repl *r);
void repl_run(struct repl *r, struct istream *in);
struct dictionary *repl_get_dictionary(struct repl *r);
struct symbol_table *repl_get_symbol_table(struct repl *r);
typedef bool (*repl_run_t)(struct repl *, struct symbol *);
bool repl_run_default(struct repl *r, struct symbol *s);
void repl_set_run(struct repl *r, repl_run_t f);
typedef bool (*repl_set_global_t)(struct repl *, struct symbol *, struct atom *);
bool repl_set_global_default(struct repl *r, struct symbol *s, struct atom *a);
void repl_set_user_data(struct repl *r, void *p);
void *repl_get_user_data(struct repl *r);
void repl_set_set_global(struct repl *r, repl_set_global_t f);
struct position repl_getpos(struct repl *r);

void primitives(struct symbol_table *tab);

enum error prim_local_get(struct chain *chain, struct stack *left, struct stack *right);
enum error prim_local_set(struct chain *chain, struct stack *left, struct stack *right);
enum error prim_local_from_stack(struct chain *chain, struct stack *left, struct stack *right);
enum error prim_local_undefined(struct chain *chain, struct stack *left, struct stack *right);
enum error prim_local_drop(struct chain *chain, struct stack *left, struct stack *right);

size_t memory_get_allocated_current(void);
size_t memory_get_allocated_peak(void);
void *memory_alloc(size_t bytes);
void memory_free(void *ptr, size_t bytes);
void *memory_realloc(void *ptr, size_t oldbytes, size_t newbytes);

#endif
