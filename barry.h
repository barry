#ifndef BARRY_H
#define BARRY_H 1

#include <stdint.h>

typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;
typedef int8_t I8;
typedef int16_t I16;
typedef int32_t I32;
typedef int64_t I64;
typedef float F32;
typedef double F64;

typedef union
{
  U8 U8;
  U16 U16;
  U32 U32;
  U64 U64;
  I8 I8;
  I16 I16;
  I32 I32;
  I64 I64;
  F32 F32;
  F64 F64;
} CELL;

#define Invert(x) (~(x))
#define Not(x) (!(x))
#define Add(a,b) ((a)+(b))
#define Sub(a,b) ((a)-(b))
#define Mul(a,b) ((a)*(b))
#define Div(a,b) ((b)==0 ? 0 : (b)==-1 ? -(a) : (a) / (b))
#define Mod(a,b) ((b)==0 ? 0 : (b)==-1 ? 0 : (((a) % (b)) + (b)) % (b))
#define And(a,b) ((a)&(b))
#define Or(a,b) ((a)|(b))
#define Xor(a,b) ((a)^(b))
#define Shl(a,b) ((a)<<(b))
#define Shr(a,b) ((a)>>(b))
#define Eq(a,b) ((a)==(b))
#define Gt(a,b) ((a)>(b))

#endif
