{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Main (main) where

import Control.Monad (forM_)
import qualified Data.Map as Map
import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import Text.Parsec (parse)

import Parse
import Interpret (interpret, depths)
import Compile (compile)

err msg = hPutStrLn stderr (show msg) >> exitFailure

main = do
  [filename] <- getArgs
  contents <- readFile filename
  case parse document filename contents of
    Right doc@(Document defs cabis) -> case interpret doc of
      Right codes -> forM_ cabis $ \cabi@(C_ABI _ _ name) -> case Map.lookup name codes of
        Just code -> case depths code of
          Right dcode -> case compile cabi dcode of
            Right str -> putStr str
            Left compileError -> err compileError
          Left depthError -> err depthError
        Nothing -> err (Nothing :: Maybe ())
      Left interpretError -> err interpretError
    Left parseError -> err parseError
