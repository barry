{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Parse where

import Data.Char (isSpace)
import Text.Parsec hiding (token)

token = try (spaces >> many (satisfy (not . isSpace)))
literal s = try (spaces >> string s)

data Definition = Definition String LocalSpec [String]
  deriving Show

definition = do
  literal ":"
  name <- token
  spec <- localSpecification
  code <- token `manyTill` literal ";"
  pure $ Definition name spec code

data C_ABI = C_ABI String TypeSpec String
  deriving Show

c_abi = do
  literal "C-ABI"
  name <- token
  spec <- typeSpecification
  word <- token
  literal ";"
  pure $ C_ABI name spec word

data LocalSpec = LocalSpec [String] [String] [String]
  deriving Show

localSpecification = do
  literal "{"
  argsVars <- token `manyTill` literal "--"
  let (args, vars1) = span (/= "|") argsVars
      vars = drop 1 vars1
  outs <- token `manyTill` literal "}"
  pure $ LocalSpec args vars outs

data TypeSpec = TypeSpec [String] [String]
  deriving Show

typeSpecification = do
  literal "{"
  args <- token `manyTill` literal "--"
  outs <- token `manyTill` literal "}"
  pure $ TypeSpec args outs

data Document = Document [Definition] [C_ABI]
  deriving Show

document = do
  ins <- many definition
  outs <- many c_abi
  spaces
  eof
  pure $ Document ins outs
