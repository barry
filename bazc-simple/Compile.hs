{-
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}

module Compile where

import Control.Monad (forM, forM_, when)
import Control.Monad.Except (ExceptT, runExceptT, throwError)
import Control.Monad.Identity (Identity, runIdentity)
import Control.Monad.State (StateT, evalStateT, get, put)
import Control.Monad.Writer (WriterT, execWriterT, tell)
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map

import Parse
import Interpret

data CompileError
  = ProgramNotFound
  | PrimitiveNotFound
  | CompileStackUnderflow
  | CompileLocalUnderflow
  | TypeAmbiguous
  | TypeConflict
  | TypeMismatch
  deriving Show

compile :: C_ABI -> (Int, Int, [Code]) -> Either CompileError String
compile cabi code = runCompile $ tell ["#include \"barry.h\""] >> compiles cabi code

--runCompile :: Compile a -> Either CompileError String
runCompile = fmap unlines . runIdentity . runExceptT . execWriterT . flip evalStateT initial

type CompileT m a = StateT Compiler (WriterT [String] (ExceptT CompileError m)) a
type Compile a = CompileT Identity a

data Compiler = Compiler
  { nextFresh :: !Int
  , dataStack :: [Atom]
  , localStack :: [Maybe Atom]
  }

initial = Compiler 0 [] []

fresh :: Compile String
fresh = do
  c <- get
  let n = nextFresh c
  put c { nextFresh = n + 1 }
  return $ "v" ++ show n

push :: Atom -> String -> Compile ()
push t v = do
  c <- get
  tell ["DATA[" ++ show (length $ dataStack c) ++ "]." ++ show t ++ " = " ++ v ++ ";"]
  put c { dataStack = t : dataStack c }

pop :: Compile (Atom, String)
pop = do
  c <- get
  case dataStack c of
    [] -> throwError CompileStackUnderflow
    (t:ts) -> do
      put c { dataStack = ts }
      return (t, "DATA[" ++ show (length ts) ++ "]." ++ show t)

compiles :: C_ABI -> (Int, Int, [Code]) -> Compile ()
compiles (C_ABI name (TypeSpec ins outs) word) (dataSize, localSize, code) = do
  ins <- forM ins $ \t -> do
    v <- fresh
    return (read t, v)
  outs <- forM outs $ \t -> do
    v <- fresh
    return (read t, v)
  let indecls = map indecl ins
      outdecls = map outdecl outs
      indecl (t, v) = show t ++ " " ++ v
      outdecl (t, v) = show t ++ " *" ++ v
  tell ["extern void " ++ name ++ "(" ++ intercalate "," (indecls ++ outdecls) ++ ")"]
  tell ["{"]
  tell ["CELL DATA[" ++ show dataSize ++ "];"]
  tell ["CELL LOCAL[" ++ show localSize ++ "];"]
  forM_ ins $ \(t, v) -> push t v
  forM_ code primitive
  forM_ (reverse outs) $ \(t, v) -> do
    (t', v') <- pop
    when (t /= t') $ throwError TypeConflict
    tell ["*" ++ v ++ " = " ++ v' ++ ";"]
  tell ["}"]

primitive :: Code -> Compile ()
primitive (LocalInit) = do
  (t, v) <- pop
  c <- get
  tell ["LOCAL[" ++ show (length (localStack c)) ++ "]." ++ show t ++ " = " ++ v ++ ";"]
  put c { localStack = Just t : localStack c }

primitive (LocalNoInit) = do
  c <- get
  put c { localStack = Nothing : localStack c }

primitive (LocalDrop n) = do
  c <- get
  put c { localStack = drop n $ localStack c }

primitive (LocalFetch a) = do
  c <- get
  let ix = length (localStack c) - 1 - a
      t = localStack c !! a
  case t of
    Nothing -> throwError TypeAmbiguous
    Just t' -> push t' ("LOCAL[" ++ show ix ++ "]." ++ show t')

primitive (LocalStore z) = do
  (t, v) <- pop
  c <- get
  let ix = length (localStack c) - 1 - z
  case splitAt z (localStack c) of
    (lo, _:hi) -> do
      tell ["LOCAL[" ++ show ix ++ "]." ++ show t ++ " = " ++ v ++ ";"]
      put c { localStack = lo ++ [Just t] ++ hi }
    _ -> throwError CompileLocalUnderflow

primitive (PushInt a) = push I64 (show a)

primitive (PushFloat a) = push F64 (show a)

primitive (Unary op) = do
  (t, a) <- pop
  push t (show op ++ "(" ++ a ++ ")")

primitive (Binary op) = do
  (tb, b) <- pop
  (ta, a) <- pop
  when (ta /= tb) $ throwError TypeMismatch
  push ta (show op ++ "(" ++ a ++ "," ++ b ++ ")")

primitive (Cast op) = do
  (t, a) <- pop
  push op ("(" ++ show op ++ ")(" ++ a ++ ")")
