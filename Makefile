#!/usr/bin/env -S make -f
# barry -- bytebeat livecoding environment
# Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

VERSION := $(shell git describe)

CC ?= gcc
AR ?= ar
C99FLAGS ?= -std=gnu99 -Wall -Wextra -pedantic -MMD
OFLAGS ?= -O3
DEBUG ?= -g
SDL2 ?= `sdl2-config  --cflags --libs`
JACK ?= -ljack
SNDFILE ?= -lsndfile
OSS ?= -pthread

LIBOBJECTS = \
	chain.o \
	memory.o \
	primitive.o \
	program.o \
	repl.o \
	stack.o \
	stream.o \
	symbol.o \
	tokenizer.o \
	word.o \

LIBHEADERS = \
	barrington.h \

EXEOBJECTS = \
	audio.o \
	audio_jack.o \
	audio_sdl2.o \
	audio_oss.o \
	audio_sndfile.o \
	barry.o \

EXEHEADERS = \
	audio.h \

WEBOBJECTS = \
	audio.o \
	audio_sdl2.o \
	barry.o \

WEBHEADERS = \
	audio.h \

EXTRASOURCES = \
	Makefile \
	README.md \
	LICENSE.md \

SOURCES = \
	$(patsubst %.o,barrington/%.c,$(LIBOBJECTS)) \
	$(patsubst %,barrington/%,$(LIBHEADERS)) \
	$(patsubst %.o,barry/%.c,$(EXEOBJECTS)) \
	$(patsubst %,barry/%,$(EXEHEADERS)) \
	$(EXTRASOURCES)

EMBED = -Wl,--format=binary -Wl,barry-$(VERSION).tar.xz -Wl,--format=default

DEPENDS := \
	$(patsubst %.o,barrington/%.d,$(LIBOBJECTS)) \
	$(patsubst %.o,barry/%.d,$(EXEOBJECTS)) \

barry/barry: $(patsubst %,barry/%,$(EXEOBJECTS)) barrington/libbarrington.a barry-$(VERSION).tar.xz
	$(CC) $(C99FLAGS) $(OFLAGS) -o $@ $(patsubst %,barry/%,$(EXEOBJECTS)) -Lbarrington -lbarrington -DEMBED=1 $(EMBED) $(SDL2) $(JACK) $(SNDFILE) $(OSS) -lm

web/barry.html: $(patsubst %,barry/%,$(WEBOBJECTS)) barrington/libbarrington.a barry-$(VERSION).tar.xz
	$(CC) $(C99FLAGS) $(OFLAGS) -o $@ $(patsubst %,barry/%,$(WEBOBJECTS)) -Lbarrington -lbarrington -DEMBED=0 $(SDL2) -s 'EXTRA_EXPORTED_RUNTIME_METHODS=["ccall"]' -s TOTAL_MEMORY=64MB --proxy-to-worker

barrington/libbarrington.a: $(patsubst %,barrington/%,$(LIBOBJECTS))
	$(AR) -rs $@ $^

tarball: barry-$(VERSION).tar.xz

barry-$(VERSION).tar.xz: $(SOURCES)
	git archive --format=tar --prefix=barry-$(VERSION)/ HEAD | xz -c -9 > $@

%.o: %.c
	$(CC) $(C99FLAGS) $(OFLAGS) $(DEBUG) -Wno-gnu-zero-variadic-macro-arguments -o $@ -c $< -Ibarrington -DVERSION=$(VERSION) -DVERSION_SYM=$(subst -,_,$(VERSION)) $(SDL2)

clean:
	-rm -r \
		barry/barry	\
		barrington/libbarrington.a \
		$(patsubst %,barrington/%,$(LIBOBJECTS)) \
		$(patsubst %,barry/%,$(EXEOBJECTS)) \
		barry-$(VERSION).tar.xz \
		$(DEPENDS)

.PHONY: clean tarball

-include $(DEPENDS)
