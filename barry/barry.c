/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "barrington.h"

#include <getopt.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#define AUDIO_API_SDL2
#ifndef __EMSCRIPTEN__
#define AUDIO_API_JACK
#define AUDIO_API_OSS
#define AUDIO_API_SNDFILE
#endif
#include "audio.h"
enum backend
{
  backend_none,
  backend_sdl2,
  backend_jack,
  backend_oss,
  backend_sndfile
};
static const enum backend default_backend = backend_none;

#define OUTCHANNELS 2

// embedded source
#define CAT(a,b,c) a ## b ## c
#define STR(a) #a
#ifndef __EMSCRIPTEN__
#define F(V) extern const unsigned char \
  CAT(_binary_barry_,V,_tar_xz_start[]), \
  CAT(_binary_barry_,V,_tar_xz_end[]);
F(VERSION_SYM)
#undef F

static int write_file(const char *name, const unsigned char *start, const unsigned char *end)
{
  printf("writing '%s'... ", name);
  fflush(stdout);
  FILE *file = fopen(name, "wxb");
  if (! file)
  {
    printf("FAILED\n");
    return 0;
  }
  int ok = (fwrite(start, end - start, 1, file) == 1);
  fclose(file);
  if (ok)
    printf("ok\n");
  else
    printf("FAILED\n");
  return ok;
}

static int write_files(void)
{
  int ok = 1;
#define F(V,VS) ok &= write_file \
  ( "barry-" STR(V) ".tar.xz" \
  , CAT(_binary_barry_,VS,_tar_xz_start) \
  , CAT(_binary_barry_,VS,_tar_xz_end) \
  );
F(VERSION,VERSION_SYM)
#undef F
  return ok;
}
#endif

static volatile bool mutex;
static void lock() { mutex = true; }
static void unlock() { mutex = false; }
static void busywait() { while (mutex) ; }

struct chain_list
{
  struct chain_list *next;
  struct chain *value;
};

struct state
{
  uint64_t t;
  int64_t incr;
  struct repl *repl;
  struct symbol *audio, *time, *increment;
  struct stack *left;
  struct stack *right;
  struct chain_list *new_chain, *cur_chain, *old_chain;
  struct grid *t_grid, *c_grid, *o_grid;
  double SR;
};

static bool can_gc_cb(void *arg, struct word *w)
{
  (void) arg;
  (void) w;
  busywait();
  return true;
}

static void process_begin(struct audio *a)
{
  lock();
  struct state *j = audio_get_userdata(a);
  if (j->new_chain)
  {
    // push cur to old list
    j->cur_chain->next = j->old_chain;
    j->old_chain = j->cur_chain;
    // push new to cur list
    j->cur_chain = j->new_chain;
    j->new_chain = 0;
    // move cur tail to old
    struct chain_list *next;
    for (struct chain_list *l = j->cur_chain->next; l; l = next)
    {
      next = l->next;
      l->next = j->old_chain;
      j->old_chain = l;
    }
    j->cur_chain->next = 0;
  }
}

static bool audio_started = false;
static void process_end(struct audio *a)
{
  (void) a;
  unlock();
  if (! audio_started)
  {
    audio_started = true;
#ifdef __EMSCRIPTEN__
    EM_ASM( onbarrystarted(); );
#endif
  }
}

static void process(struct audio *a, audio_sample **in, audio_sample **out, int ins, int outs, int blocksize)
{
  process_begin(a);
  (void) in;
  (void) ins;
  struct state *j = audio_get_userdata(a);
  assert(j->t_grid->type == g_i64);
  assert(j->t_grid->ndims == 1);
  assert(j->t_grid->dim[0] == (u64) blocksize);
  assert(j->c_grid->type == g_i64);
  assert(j->c_grid->ndims == 0);
  if (! j->o_grid) goto end;
  if (j->o_grid->ndims != 1) goto end;
  if (j->o_grid->dim[0] != (u64) blocksize) goto end;
  for (int i = 0; i < blocksize; ++i)
  {
    j->t_grid->value.i64[i] = j->t;
    j->t += j->incr;
  }
  for (int c = 0; c < outs; ++c)
  {
    j->c_grid->value.i64[0] = c;
    union chain_item *w = j->cur_chain->value->data;
    while (w->chainop)
    {
      w = w->chainop(w);
    }
    for (int i = 0; i < blocksize; ++i)
    {
      audio_sample o = 0;
      switch (j->o_grid->type)
      {
        case g_u8: o = (((audio_sample) j->o_grid->value.u8[i]) - ((audio_sample) 0x80)) / ((audio_sample) 0x80); break;
        case g_i8: o = ((audio_sample) j->o_grid->value.i8[i]) / ((audio_sample) 0x80); break;
        case g_u16: o = (((audio_sample) j->o_grid->value.u16[i]) - ((audio_sample) 0x8000)) / ((audio_sample) 0x8000); break;
        case g_i16: o = ((audio_sample) j->o_grid->value.i16[i]) / ((audio_sample) 0x8000); break;
        case g_u32: o = (((audio_sample) j->o_grid->value.u32[i]) - ((audio_sample) 0x80000000u)) / ((audio_sample) 0x80000000u); break;
        case g_i32: o = ((audio_sample) j->o_grid->value.i32[i]) / ((audio_sample) 0x80000000u); break;
        case g_u64: o = (((audio_sample) j->o_grid->value.u64[i]) - ((audio_sample) 0x8000000000000000ull)) / ((audio_sample) 0x8000000000000000ull); break;
        case g_i64: o = ((audio_sample) j->o_grid->value.i64[i]) / ((audio_sample) 0x8000000000000000ull); break;
        case g_f32: o = ((audio_sample) j->o_grid->value.f32[i]); break;
        case g_f64: o = ((audio_sample) j->o_grid->value.f64[i]); break;
      }
      if (isinf(o) || isnan(o)) o = 0;
      if (! (-1 <= o)) o = -1;
      if (! (o <= 1)) o = 1;
      out[c][i] = o;
    }
  }
end:
  process_end(a);
}

static bool run_inhibited = false;

static bool run(struct repl *r, struct symbol *s)
{
  if (run_inhibited)
  {
    return true;
  }
  struct position p = repl_getpos(r);
  struct state *j = repl_get_user_data(r);
  if (s == j->audio)
  {
    busywait();
    struct chain_list *head = j->old_chain;
    while (head)
    {
      j->old_chain = head->next;
      chain_delete(head->value);
      free(head);
      busywait();
      head = j->old_chain;
    }
    struct chain_list *next = calloc(1, sizeof(*next));
    next->value = chain_new(1 << 28);
    enum error e = stack_push_grid(j->left, j->c_grid);
    if (e)
    {
      fprintf(stderr, "%d:%d: ERROR: c %s\n", p.row, p.col, error_string(e));
      chain_delete(next->value);
      free(next);
      return false;
    }
    e = stack_push_grid(j->left, j->t_grid);
    if (e)
    {
      fprintf(stderr, "%d:%d: ERROR: t %s\n", p.row, p.col, error_string(e));
      chain_delete(next->value);
      free(next);
      return false;
    }
    e = compile_symbol(p, s, next->value, j->left, j->right);
    if (e)
    {
      // error is already printed
      chain_delete(next->value);
      free(next);
      return false;
    }
    struct atom a;
    e = stack_pop(j->left, &a);
    if (e)
    {
      fprintf(stderr, "%d:%d: ERROR: %s\n", p.row, p.col, error_string(e));
      chain_delete(next->value);
      free(next);
      return false;
    }
    if (a.type != a_grid)
    {
      fprintf(stderr, "%d:%d: ERROR: not a grid %d\n", p.row, p.col, a.type);
      chain_delete(next->value);
      free(next);
      return false;
    }
    union chain_item zero;
    zero.chainop = 0;
    chain_append1(next->value, zero);
    busywait();
    next->next = j->new_chain;
    j->new_chain = next;
    j->o_grid = a.value.grid;
    fprintf(stdout, "\nOK! Memory: %lu current, %lu peak.\n",
      memory_get_allocated_current(), memory_get_allocated_peak());
    fflush(stdout);
    return true;
  }
  else
  {
    fprintf(stderr, "%d:%d: ERROR: expected audio\n", p.row, p.col);
  }
  return false;
}

static bool set_global(struct repl *r, struct symbol *s, struct atom *a)
{
  struct position p = repl_getpos(r);
  struct state *j = repl_get_user_data(r);
  if (s == j->time)
  {
    int ok = 0;
    switch (a->type)
    {
      case a_grid:
      {
        switch(a->value.grid->type)
        {
          case g_i64:
          {
            if (a->value.grid->ndims == 0)
            {
              i64 t = a->value.grid->value.i64[0];
              busywait();
              j->t = t;
              ok = 1;
            }
          }
        }
      }
    }
    if (! ok)
    {
      fprintf(stderr, "%d:%d: ERROR: not an i64\n", p.row, p.col);
    }
  }
  else if (s == j->increment)
  {
    int ok = 0;
    switch (a->type)
    {
      case a_grid:
      {
        switch(a->value.grid->type)
        {
          case g_i64:
          {
            if (a->value.grid->ndims == 0)
            {
              i64 incr = a->value.grid->value.i64[0];
              busywait();
              j->incr = incr;
              ok = 1;
            }
          }
        }
      }
    }
    if (! ok)
    {
      fprintf(stderr, "%d:%d: ERROR: not an i64\n", p.row, p.col);
    }
  }
  else
  {
    fprintf(stderr, "%d:%d: ERROR: expected increment or time\n", p.row, p.col);
  }
  return true;
}

struct state S;

#ifdef __EMSCRIPTEN__
void EMSCRIPTEN_KEEPALIVE input(const char *str)
{
  struct istream *i = istream_string_new(str);
  repl_run(S.repl, i);
  istream_delete(i);
}

void EMSCRIPTEN_KEEPALIVE enable_run(bool enable)
{
  run_inhibited = ! enable;
}
#endif

extern int main(int argc, char **argv)
{
  // parse args
#ifdef __EMSCRIPTEN__
  enum backend audio_backend = backend_sdl2;
#else
  enum backend audio_backend = default_backend;
  while (1)
  {
    int option_index = 0;
    static struct option long_options[] =
      { { "help",    no_argument,       0, 'h' }
      , { "version", no_argument,       0, 'v' }
      , { "source",  no_argument,       0, 'S' }
      , { "audio",   required_argument, 0, 'a' }
      , { 0,         0,                 0,  0  }
      };
    int opt = getopt_long(argc, argv, "hH?vVSa", long_options, &option_index);
    if (opt == -1) break;
    switch (opt)
    {
      case 'h':
      case 'H':
      case '?':
        printf(
          "barry -- bytebeat livecoding environment\n"
          "Copyright (C) 2020  Claude Heiland-Allen\n"
          "License: GNU AGPLv3+\n"
          "\n"
          "Usage:\n"
          "    %s [--audio=none|sdl2|jack|oss|sndfile]\n"
          "        launch interactive interpreter\n"
          "    %s -?,-h,-H,--help\n"
          "        output this message\n"
          "    %s -v,-V,--version\n"
          "        output version string\n"
          "    %s -S,--source\n"
          "        output %s's source code to the current working directory\n"
          "        files written:\n"
#define F(V) \
          "            barry-" STR(V) ".tar.xz\n"
F(VERSION)
#undef F
          , argv[0], argv[0], argv[0], argv[0], argv[0]
          );
        return 0;
      case 'v':
      case 'V':
#define F(V) \
        printf("%s\n", STR(V));
F(VERSION)
#undef F
        return 0;
      case 'S':
        return ! write_files();
      case 'a':
        if (0 == strcmp(optarg, "none"))
        {
          audio_backend = backend_none;
        }
        else if (0 == strcmp(optarg, "sdl2"))
        {
          audio_backend = backend_sdl2;
        }
        else if (0 == strcmp(optarg, "sndfile"))
        {
          audio_backend = backend_sndfile;
        }
        else if (0 == strcmp(optarg, "jack"))
        {
          audio_backend = backend_jack;
        }
        else if (0 == strcmp(optarg, "oss"))
        {
          audio_backend = backend_oss;
        }
        else
        {
          fprintf(stderr, "ERROR: unsupported audio backend\n");
          return 1;
        }
        break;
    }
  }
  if (optind < argc)
  {
    fprintf(stderr, "%s: error: too many arguments\n", argv[0]);
    return 1;
  }
#endif

  memset(&S, 0, sizeof(S));
  S.SR = 44100;

  // create new interpreter
  struct repl *r = S.repl = repl_new(stdout, stderr);
  repl_set_user_data(r, &S);
  dictionary_set_can_gc(repl_get_dictionary(r), &S, can_gc_cb);

  // launch audio backend
  struct audio *a = 0;
  if (audio_backend != backend_none)
  {

    // link processing to audio symbol in the interpreter
    S.audio = symbol_new(repl_get_symbol_table(r), "audio");
    S.time = symbol_new(repl_get_symbol_table(r), "time");
    S.increment = symbol_new(repl_get_symbol_table(r), "increment");
    repl_set_set_global(r, set_global);
    S.left = stack_new();
    S.right = stack_new();
    S.cur_chain = calloc(1, sizeof(*S.cur_chain));
    S.cur_chain->value = chain_new(1 << 28);
    union chain_item zero;
    zero.chainop = 0;
    chain_append1(S.cur_chain->value, zero);

    switch (audio_backend)
    {
      case backend_none: assert(! "reachable"); break;
      case backend_sdl2: a = audio_sdl2_new(S.SR, OUTCHANNELS); break;
#ifdef AUDIO_API_JACK
      case backend_jack: a = audio_jack_new(S.SR, 0, OUTCHANNELS, "barry", 1); break;
#endif
#ifdef AUDIO_API_OSS
      case backend_oss: a = audio_oss_new("/dev/dsp", S.SR, OUTCHANNELS); break;
#endif
#ifdef AUDIO_API_SNDFILE
      case backend_sndfile: a = audio_sndfile_output_new("barry.wav", S.SR, OUTCHANNELS, 0x10000000); break;
#endif
    }
    if (! a)
    {
      fprintf(stderr, "ERROR: could not open audio device\n");
      return 1;
    }
    audio_set_userdata(a, &S);
    audio_set_process(a, process);

    S.SR = audio_get_sample_rate(a);
    fprintf(stdout, "INFO: sample rate: %f\n", S.SR);

    u64 blocksize = audio_get_block_size(a);
    S.t_grid = grid_new(0, g_i64, 1, &blocksize);
    S.c_grid = grid_new(0, g_i64, 0, 0);

    // start audio
    if (audio_backend != backend_sndfile && audio_start(a))
    {
      fprintf(stderr, "ERROR: could not start audio\n");
      return 1;
    }
  }

  // run interpreter
  repl_set_run(r, run);

  // set SR to current sample rate
  {
    char set_sr[100];
    snprintf(set_sr, 100, ": SR { -- sr } %f ;\n", S.SR);
    struct istream *i = istream_string_new(set_sr);
    repl_run(r, i);
    istream_delete(i);
    struct symbol_table *t = repl_get_symbol_table(r);
    symbol_set_immutable(symbol_new(t, "SR"), true);
    // avoid potential confusion
    symbol_set_immutable(symbol_new(t, "RUN"), true);
    symbol_set_immutable(symbol_new(t, "time"), true);
    symbol_set_immutable(symbol_new(t, "increment"), true);
  }

#ifdef __EMSCRIPTEN__
  EM_ASM( onbarryloaded(); );
#else
  struct istream *i = istream_file_new(stdin);
  repl_run(r, i);
  istream_delete(i);
  if (audio_backend == backend_sndfile && a)
  {
    audio_start(a);
  }

  // cleanup
  if (a)
  {
    audio_delete(a);
    stack_delete(S.left);
    stack_delete(S.right);
  }
  repl_delete(r);
#endif
  return 0;
}
