/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "audio.h"

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <sys/soundcard.h>

#include <pthread.h>

struct audio_oss
{
  int running;
  pthread_t thread;
  int fd;
  int blocksize;
  int16_t *buf;
  audio_sample **inbufs;
  audio_sample **outbufs;
};

static void *audio_oss_thread(void *arg)
{
  struct audio *a = arg;
  struct audio_oss *s = audio_get_backend(a);
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  int blocksize = s->blocksize;
  while (s->running)
  {
    // clear buffers
    for (int i = 0; i < ins; ++i)
    {
      memset(s->inbufs[i], 0, sizeof(*s->inbufs[i]) * blocksize);
    }
    for (int i = 0; i < outs; ++i)
    {
      memset(s->outbufs[i], 0, sizeof(*s->outbufs[i]) * blocksize);
    }
    // callback
    audio_process(a, s->inbufs, s->outbufs, ins, outs, blocksize);
    // copy from buffer
    for (int i = 0; i < blocksize; ++i)
    {
      for (int c = 0; c < outs; ++c)
      {
        s->buf[i * outs + c] = fminf(fmaxf(s->outbufs[c][i], -1), 1) * 0x7FFF;
      }
    }
    write(s->fd, s->buf, sizeof(int16_t) * blocksize * outs);
  }
  return 0;
}

static int audio_oss_start(struct audio *a)
{
  struct audio_oss *s = audio_get_backend(a);
  s->running = 1;
  int err = pthread_create(&s->thread, 0, audio_oss_thread, a);
  if (err)
  {
    s->running = 0;
    return 1;
  }
  return 0;
}

static void audio_oss_delete(struct audio *a)
{
  if (! a)
  {
    return;
  }
  struct audio_oss *s = audio_get_backend(a);
  if (! s)
  {
    return;
  }
  if (s->running)
  {
    s->running = 0;
    pthread_join(s->thread, 0);
  }
  audio_set_backend(a, 0);
  if (s->fd > 0)
  {
    close(s->fd);
  }
  if (s->buf)
  {
    free(s->buf);
  }
  free(s);
}

extern struct audio *audio_oss_new(const char *device, double sr, int channels)
{
  struct audio_oss *s = calloc(1, sizeof(*s));
  if (! s)
  {
    return 0;
  }
  s->fd = open(device, O_RDWR);
  if (s->fd < 0)
  {
    free(s);
    return 0;
  }
  struct audio *a = audio_new(sr, 0, channels);
  if (! a)
  {
    close(s->fd);
    free(s);
    return 0;
  }
  audio_set_backend(a, s);
  audio_set_delete(a, audio_oss_delete);
  audio_set_start(a, audio_oss_start);
  int format = AFMT_S16_NE;
  if (-1 == ioctl(s->fd, SNDCTL_DSP_SETFMT, &format))
  {
    audio_delete(a);
    return 0;
  }
  if (format != AFMT_S16_NE)
  {
    audio_delete(a);
    return 0;
  }
  int channels2 = channels;
  if (-1 == ioctl(s->fd, SNDCTL_DSP_CHANNELS, &channels2))
  {
    audio_delete(a);
    return 0;
  }
  if (channels2 != channels)
  {
    audio_delete(a);
    return 0;
  }
  int speed = sr;
  if (-1 == ioctl(s->fd, SNDCTL_DSP_SPEED, &speed))
  {
    audio_delete(a);
    return 0;
  }
  audio_set_samplerate(a, speed);
  int fragmentsize = 0;
  if (-1 == ioctl(s->fd, SNDCTL_DSP_GETBLKSIZE, &fragmentsize))
  {
    audio_delete(a);
    return 0;
  }
  s->blocksize = fragmentsize / (sizeof(int16_t) * channels);
  audio_set_block_size(a, s->blocksize);
  s->buf = calloc(1, fragmentsize);
  if (! s->buf)
  {
    audio_delete(a);
    return 0;
  }
  return a;
}
