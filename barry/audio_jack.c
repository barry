/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "audio.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jack/jack.h>

struct audio_jack
{
  jack_client_t *client;
  jack_port_t **inports;
  jack_port_t **outports;
  jack_default_audio_sample_t **inbufs;
  jack_default_audio_sample_t **outbufs;
  int autoconnect;
};

static int audio_jack_process(jack_nframes_t nframes, void *arg)
{
  struct audio *a = arg;
  struct audio_jack *j = audio_get_backend(a);
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  for (int c = 0; c < ins; ++c)
  {
    j->inbufs[c] = jack_port_get_buffer(j->inports[c], nframes);
  }
  for (int c = 0; c < outs; ++c)
  {
    j->outbufs[c] = jack_port_get_buffer(j->outports[c], nframes);
    memset(j->outbufs[c], 0, sizeof(*j->outbufs[c]) * nframes);
  }
  audio_process(a, j->inbufs, j->outbufs, ins, outs, nframes);
	return 0;
}

static void audio_jack_delete(struct audio *a)
{
  if (! a)
  {
    return;
  }
  struct audio_jack *j = audio_get_backend(a);
  if (! j)
  {
    return;
  }
  if (j->client) jack_client_close(j->client);
  if (j->inports) free(j->inports);
  if (j->outports) free(j->outports);
  if (j->inbufs) free(j->inbufs);
  if (j->outbufs) free(j->outbufs);
  free(j);
  audio_set_backend(a, 0);
}

static int audio_jack_start(struct audio *a)
{
  if (! a)
  {
    return 0;
  }
  struct audio_jack *j = audio_get_backend(a);
  if (! j)
  {
    return 1;
  }
  if (jack_activate(j->client))
  {
    return 1;
  }
  if (j->autoconnect)
  {
    int ins = audio_get_in_channels(a);
    int outs = audio_get_out_channels(a);
    const char **ports = jack_get_ports(j->client, 0, 0, JackPortIsPhysical | JackPortIsOutput);
    if (ports)
    {
      for (int c = 0; c < ins; ++c)
      {
        if (! ports[c])
        {
          break;
        }
        jack_connect(j->client, ports[c], jack_port_name(j->inports[c]));
      }
      free(ports);
    }
    ports = jack_get_ports(j->client, 0, 0, JackPortIsPhysical | JackPortIsInput);
    if (ports)
    {
      for (int c = 0; c < outs; ++c)
      {
        if (! ports[c])
        {
          break;
        }
        jack_connect(j->client, jack_port_name(j->outports[c]), ports[c]);
      }
      free(ports);
    }
  }
  return 0;
}

extern struct audio *audio_jack_new(double sr, int ins, int outs, const char *name, int autoconnect)
{
  struct audio *a = audio_new(sr, ins, outs);
  if (! a)
  {
    return 0;
  }
  struct audio_jack *j = calloc(1, sizeof(*j));
  if (! j)
  {
    audio_delete(a);
    return 0;
  }
  audio_set_backend(a, j);
  audio_set_delete(a, audio_jack_delete);
  j->client = jack_client_open(name, JackNoStartServer, 0, 0);
  if (! j->client)
  {
    audio_delete(a);
    return 0;
  }
  audio_set_block_size(a, jack_get_buffer_size(j->client));
  audio_set_samplerate(a, jack_get_sample_rate(j->client));
  j->inports = calloc(1, sizeof(*j->inports) * ins);
  j->outports = calloc(1, sizeof(*j->outports) * outs);
  j->inbufs = calloc(1, sizeof(*j->inbufs) * ins);
  j->outbufs = calloc(1, sizeof(*j->outbufs) * outs);
  if (! j->inports || ! j->outports || ! j->inbufs || ! j->outbufs)
  {
    audio_delete(a);
    return 0;
  }
  for (int c = 0; c < ins; ++c)
  {
    char portname[100];
    snprintf(portname, 100, "input_%d", c + 1);
    j->inports[c] = jack_port_register
      (j->client, portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  }
  for (int c = 0; c < outs; ++c)
  {
    char portname[100];
    snprintf(portname, 100, "output_%d", c + 1);
    j->outports[c] = jack_port_register
      (j->client, portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  }
  jack_set_process_callback(j->client, audio_jack_process, a);
  j->autoconnect = autoconnect;
  audio_set_start(a, audio_jack_start);
  return a;
}
