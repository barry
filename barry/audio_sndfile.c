/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "audio.h"

#include <stdlib.h>

#include <sndfile.h>

struct audio_sndfile
{
  SF_INFO info;
  SF_INFO outfo;
  SNDFILE *in;
  SNDFILE *out;
  int blocksize;
  audio_sample *inbuf;
  audio_sample *outbuf;
  audio_sample **inbufs;
  audio_sample **outbufs;
  uint64_t nframes;
};

static int audio_sndfile_start_duplex(struct audio *a)
{
  struct audio_sndfile *sf = audio_get_backend(a);
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  // loop over samples
  int n;
  while (0 < (n = sf_readf_float(sf->in, sf->inbuf, sf->blocksize)))
  {
    // to buffer
    for (int i = 0; i < n; ++i)
    {
      for (int c = 0; c < ins; ++c)
      {
        sf->inbufs[c][i] = sf->inbuf[i * ins + c];
      }
      for (int c = 0; c < outs; ++c)
      {
        sf->outbufs[c][i] = 0;
      }
    }
    // callback
    audio_process(a, sf->inbufs, sf->outbufs, ins, outs, n);
    // from buffer
    for (int i = 0; i < n; ++i)
    {
      for (int c = 0; c < outs; ++c)
      {
        sf->outbuf[i * outs + c] = sf->outbufs[c][i];
      }
    }
    sf_writef_float(sf->out, sf->outbuf, n);
  }
  return 0;
}

static int audio_sndfile_start_output(struct audio *a)
{
  struct audio_sndfile *sf = audio_get_backend(a);
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  // loop over samples
  uint64_t m = 0;
  while (m < sf->nframes)
  {
    int i;
    // to buffer
    for (i = 0; i < sf->blocksize && m < sf->nframes; ++i, ++m)
    {
      for (int c = 0; c < ins; ++c)
      {
        sf->inbufs[c][i] = 0;
      }
      for (int c = 0; c < outs; ++c)
      {
        sf->outbufs[c][i] = 0;
      }
    }
    int n = i;
    // callback
    audio_process(a, sf->inbufs, sf->outbufs, ins, outs, n);
    // from buffer
    for (i = 0; i < n; ++i)
    {
      for (int c = 0; c < outs; ++c)
      {
        sf->outbuf[i * outs + c] = sf->outbufs[c][i];
      }
    }
    sf_writef_float(sf->out, sf->outbuf, n);
  }
  return 0;
}

static void audio_sndfile_delete(struct audio *a)
{
  if (! a)
  {
    return;
  }
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  struct audio_sndfile *s = audio_get_backend(a);
  if (! s)
  {
    return;
  }
  audio_set_backend(a, 0);
  if (s->in)
  {
    sf_close(s->in);
  }
  if (s->out)
  {
    sf_close(s->out);
  }
  if (s->inbuf)
  {
    free(s->inbuf);
  }
  if (s->outbuf)
  {
    free(s->outbuf);
  }
  if (s->inbufs)
  {
    for (int i = 0; i < ins; ++i)
    {
      if (s->inbufs[i])
      {
        free(s->inbufs[i]);
      }
    }
    free(s->inbufs);
  }
  if (s->outbufs)
  {
    for (int i = 0; i < outs; ++i)
    {
      if (s->outbufs[i])
      {
        free(s->outbufs[i]);
      }
    }
    free(s->outbufs);
  }
  free(s);
}

extern struct audio *audio_sndfile_duplex_new(const char *inpath, const char *outpath, int outs)
{
  struct audio_sndfile *s = calloc(1, sizeof(*s));
  if (! s)
  {
    return 0;
  }
  if (! (s->in = sf_open(inpath, SFM_READ, &s->info)))
  {
    free(s);
    return 0;
  }
  int ins = s->info.channels;
  s->outfo.samplerate = s->info.samplerate;
  s->outfo.channels = outs;
  s->outfo.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
  if (! (s->out = sf_open(outpath, SFM_WRITE, &s->outfo)))
  {
    sf_close(s->in);
    free(s);
    return 0;
  }
  struct audio *a = audio_new(s->info.samplerate, ins, outs);
  if (! a)
  {
    sf_close(s->out);
    sf_close(s->in);
    free(s);
    return 0;
  }
  audio_set_backend(a, s);
  audio_set_delete(a, audio_sndfile_delete);
  audio_set_start(a, audio_sndfile_start_duplex);
  s->blocksize = 1024;
  audio_set_block_size(a, 1024);
  s->inbuf = malloc(s->blocksize * s->info.channels * sizeof(*s->inbuf));
  s->outbuf = malloc(s->blocksize * s->outfo.channels * sizeof(*s->outbuf));
  if (! s->inbuf || ! s->outbuf)
  {
    audio_delete(a);
    return 0;
  }
  s->inbufs = calloc(ins, sizeof(*s->inbufs));
  if (ins > 0 && ! s->inbufs)
  {
    audio_delete(a);
    return 0;
  }
  for (int i = 0; i < ins; ++i)
  {
    s->inbufs[i] = calloc(s->blocksize, sizeof(*s->inbufs[i]));
    if (! s->inbufs[i])
    {
      audio_delete(a);
      return 0;
    }
  }
  s->outbufs = calloc(outs, sizeof(*s->outbufs));
  if (outs > 0 && ! s->outbufs)
  {
    audio_delete(a);
    return 0;
  }
  for (int i = 0; i < outs; ++i)
  {
    s->outbufs[i] = calloc(s->blocksize, sizeof(*s->outbufs[i]));
    if (! s->outbufs[i])
    {
      audio_delete(a);
      return 0;
    }
  }
  return a;
}

extern struct audio *audio_sndfile_output_new(const char *outpath, double sr, int outs, uint64_t nframes)
{
  struct audio_sndfile *s = calloc(1, sizeof(*s));
  if (! s)
  {
    return 0;
  }
  s->outfo.samplerate = sr;
  s->outfo.channels = outs;
  s->outfo.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
  if (! (s->out = sf_open(outpath, SFM_WRITE, &s->outfo)))
  {
    free(s);
    return 0;
  }
  struct audio *a = audio_new(sr, 0, outs);
  if (! a)
  {
    sf_close(s->out);
    free(s);
    return 0;
  }
  audio_set_backend(a, s);
  audio_set_delete(a, audio_sndfile_delete);
  audio_set_start(a, audio_sndfile_start_output);
  s->blocksize = 1024;
  audio_set_block_size(a, 1024);
  s->outbuf = malloc(s->blocksize * outs * sizeof(*s->outbuf));
  s->nframes = nframes;
  if (! s->outbuf)
  {
    audio_delete(a);
    return 0;
  }
  s->inbufs = 0;
  s->outbufs = calloc(outs, sizeof(*s->outbufs));
  if (outs > 0 && ! s->outbufs)
  {
    audio_delete(a);
    return 0;
  }
  for (int i = 0; i < outs; ++i)
  {
    s->outbufs[i] = calloc(s->blocksize, sizeof(*s->outbufs[i]));
    if (! s->outbufs[i])
    {
      audio_delete(a);
      return 0;
    }
  }
  return a;
}
