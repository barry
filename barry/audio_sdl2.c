/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "audio.h"

#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>

struct audio_sdl2
{
  SDL_AudioDeviceID device;
  int blocksize;
  audio_sample **inbufs;
  audio_sample **outbufs;
};

static void audio_sdl2_process(void *userdata, Uint8 *stream, int len)
{
  struct audio *a = userdata;
  struct audio_sdl2 *s = audio_get_backend(a);
  int ins = audio_get_in_channels(a);
  int outs = audio_get_out_channels(a);
  float *out = (float *) stream;
  int nframes = len / sizeof(float) / outs;
  assert(nframes <= s->blocksize);
  for (int c = 0; c < outs; ++c)
  {
    memset(s->outbufs[c], 0, sizeof(*s->outbufs[c]) * nframes);
  }
  // callback
  audio_process(a, s->inbufs, s->outbufs, ins, outs, nframes);
  // copy from buffer
  int k = 0;
  for (int i = 0; i < nframes; ++i)
  {
    for (int c = 0; c < outs; ++c)
    {
      out[k++] = s->outbufs[c][i];
    }
  }
}

static void audio_sdl2_delete(struct audio *a)
{
  if (! a)
  {
    return;
  }
  struct audio_sdl2 *s = audio_get_backend(a);
  if (! s)
  {
    return;
  }
  if (s->device)
  {
    SDL_CloseAudioDevice(s->device);
    SDL_Quit();
  }
  audio_set_backend(a, 0);
  if (s->outbufs)
  {
    int outs = audio_get_out_channels(a);
    for (int i = 0; i < outs; ++i)
    {
      if (s->outbufs[i])
      {
        free(s->outbufs[i]);
      }
    }
    free(s->outbufs);
  }
  free(s);
}

static int audio_sdl2_start(struct audio *a)
{
  struct audio_sdl2 *s = audio_get_backend(a);
  SDL_PauseAudioDevice(s->device, 0);
  return 0;
}

extern struct audio *audio_sdl2_new(double sr, int outs)
{
  struct audio *a = audio_new(sr, 0, outs);
  if (! a)
  {
    return 0;
  }
  struct audio_sdl2 *s = calloc(1, sizeof(*s));
  if (! s)
  {
    audio_delete(a);
    return 0;
  }
  audio_set_backend(a, s);
  audio_set_delete(a, audio_sdl2_delete);
  audio_set_start(a, audio_sdl2_start);
  SDL_Init(SDL_INIT_AUDIO);
  SDL_AudioSpec want, have;
  want.freq = 48000; // FIXME don't hardcode desired sample rate
  want.format = AUDIO_F32;
  want.channels = outs;
  want.samples = 4096; // FIXME don't hardcode buffer size
  want.callback = audio_sdl2_process;
  want.userdata = a;
  s->device = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (! s->device || have.format != AUDIO_F32 || have.channels != outs)
  {
    audio_delete(a);
    return 0;
  }
  audio_set_samplerate(a, have.freq);
  s->blocksize = have.samples;
  audio_set_block_size(a, s->blocksize);
  s->inbufs = 0;
  s->outbufs = calloc(outs, sizeof(*s->outbufs));
  if (outs > 0 && ! s->outbufs)
  {
    audio_delete(a);
    return 0;
  }
  for (int i = 0; i < outs; ++i)
  {
    s->outbufs[i] = calloc(s->blocksize, sizeof(*s->outbufs[i]));
    if (! s->outbufs[i])
    {
      audio_delete(a);
      return 0;
    }
  }
  return a;
}
