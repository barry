/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AUDIO_H
#define AUDIO_H 1

// user API types

struct audio;
typedef float audio_sample;
typedef void (*audio_process_t)(struct audio *, audio_sample **, audio_sample **, int, int, int);

// backend API

extern struct audio *audio_new(double sr, int ins, int outs);
extern void audio_set_backend(struct audio *a, void *backend);
extern void *audio_get_backend(struct audio *a);
extern void audio_delete_default(struct audio *a);
typedef void (*audio_delete_t)(struct audio *);
extern void audio_set_delete(struct audio *a, audio_delete_t delete_);
extern void audio_set_block_size(struct audio *a, int n);
extern void audio_set_samplerate(struct audio *a, double sr);
extern void audio_process_default(struct audio *a, audio_sample **in, audio_sample **out, int nins, int nouts, int blocksize);
extern void audio_set_process(struct audio *a, audio_process_t process);
typedef int (*audio_start_t)(struct audio *);
extern void audio_set_start(struct audio *a, audio_start_t start);

// user API

extern void audio_set_userdata(struct audio *a, void *userdata);
extern void *audio_get_userdata(struct audio *a);
extern int audio_get_in_channels(struct audio *a);
extern int audio_get_out_channels(struct audio *a);
extern int audio_get_block_size(struct audio *a);
extern double audio_get_sample_rate(struct audio *a);
extern int audio_start(struct audio *a);
extern void audio_process(struct audio *a, audio_sample **ins, audio_sample **outs, int nins, int nouts, int blocksize);
extern void audio_delete(struct audio *a);

// user API for backends

#ifdef AUDIO_API_SDL2
extern struct audio *audio_sdl2_new(double sr, int outs);
#endif

#ifdef AUDIO_API_JACK
extern struct audio *audio_jack_new(double sr, int ins, int outs, const char *name, int autoconnect);
#endif

#ifdef AUDIO_API_OSS
extern struct audio *audio_oss_new(const char *device, double sr, int channels);
#endif

#ifdef AUDIO_API_SNDFILE
extern struct audio *audio_sndfile_duplex_new(const char *inpath, const char *outpath, int outs);
extern struct audio *audio_sndfile_output_new(const char *outpath, double sr, int outs, uint64_t nframes);
#endif

#endif
