/*
barry -- bytebeat livecoding environment
Copyright (C) 2020  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "audio.h"

#include <stdlib.h>

struct audio_parameters
{
  double sr;
  int ins;
  int outs;
  int blocksize;
};

struct audio
{
  struct audio_parameters requested, actual;
  void *backend;
  void *userdata;
  audio_delete_t delete_;
  audio_start_t start;
  audio_process_t process;
};

extern int audio_start_default(struct audio *a)
{
  (void) a;
  return 0;
}

extern void audio_process_default(struct audio *a, audio_sample **in, audio_sample **out, int ins, int outs, int blocksize)
{
  (void) a;
  (void) in;
  (void) out;
  (void) ins;
  (void) outs;
  (void) blocksize;
  return;
}

extern void audio_delete_default(struct audio *a)
{
  (void) a;
  return;
}

extern struct audio *audio_new(double sr, int ins, int outs)
{
  struct audio *a = calloc(1, sizeof(*a));
  if (! a)
  {
    return 0;
  }
  a->requested.sr = sr;
  a->requested.ins = ins;
  a->requested.outs = outs;
  a->requested.blocksize = 0;
  a->actual = a->requested;
  a->delete_ = audio_delete_default;
  a->start = audio_start_default;
  a->process = audio_process_default;
  return a;
}

extern void audio_delete(struct audio *a)
{
  a->delete_(a);
  free(a);
}

extern void audio_set_delete(struct audio *a, audio_delete_t delete_)
{
  a->delete_ = delete_;
}

extern void audio_set_backend(struct audio *a, void *backend)
{
  a->backend = backend;
}

extern void *audio_get_backend(struct audio *a)
{
  return a->backend;
}

extern void audio_set_userdata(struct audio *a, void *userdata)
{
  a->userdata = userdata;
}

extern void *audio_get_userdata(struct audio *a)
{
  return a->userdata;
}

extern void audio_set_process(struct audio *a, audio_process_t process)
{
  a->process = process;
}

extern void audio_set_samplerate(struct audio *a, double sr)
{
  a->actual.sr = sr;
}

extern void audio_process(struct audio *a, audio_sample **ins, audio_sample **outs, int nins, int nouts, int blocksize)
{
  a->process(a, ins, outs, nins, nouts, blocksize);
}

extern void audio_set_start(struct audio *a, audio_start_t start)
{
  a->start = start;
}

extern int audio_start(struct audio *a)
{
  return a->start(a);
}

extern int audio_get_in_channels(struct audio *a)
{
  return a->actual.ins;
}

extern int audio_get_out_channels(struct audio *a)
{
  return a->actual.outs;
}

extern void audio_set_block_size(struct audio *a, int n)
{
  a->actual.blocksize = n;
}

extern int audio_get_block_size(struct audio *a)
{
  return a->actual.blocksize;
}

extern double audio_get_sample_rate(struct audio *a)
{
  return a->actual.sr;
}
